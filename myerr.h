#ifndef _MYERRCODE
#define _MYERRCODE

#include <petscsys.h>

typedef enum
{
  Successfully,
  WrongDomain, 			/* xmin>=xmax, or .. */
  CommUndefined,		/* MPI_Comm is undefined */
  MyDMUndefined,
  NumProcWrongFactors,		/* px*py*pz != p || px<=0, .. */
  EmptyArray,
  EmptyCtx,
  DividedByZero,
  IllegalIndex,
  WrongNumCells,
  WrongNumPML,
  CellLessThanProc,		/* px>nx, or .. */
  MyDMNotInitialized,
  HelmBVPNullFun,
  CannotOpenFile,
  FileLengthNonMatching,
  CtxIsNull,
  FileNameIsNull,
  HasCoordFalse,
  CoordOutofDomain,		/* mesh coodinate is out of domain */
  OptArrayIgnored,
  OptArrayInComplete,
  ZeroPhyCells,
  SrcPtsUnsetted,
  WavespeedDomainUnsetted,
  GaussQuadOrderOut,
  UnknownCtxFun,
  MyDOFNotInitialized,
  RobinOnPMLNotImplemented,
  NullMyDM,
  MyDMNeedsFunc,
  DDMNeedsInputs,
  DDMResetPartition,
  DDMResetInterface,
  DDMAlreadyInitialized,
  DDMNotInitialized,
  /* NotAnEdge, */
  /* NotAFace, */
  /* NotAVertex, */
  ZeroSubdomains,		/* sx<=0, or .. */
  TooManySubdomains,		/* sx>=nx, or .. */
  SubToProcNonInteger,		/* (sx%px)*(px%sx)!=0, or .. */
  NotTheSameDM,			/* ddof->gdof->dm!=ddof->ddm->gdm */
  DDOFNeedsInputs,
  DDOFNotInitialized,
  SubDOFNull,
  SubDOFBoundsNull,
  SubISNull,
  FETIDOFNull,
  FETIDOFNeedsInput,
  FETIDOFCannotOverlap,
  FETIDOFPMLNotImpl,
  FETIDOFNotInit,  
  FETIDOFWrong,
  MySectionWrong,
  BadDirection,
  UnknownError
} MyErrCode;

void CHKMyErrQ(MyErrCode myerr);

#endif
