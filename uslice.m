function uslice(u,nx,ny,nz,x0,y0,z0)
Lx= 13520; Ly= 13520; Lz= 4200;
x= linspace(0,Lx,nx+1); y= linspace(0,Ly,ny+1); z= linspace(0,Lz,nz+1);
u3d= reshape(u,nz+1,ny+1,nx+1); u3d= permute(u3d,[2 3 1]);
figure, fig1= slice(x,y,z,real(u3d),x0,y0,z0); title('Re'), colorbar;
figure, fig2= slice(x,y,z,imag(u3d),x0,y0,z0); title('Im'), colorbar;
set(fig1,'edgecolor','none');
set(fig2,'edgecolor','none');
% use caxis to get and to set color ranges