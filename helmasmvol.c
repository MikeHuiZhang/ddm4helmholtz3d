/* ---------------------------------------------------------------------------
   Assembly of the matrix for a HelmBVP with MyDOF corresponding to Q-1 element
   and Neumann boundary conditions.
   --------------------------------------------------------------------------- */
#ifndef _HELMASMVOL
#define _HELMASMVOL

/*
   Create cell mass and stiffness matrices.  Matrix entries are first counted
   along columns, which is called row major in Petsc's manual and is the right
   format for MatSetValuesLocal.  The following graph is cited from mydof.c

       3-----7
      /|    /|
     / |   / |
    1-----5  |
    |  2--|--6
    | /   | /
    |/    |/
    0-----4

*/
#undef __FUNCT__
#define __FUNCT__ "CellGetMatQ1"
MyErrCode CellGetMatQ1(PetscScalar **M, PetscScalar **K, PetscInt **rows, PetscInt *ndof, const PetscScalar h[])
{
  PetscErrorCode ierr;
  PetscInt i,j,ix,iy,iz,jx,jy,jz;
  PetscScalar h012,h120,h201;
  const PetscInt ndofxQ1= 2, ndofyQ1= 2, ndofzQ1= 2;
  const PetscInt ndofQ1= 8;

  PetscFunctionBegin;
  if(ndof) ndof[0]= ndofQ1;
  if(!h) return EmptyArray;

  /* cell dofs */
  if(rows) {
    if(!(*rows)) {ierr=PetscMalloc(ndofQ1*sizeof(PetscInt),rows);CHKERRQ(ierr);}
    for(i=0;i<ndofQ1;i++) (*rows)[i]= i;
  }

  /* cell mass: in 1D cell it is (h/3, h/6; h/6, h/3) */
  if(M) {
    if(!(*M)) {ierr=PetscMalloc(ndofQ1*(ndofQ1)*sizeof(PetscScalar),M);CHKERRQ(ierr);}

    (*M)[0]= 1.0/27.0*h[0]*h[1]*h[2]; /* M(0,0) */
    (*M)[1]= (*M)[0]*0.5;	      /* M(1,0) */
    (*M)[2]= (*M)[1];		      /* M(2,0) */
    (*M)[3]= (*M)[1]*0.5;
    (*M)[4]= (*M)[1];
    (*M)[5]= (*M)[3];
    (*M)[6]= (*M)[3];
    (*M)[7]= (*M)[3]*0.5;	/* M(7,0) */
    for(j=1;j<ndofQ1;j++){	/* M(:,j) */
      MyArrayind2sub(ndofxQ1, ndofyQ1, ndofzQ1, &jx, &jy, &jz, j);
      for(i=0;i<ndofQ1;i++){
	MyArrayind2sub(ndofxQ1, ndofyQ1, ndofzQ1, &ix, &iy, &iz, i);
	(*M)[j*ndofQ1+i]= (*M)[0]*((ix-jx)?.5:1.)*((iy-jy)?.5:1.)*((iz-jz)?.5:1.);
      }
    }
  }

  /* cell stiffness: in 1D cell it is (1.0/h, -1.0/h; -1.0/h, 1.0/h), in 3D it
     should be a sum of stiffness in one direction tensored with masses in other
     directions */
  if(K) {
    if(!(*K)) {ierr=PetscMalloc(ndofQ1*(ndofQ1)*sizeof(PetscScalar),K);CHKERRQ(ierr);}
    h012= h[0]*h[1]/h[2]; h120= h[1]*h[2]/h[0]; h201= h[2]*h[0]/h[1];
    (*K)[0]= (h012+h120+h201)/9.0;       /* K(0,0) */
    (*K)[1]= (-h012+(h120+h201)*.5)/9.0; /* K(1,0) */
    (*K)[2]= (-h201+(h012+h120)*.5)/9.0; /* K(2,0) */
    (*K)[3]= (h120*.5-(h012+h201))/18.0;
    (*K)[4]= (-h120+(h012+h201)*.5)/9.0;
    (*K)[5]= (h201*.5-(h012+h120))/18.0;
    (*K)[6]= (h012*.5-(h120+h201))/18.0;
    (*K)[7]= -(h012+h120+h201)/36.0; /* K(7,0) */
    for(j=1;j<ndofQ1;j++){	     /* K(:,j) */
      MyArrayind2sub(ndofxQ1, ndofyQ1, ndofzQ1, &jx, &jy, &jz, j);
      for(i=0;i<ndofQ1;i++){
	MyArrayind2sub(ndofxQ1, ndofyQ1, ndofzQ1, &ix, &iy, &iz, i);
	(*K)[j*ndofQ1+i]=(h012*((iz-jz)?-1:1)*((ix-jx)?.5:1)*((iy-jy)?.5:1)+
			  h120*((ix-jx)?-1:1)*((iz-jz)?.5:1)*((iy-jy)?.5:1)+
			  h201*((iy-jy)?-1:1)*((ix-jx)?.5:1)*((iz-jz)?.5:1))/9.0;
      }
    }
  }

  PetscFunctionReturn(0);
}

/*
   Gauss-Legendre quadrature points and weights in (0,1), obtained using lgwt.m.
*/
static PetscScalar *gp1d=PETSC_NULL, *gw1d=PETSC_NULL;
static MyErrCode GetGaussQuadrature(PetscInt order)
{
  PetscErrorCode ierr;
  if(gp1d || gw1d) {PetscFree(gp1d); PetscFree(gw1d);}
  ierr= PetscMalloc(order*sizeof(PetscScalar),&gp1d);CHKERRQ(ierr);
  ierr= PetscMalloc(order*sizeof(PetscScalar),&gw1d);CHKERRQ(ierr);
  switch (order) {
  case 1: gp1d[0]= 0.5; gw1d[0]= 1.0; break;
  case 2:
    gp1d[0]= 0.211324865405187; gw1d[0]= 0.5;
    gp1d[1]= 0.788675134594813; gw1d[1]= 0.5;
    break;
  case 3:
    gp1d[0]= 0.112701665379258; gw1d[0]= 0.277777777777777;
    gp1d[1]= 0.500000000000000; gw1d[1]= 0.444444444444444;
    gp1d[2]= 0.887298334620742; gw1d[2]= 0.277777777777777;
    break;
  case 4:
    gp1d[0]= 0.069431844202974; gw1d[0]= 0.173927422568727;
    gp1d[1]= 0.330009478207572; gw1d[1]= 0.326072577431273;
    gp1d[2]= 0.669990521792428; gw1d[2]= 0.326072577431273;
    gp1d[3]= 0.930568155797026; gw1d[3]= 0.173927422568727;
    break;
  case 5:
    gp1d[0]= 0.046910077030668; gw1d[0]= 0.118463442528095;
    gp1d[1]= 0.230765344947158; gw1d[1]= 0.239314335249683;
    gp1d[2]= 0.500000000000000; gw1d[2]= 0.284444444444444;
    gp1d[3]= 0.769234655052841; gw1d[3]= 0.239314335249683;
    gp1d[4]= 0.953089922969332; gw1d[4]= 0.118463442528095;
    break;
  case 6:
    gp1d[0]= 0.033765242898424; gw1d[0]= 0.085662246189585;
    gp1d[1]= 0.169395306766868; gw1d[1]= 0.180380786524069;
    gp1d[2]= 0.380690406958402; gw1d[2]= 0.233956967286345;
    gp1d[3]= 0.619309593041598; gw1d[3]= 0.233956967286345;
    gp1d[4]= 0.830604693233132; gw1d[4]= 0.180380786524069;
    gp1d[5]= 0.966234757101576; gw1d[5]= 0.085662246189585;
    break;
  case 8:
    gp1d[0]= 0.019855071751232; gw1d[0]= 0.050614268145188;
    gp1d[1]= 0.101666761293187; gw1d[1]= 0.111190517226687;
    gp1d[2]= 0.237233795041836; gw1d[2]= 0.156853322938944;
    gp1d[3]= 0.408282678752175; gw1d[3]= 0.181341891689181;
    gp1d[4]= 0.591717321247825; gw1d[4]= 0.181341891689181;
    gp1d[5]= 0.762766204958164; gw1d[5]= 0.156853322938944;
    gp1d[6]= 0.898333238706813; gw1d[6]= 0.111190517226687;
    gp1d[7]= 0.980144928248768; gw1d[7]= 0.050614268145188;
    break;
  case 10:
    gp1d[0]= 0.013046735741414; gw1d[0]= 0.033335672154344;
    gp1d[1]= 0.067468316655508; gw1d[1]= 0.074725674575290;
    gp1d[2]= 0.160295215850488; gw1d[2]= 0.109543181257991;
    gp1d[3]= 0.283302302935376; gw1d[3]= 0.134633359654998;
    gp1d[4]= 0.425562830509184; gw1d[4]= 0.147762112357376;
    gp1d[5]= 0.574437169490816; gw1d[5]= 0.147762112357376;
    gp1d[6]= 0.716697697064624; gw1d[6]= 0.134633359654998;
    gp1d[7]= 0.839704784149512; gw1d[7]= 0.109543181257991;
    gp1d[8]= 0.932531683344492; gw1d[8]= 0.074725674575290;
    gp1d[9]= 0.986953264258586; gw1d[9]= 0.033335672154344;
    break;
  default:
    return GaussQuadOrderOut;
  }
  return 0;
}

/*
   Compute the cell matrix corresponding to the Helmholtz operator, for use when
   xi is not constant in the cell i.e. when the cell is in PML.

   By the coordinate stretching

   D1 -> xi1*D1,  D2 -> xi2*D2,  D3 -> xi3*D3

   and divided by xi1*xi2*xi3*rho, the Helmholtz operator on u becomes

   -D1(xi1/xi2/xi3/rho* D1 u) - D2(xi2/xi3/xi2/rho* D2 u) - D3(xi3/xi1/xi2/rho* D3 u) - w^2/rho/xi1/xi2/xi3* u.

   We will make use of the tensor structure.  For example, the first term gives

   int_0^h1 xi1*D1(phi1_i)*D1(phi1_j) dx * int_0^h2 phi2_i*phi2_j/xi2 dy * int_0^h3 phi3_i*phi3_j/xi3 dz * 1.0/rho,

   where phi_i=phi1_i*phi2_i*phi3_i.  The zero-order term gives

   int_0^h1 phi1_i*phi1_j/xi1 dx * int_0^h2 phi2_i*phi2_j/xi2 dy * int_0^h3 phi3_i*phi3_j/xi3 dz * w^2/rho.
*/

#define HelmGetStencil1DQ1(Ks,Ms,d)					\
  {									\
    ctx4xi->L= ctx->L[d];						\
    dir= (PetscRealPart(ctx->t0[d])>PetscRealPart(ctx->t1[d]))?-1:1;	\
    if(cabs(ctx->t0[d])<HELMBVP_AS_ZERO && cabs(ctx->t1[d])<HELMBVP_AS_ZERO) \
      dir= 0;								\
    for(i=0;i<ctx->quad_order;++i){					\
      if (dir) {							\
	ctx4xi->t= ctx->t0[d]+gp1d[i]*(ctx->h[d])*dir;			\
	xii= ctx->xi(ctx4xi);						\
      }									\
      else xii= 1.0;							\
      Ks[0]+= xii*gw1d[i];						\
      Ms[0]+= (1.0-gp1d[i])*(1.0-gp1d[i])/xii*gw1d[i];			\
      Ms[1]+= (1.0-gp1d[i])*gp1d[i]/xii*gw1d[i];			\
      Ms[2]+= gp1d[i]*gp1d[i]/xii*gw1d[i];				\
    }									\
    Ks[0]= Ks[0]/ctx->h[d]; Ks[1]= -Ks[0];				\
    Ms[0]*= ctx->h[d]; Ms[1]*= ctx->h[d]; Ms[2]*= ctx->h[d];		\
  }

#undef __FUNCT__
#define __FUNCT__ "CellComputeHelmMatQ1"
MyErrCode CellComputeHelmMatQ1(PetscScalar Ai[],PetscInt idofs[],CellHelmCtxQ1 *ctx)
{
   /* 1D cell stiffness stencil, Ks[0] is self-interaction */
  PetscScalar Ks0[2]={0.0,0.0}, Ks1[2]={0.0,0.0}, Ks2[2]={0.0,0.0};
  /* 1D cell mass stencil, Ms[0] and Ms[2] are self-interactions for the left
   * and right nodes, resp., Ms[1] is the cross-interaction */
  PetscScalar Ms0[3]={0.0,0.0,0.0}, Ms1[3]={0.0,0.0,0.0}, Ms2[3]={0.0,0.0,0.0};
  PetscInt i,j,ix,iy,iz,jx,jy,jz;
  xiDefaultCtx *ctx4xi=PETSC_NULL;
  PetscErrorCode ierr;
  PetscInt dir;
  PetscScalar xii;
  const PetscInt ndof= 8;
  const PetscInt ndofx=2, ndofy=2, ndofz= 2;
  MyErrCode myerr;

  PetscFunctionBegin;

  /* initialize ctx4xi */
  if (ctx->xi==(CtxFun)xi_default) {
    ierr= PetscMalloc(sizeof(xiDefaultCtx),&ctx4xi); CHKERRQ(ierr);
    ctx4xi->sign= ctx->sign; ctx4xi->wi= ctx->wi;
  }
  else return UnknownCtxFun;
  if(!gp1d || !gw1d) {myerr= GetGaussQuadrature(ctx->quad_order);CHKMyErrQ(myerr);}

  /* compute cell 1D stencils, without the factors in w and rho */
  HelmGetStencil1DQ1(Ks0,Ms0,0);
  HelmGetStencil1DQ1(Ks1,Ms1,1);
  HelmGetStencil1DQ1(Ks2,Ms2,2);
  /* PetscScalarView(3,Ms0,PETSC_VIEWER_STDOUT_SELF); */
  /* PetscScalarView(2,Ks0,PETSC_VIEWER_STDOUT_SELF); */
  /* printf("Ks1= [%f+%fi,%f+%fi]\n",PetscRealPart(Ks1[0]),PetscImaginaryPart(Ks1[0]),PetscRealPart(Ks1[1]),PetscImaginaryPart(Ks1[1])); */

  /* set entries of Ai */
  for(i=0;i<ndof;++i) idofs[i]= i;
  for(j=0;j<ndof;++j) {
    MyArrayind2sub(ndofx,ndofy,ndofz,&jx,&jy,&jz,j);
    for(i=0;i<ndof;++i){
      MyArrayind2sub(ndofx,ndofy,ndofz,&ix,&iy,&iz,i);
      Ai[j*ndof+i]= ( ((ix-jx)?Ks0[1]:Ks0[0])*
		      ((iy-jy)?Ms1[1]:(iy?Ms1[2]:Ms1[0]))*
		      ((iz-jz)?Ms2[1]:(iz?Ms2[2]:Ms2[0]))+
		      ((ix-jx)?Ms0[1]:(ix?Ms0[2]:Ms0[0]))*
		      ((iy-jy)?Ks1[1]:Ks1[0])*
		      ((iz-jz)?Ms2[1]:(iz?Ms2[2]:Ms2[0]))+
		      ((ix-jx)?Ms0[1]:(ix?Ms0[2]:Ms0[0]))*
		      ((iy-jy)?Ms1[1]:(iy?Ms1[2]:Ms1[0]))*
		      ((iz-jz)?Ks2[1]:Ks2[0])-
		      (ctx->wi)*(ctx->wi)*
		      ((ix-jx)?Ms0[1]:(ix?Ms0[2]:Ms0[0]))*
		      ((iy-jy)?Ms1[1]:(iy?Ms1[2]:Ms1[0]))*
		      ((iz-jz)?Ms2[1]:(iz?Ms2[2]:Ms2[0])) ) / ctx->rho;
    }
  }
  PetscFree(ctx4xi);
  PetscFunctionReturn(0);
}

/*
   Setup the cell context in the d-th dimension.
*/
#define HelmCellInit1DQ1(id,eta_l,eta_r,nd,d,coord,inPML)		\
  {									\
    cellctx->h[d]= coord[id+1]-coord[id];				\
    if (id<eta_l) {							\
      cellctx->L[d]= coord[eta_l]-coord[0];				\
      cellctx->t0[d]= coord[eta_l]-coord[id];				\
      cellctx->t1[d]= coord[eta_l]-coord[id+1];				\
      inPML= PETSC_TRUE;						\
    }									\
    else if (id>nd-1-eta_r) {						\
      cellctx->L[d]= coord[nd] - coord[nd-eta_r];			\
      cellctx->t0[d]= coord[id] - coord[nd-eta_r];			\
      cellctx->t1[d]= coord[id+1] - coord[nd-eta_r];			\
      inPML= PETSC_TRUE;						\
    }									\
    else {								\
      cellctx->t0[d]= 0.0+0.0*PETSC_i;					\
      cellctx->t1[d]= 0.0+0.0*PETSC_i;					\
      inPML= PETSC_FALSE;						\
    }                                                                   \
  }

/*
   Interior assembly of HelmBVP with MyDOF.

   A -- matrix already created and supports MatSetValuesLocal
*/
#undef __FUNCT__
#define __FUNCT__  "HelmBVPInteriorAssembly"
MyErrCode HelmBVPInteriorAssembly(HelmBVP bvp, MyDOF dof, Mat A)
{
  PetscInt i, cellndof, ix, iy, iz, *idofs= 0, idof0;
  PetscScalar *Mi=0,*Ai=0;
  PetscErrorCode ierr;
  CellHelmCtxQ1 *cellctx=0;
  MyDM dm=0;
  PetscBool inPMLx=PETSC_FALSE, inPMLy=PETSC_FALSE, inPMLz=PETSC_FALSE;

  PetscFunctionBegin;
  dm= dof->dm;
  ierr= PetscMalloc(sizeof(CellHelmCtxQ1),&cellctx);CHKERRQ(ierr);
  cellctx->h[0]= 1.0; cellctx->h[1]= 1.0; cellctx->h[2]= 1.0;
  CellGetMatQ1(&Mi,PETSC_NULL,PETSC_NULL,&cellndof,cellctx->h);
  /* MyMatView(Mi,cellndof,cellndof,PETSC_NULL); */
  /* MyMatView(Ai,cellndof,cellndof,PETSC_NULL); */
  ierr= PetscMalloc(cellndof*cellndof*sizeof(PetscScalar),&Ai);CHKERRQ(ierr);
  ierr= PetscMalloc(cellndof*sizeof(PetscInt),&idofs);CHKERRQ(ierr);
  cellctx->sign=bvp->sign;cellctx->xi=bvp->xi;cellctx->quad_order=bvp->quad_order;
  for(ix=dm->ixbegin;ix<dm->ixend;++ix){
    HelmCellInit1DQ1(ix,dm->eta_xl,dm->eta_xr,dm->nx_all,0,dm->x,inPMLx);
    for(iy=dm->iybegin;iy<dm->iyend;++iy){
      HelmCellInit1DQ1(iy,dm->eta_yl,dm->eta_yr,dm->ny_all,1,dm->y,inPMLy);
      for(iz=dm->izbegin;iz<dm->izend;++iz){
  	HelmCellInit1DQ1(iz,dm->eta_zl,dm->eta_zr,dm->nz_all,2,dm->z,inPMLz);
  	MyArraysub2ind(dm->nxloc,dm->nyloc,dm->nzloc,ix-dm->ixbegin,iy-dm->iybegin,iz-dm->izbegin,&i);
  	cellctx->wi= bvp->w[i]; cellctx->rho= bvp->rho[i];
  	/* printf("i= %d, ",i); */
  	if (inPMLx || inPMLy || inPMLz) {
  	  /* printf("in PML\n"); */
  	  CellComputeHelmMatQ1(Ai,idofs,cellctx);
  	}
  	else {
  	  /* printf("not in PML\n"); */
  	  CellGetMatQ1(PETSC_NULL,&Ai,&idofs,PETSC_NULL,cellctx->h);
  	  for(i=0;i<cellndof*cellndof;++i)
  	    Ai[i]= (Ai[i]-(cellctx->wi)*(cellctx->wi)*Mi[i]*
  	  	    (cellctx->h[0])*(cellctx->h[1])*(cellctx->h[2]))/cellctx->rho;
  	}
  	inPMLz= PETSC_FALSE;
  	idof0= dof->celldof0(ix-dm->ixbegin,iy-dm->iybegin,iz-dm->izbegin,dm->nxloc,dm->nyloc,dm->nzloc);
  	for(i=0;i<cellndof;++i){
  	  idofs[i]= dof->celldofi(idofs[i],idof0,dof->ndof_xloc,dof->ndof_yloc,dof->ndof_zloc);
  	  /* printf("%d ",idofs[i]); */
  	}
  	/* printf("\n"); */
  	/* MyMatView(Ai,cellndof,cellndof,PETSC_NULL); */
  	MatSetValuesLocal(A,cellndof,idofs,cellndof,idofs,Ai,ADD_VALUES);
      }
      inPMLy= PETSC_FALSE;
    }
    inPMLx= PETSC_FALSE;
  }
  PetscFree(Mi); PetscFree(Ai); PetscFree(idofs); PetscFree(cellctx);
  PetscFree(gp1d); PetscFree(gw1d);
  PetscFunctionReturn(0);
}

#undef HelmCellInit1DQ1
#undef HelmGetStencil1DQ1

#endif
