#ifndef _MYKSP
#define _MYKSP

#include <petscksp.h>


extern PetscErrorCode DirectSetupKSP(KSP *ksp, Mat A);


#endif
