static char help[] = "Solves the Helmholtz equation in 3D.";

#include "helmbvp.h"
#include "myksp.h"
#include "fetihelm.h"

#undef __FUNCT__
#define __FUNCT__
int main(int argc, char**argv)
{
  char *optsfile=0, *prefix=0;
  HelmBVP bvp=0;
  MyDM dm=0;
  MyDOF dof=0;
  Mat A=0;
  Vec b=0;
  KSP kspdirect=0;
  MyDDM ddm=0;
  MyDDOF ddof=0;
  PetscViewer viewer=0;
  MyErrCode myerr;
  PetscBool set;
  PetscErrorCode ierr;

  PetscInitialize(&argc,&argv,PETSC_NULL,help);
  ierr=PetscOptionsInsertFile(PETSC_COMM_WORLD,"all.opts",PETSC_TRUE);CHKERRQ(ierr);
  ierr=PetscMalloc(MYIO_MAX_FILENAME*sizeof(char),&optsfile);CHKERRQ(ierr);

  /* HelmBVP */
  HelmBVPCreate(&bvp);
  ierr=PetscMalloc(HELMBVP_MAX_NAMELEN*sizeof(char),&prefix);CHKERRQ(ierr);
  PetscOptionsGetString("opts_","-bvp",optsfile,MYIO_MAX_FILENAME*sizeof(char),&set);if(!set) PetscStrcpy(optsfile,"bvp.opts");
  PetscOptionsGetString("opts_","-bvp_prefix",prefix,HELMBVP_MAX_NAMELEN*sizeof(char),&set);if(!set) PetscStrcpy(prefix,"bvp_");
  myerr= HelmBVPSetFromOptions(bvp,optsfile,prefix); CHKMyErrQ(myerr);
  PetscFree(prefix);
  HelmBVPViewOnScreen(bvp,PETSC_COMM_WORLD);

  /* Mesh: MyDM */
  MyDMCreate(&dm);
  ierr=PetscMalloc(MYDM_MAX_NAMELEN*sizeof(char),&prefix);CHKERRQ(ierr);
  PetscOptionsGetString("opts_","-dm",optsfile,MYIO_MAX_FILENAME*sizeof(char),&set);if(!set) PetscStrcpy(optsfile,"dm.opts");
  PetscOptionsGetString("opts_","-dm_prefix",prefix,MYDM_MAX_NAMELEN*sizeof(char),&set);if(!set) PetscStrcpy(prefix,"");
  MyDMSetFromOptions(dm,PETSC_COMM_WORLD,optsfile,prefix);
  PetscFree(prefix);
  myerr= MyDMSetup(dm); CHKMyErrQ(myerr);
  MyDMSetUniformCoord(dm,bvp->xmin,bvp->xmax,bvp->ymin,bvp->ymax,bvp->zmin,bvp->zmax);
  MyDMViewOnScreen(dm);

  /* DOF: MyDOF */
  MyDOFCreate(&dof);
  MyDOFSetDM(dof,dm);
  MyDOFSetType(dof,MyDOFQ1);
  MyDOFSetup(dof);
  MyDOFViewOnScreen(dof);


#if 0
  /* Decomposition of mesh: MyDDM */
  myerr= MyDDMCreate(&ddm); CHKMyErrQ(myerr);
  ierr=PetscMalloc(MYDDM_MAX_NAMELEN*sizeof(char),&prefix);CHKERRQ(ierr);
  PetscOptionsGetString("opts_","-ddm",optsfile,MYIO_MAX_FILENAME*sizeof(char),&set);if(!set) PetscStrcpy(optsfile,"ddm.opts");
  PetscOptionsGetString("opts_","-ddm_prefix",prefix,MYDDM_MAX_NAMELEN*sizeof(char),&set);if(!set) PetscStrcpy(prefix,"");
  MyDDMSetFromOptions(ddm,dm,optsfile,prefix); PetscFree(prefix);
  myerr= MyDDMSetup(ddm); CHKMyErrQ(myerr);
  myerr= MyDDMViewOnScreen(ddm); CHKMyErrQ(myerr);

  /* Decomposition of dof: MyDDOF */
  MyDDOFCreate(&ddof);
  MyDDOFSetGDOFDDM(ddof,dof,ddm);
  MyDDOFSetup(ddof);
//#include "tstscat.c"   /* tests of scatters */


  /* FETI algorithm: subdomains must __not__ have pml or overlap */
  FETIDOF fetidof=0;
  FETIDOFCreate(&fetidof);
  FETIDOFSetDDOF(fetidof,ddof);
  FETIDOFSetupSection(fetidof);	/* needed by fetihelm */
//#include "tstsection.c"   /* test of intersection dof */
  FETIHelm fetihelm=0;
  FETIHelmCreate(&fetihelm);
  FETIHelmSetBVPDOF(fetihelm,bvp,fetidof);
  ierr=PetscMalloc(HELMBVP_MAX_NAMELEN*sizeof(char),&prefix);CHKERRQ(ierr);
  PetscOptionsGetString("opts_","-feti",optsfile,MYIO_MAX_FILENAME*sizeof(char),&set);if(!set) PetscStrcpy(optsfile,"");
  PetscOptionsGetString("opts_","-feti_prefix",prefix,HELMBVP_MAX_NAMELEN*sizeof(char),&set);if(!set) PetscStrcpy(prefix,"");
  FETIHelmSetFromOptions(fetihelm,optsfile,prefix); PetscFree(prefix);
  FETIHelmSetup(fetihelm);	/* provides coarse dof for fetidof */
  FETIDOFSetup(fetidof);
  FETIHelmDestroy(&fetihelm);
  FETIDOFDestroy(&fetidof);
#endif

  /* Assembly of linear system */
  myerr= HelmBVPAssembly(bvp,dof,&A,&b);CHKMyErrQ(myerr);

  /* Direct solver */
#include "diresol.c"


  /* Clean-up: first free parents to avoid accessing destroyed children */
  MyDDOFDestroy(&ddof);
  MyDDMDestroy(&ddm);
  MyDOFDestroy(&dof);
  MyDMDestroy(&dm);
  HelmBVPDestroy(&bvp);
  MatDestroy(&A);
  VecDestroy(&b);
  KSPDestroy(&kspdirect);
  PetscFree(optsfile);
  PetscFinalize();

  return 0;
}
