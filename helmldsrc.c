#ifndef _HELMLDSRC
#define _HELMLDSRC

/*
   Compare the location of the point (key) and the end points of a 1D cell,
   return <0 if the point (key) is before the cell, return >0 if the point is
   after the cell, return 0 if the point is in the cell.  We assume no point
   lies on the cell boundary!
*/
static int PointInCell(const void *pkey, const void *pcell)
{
  return PetscRealPart(*(PetscScalar*)pkey)<PetscRealPart(*(PetscScalar*)pcell)?-1:
    (PetscRealPart(*(PetscScalar*)pkey)<PetscRealPart(((PetscScalar*)pcell)[1])?0:1);
}

/*
   Load one point source in a cell.
*/
#undef __FUNCT__
#define __FUNCT__ "LoadPointSourceInCellQ1"
static MyErrCode LoadPointSourceInCellQ1(PetscScalar srcpt[3], PetscScalar ampl, PetscScalar cellpt0[3], PetscScalar h[3], PetscScalar bcell[], PetscInt idofs[])
{
  PetscInt i;
  const PetscInt ndof= 8;

  PetscFunctionBegin;
  for(i=0;i<ndof;++i) idofs[i]= i;
  bcell[0]= ampl*(1.0-(srcpt[0]-cellpt0[0])/h[0])*(1.0-(srcpt[1]-cellpt0[1])/h[1])*(1.0-(srcpt[2]-cellpt0[2])/h[2]);
  bcell[1]= ampl*(1.0-(srcpt[0]-cellpt0[0])/h[0])*(1.0-(srcpt[1]-cellpt0[1])/h[1])*((srcpt[2]-cellpt0[2])/h[2]);
  bcell[2]= ampl*(1.0-(srcpt[0]-cellpt0[0])/h[0])*((srcpt[1]-cellpt0[1])/h[1])*(1.0-(srcpt[2]-cellpt0[2])/h[2]);
  bcell[3]= ampl*(1.0-(srcpt[0]-cellpt0[0])/h[0])*((srcpt[1]-cellpt0[1])/h[1])*((srcpt[2]-cellpt0[2])/h[2]);
  bcell[4]= ampl*((srcpt[0]-cellpt0[0])/h[0])*(1.0-(srcpt[1]-cellpt0[1])/h[1])*(1.0-(srcpt[2]-cellpt0[2])/h[2]);
  bcell[5]= ampl*((srcpt[0]-cellpt0[0])/h[0])*(1.0-(srcpt[1]-cellpt0[1])/h[1])*((srcpt[2]-cellpt0[2])/h[2]);
  bcell[6]= ampl*((srcpt[0]-cellpt0[0])/h[0])*((srcpt[1]-cellpt0[1])/h[1])*(1.0-(srcpt[2]-cellpt0[2])/h[2]);
  bcell[7]= ampl*((srcpt[0]-cellpt0[0])/h[0])*((srcpt[1]-cellpt0[1])/h[1])*((srcpt[2]-cellpt0[2])/h[2]);
  PetscFunctionReturn(0);
}


/*
   Load point sources: point sources in the PML are omitted, the amplitude is
   divided by the density rho (see also assembly of the matrix)
*/
#undef __FUNCT__
#define __FUNCT__ "HelmBVPInteriorLoad"
MyErrCode HelmBVPInteriorLoad(HelmBVP bvp,MyDOF dof,Vec *b)
{
  PetscScalar *pcell=0, *bcell=0, ampl, srcpt[3], cellpt0[3], h[3]; 
  PetscInt i,j,ix,iy,iz,*idofs=0, idof0;
  const PetscInt cellndof= 8;	/* assume Q1 element */
  MyDM dm;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  dm= dof->dm;
  ierr= PetscMalloc(cellndof*sizeof(PetscScalar),&bcell); CHKERRQ(ierr);
  ierr= PetscMalloc(cellndof*sizeof(PetscInt),&idofs); CHKERRQ(ierr);
  for(i=0;i<bvp->numsrcpts;++i) {
    pcell= (PetscScalar*) bsearch (bvp->srcpts_x+i, dm->x + dm->ixbegin, dm->nxloc, sizeof(PetscScalar), PointInCell);
    if(pcell && PetscRealPart(pcell[0])>=PetscRealPart(dm->x[dm->eta_xl])
       && PetscRealPart(pcell[0])<=PetscRealPart(dm->x[dm->nx_all-dm->eta_xr])) ix= pcell - dm->x; else ix= -1;
    pcell= (PetscScalar*) bsearch (bvp->srcpts_y+i, dm->y + dm->iybegin, dm->nyloc, sizeof(PetscScalar), PointInCell);
    if(pcell && PetscRealPart(pcell[0])>=PetscRealPart(dm->y[dm->eta_yl])
       && PetscRealPart(pcell[0])<=PetscRealPart(dm->y[dm->ny_all-dm->eta_yr])) iy= pcell - dm->y; else iy= -1;
    pcell= (PetscScalar*) bsearch (bvp->srcpts_z+i, dm->z + dm->izbegin, dm->nzloc, sizeof(PetscScalar), PointInCell);
    if(pcell && PetscRealPart(pcell[0])>=PetscRealPart(dm->z[dm->eta_zl])
       && PetscRealPart(pcell[0])<=PetscRealPart(dm->z[dm->nz_all-dm->eta_zr])) iz= pcell - dm->z; else iz= -1;
    if (ix>=0 && iy>=0 && iz>=0) {
      srcpt[0]= bvp->srcpts_x[i]; srcpt[1]= bvp->srcpts_y[i]; srcpt[2]= bvp->srcpts_z[i];
      MyArraysub2ind(dm->nxloc,dm->nyloc,dm->nzloc,ix-dm->ixbegin,iy-dm->iybegin,iz-dm->izbegin,&j);
      ampl= bvp->srcpts_amplitude[i]/bvp->rho[j];
      cellpt0[0]= dm->x[ix]; cellpt0[1]= dm->y[iy]; cellpt0[2]= dm->z[iz];
      h[0]= dm->x[ix+1]-dm->x[ix]; h[1]= dm->y[iy+1]-dm->y[iy]; h[2]= dm->z[iz+1]-dm->z[iz];
      LoadPointSourceInCellQ1(srcpt,ampl,cellpt0,h,bcell,idofs);
      idof0= dof->celldof0(ix-dm->ixbegin,iy-dm->iybegin,iz-dm->izbegin,dm->nxloc,dm->nyloc,dm->nzloc);
      for(j=0;j<cellndof;++j){
  	idofs[j]= dof->celldofi(idofs[j],idof0,dof->ndof_xloc,dof->ndof_yloc,dof->ndof_zloc);
      }
      VecSetValuesLocal(*b,cellndof,idofs,bcell,ADD_VALUES);
      printf("Load point source in the cell (%d, %d, %d)\n",ix,iy,iz);
    }
  }
  PetscFree(bcell); PetscFree(idofs);
  PetscFunctionReturn(0);
}

#endif
