  {
    Vec v=0, vi=0, vl=0;
    PetscScalar *a=0;
    PetscInt i, j, low, high;
    myerr=MyDDOFCreateScatters(ddof,MyDDOFFull);CHKMyErrQ(myerr);
    myerr=MyDDOFCreateScatters(ddof,MyDDOFRestricted);CHKMyErrQ(myerr);
    VecDuplicate(dof->v,&v); VecGetOwnershipRange(v,&low,&high);
    VecGetArray(v,&a);
    for (i=low;i<high;++i) a[i-low]= i;
    VecRestoreArray(v,&a); //VecAssemblyBegin(v); VecAssemblyEnd(v);
    /* VecView(v,PETSC_VIEWER_STDOUT_WORLD); */
#if 1
    for (i=0;i<ddof->sloc;++i) {
      VecDuplicate(ddof->subdof[i]->v,&vi);
      VecGetOwnershipRange(vi,&low,&high);
      VecGetArray(vi,&a);
      for (j=low;j<high;++j) a[j-low]= -1;
      VecRestoreArray(vi,&a); //VecAssemblyBegin(vi); VecAssemblyEnd(vi);
      MyVecGetLocalSubVector(vi,&vl);
      VecScatterBegin(ddof->scfull[i],v,vl,INSERT_VALUES,SCATTER_FORWARD);
      VecScatterEnd(ddof->scfull[i],v,vl,INSERT_VALUES,SCATTER_FORWARD);
      VecScatterBegin(ddof->scres[i],vl,v,INSERT_VALUES,SCATTER_REVERSE);
      VecScatterEnd(ddof->scres[i],vl,v,INSERT_VALUES,SCATTER_REVERSE);
      MyVecRestoreLocalSubVector(vi,&vl);
      {
	PetscInt sx, sy, sz;
	char filenamevi[MYDM_MAX_NAMELEN];
	MyArrayind2sub(ddof->sxloc,ddof->syloc,ddof->szloc,&sx,&sy,&sz,i);
	/* if (sx+ddof->ddm->isxs==0 && sy+ddof->ddm->isys==0 && sz+ddof->ddm->iszs==0) { */
	PetscSynchronizedPrintf(ddof->ddm->subdm[i]->comm,"subdomain (%d, %d, %d) ",sx+ddof->ddm->isxs,sy+ddof->ddm->isys,sz+ddof->ddm->iszs);
	PetscSynchronizedFlush(ddof->ddm->subdm[i]->comm);
	sprintf(filenamevi,"%d_%d_%d.bin",sx+ddof->ddm->isxs,sy+ddof->ddm->isys,sz+ddof->ddm->iszs);
	PetscViewerBinaryOpen(ddof->ddm->subdm[i]->comm,filenamevi,FILE_MODE_WRITE,&viewer);
	VecView(vi,viewer);
	PetscViewerDestroy(&viewer);
	/* VecView(vi,PETSC_VIEWER_STDOUT_(ddof->ddm->subdm[i]->comm)); */
	/* } */
      }
      VecDestroy(&vi);
    }
    /* VecView(v,PETSC_VIEWER_STDOUT_WORLD); */
    VecDestroy(&v);
#endif
  }
