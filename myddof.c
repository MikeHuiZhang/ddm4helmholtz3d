#include "myddof.h"

#undef __FUNCT__
#define __FUNCT__  "MyDDOFCreate"
MyErrCode MyDDOFCreate(MyDDOF *p_ddof)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr= PetscMalloc(sizeof(structMyDDOF),p_ddof); CHKERRQ(ierr);
  PetscMemzero(*p_ddof,sizeof(structMyDDOF));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyDDOFDestroy"
MyErrCode MyDDOFDestroy(MyDDOF *p_ddof)
{
  PetscInt i;
  PetscFunctionBegin;
  if (!*p_ddof) return(0);
  PetscFree((*p_ddof)->dofxs);
  PetscFree((*p_ddof)->dofxe);
  PetscFree((*p_ddof)->dofys);
  PetscFree((*p_ddof)->dofye);
  PetscFree((*p_ddof)->dofzs);
  PetscFree((*p_ddof)->dofze);
  PetscFree((*p_ddof)->dofinxs);
  PetscFree((*p_ddof)->dofinxe);
  PetscFree((*p_ddof)->dofinys);
  PetscFree((*p_ddof)->dofinye);
  PetscFree((*p_ddof)->dofinzs);
  PetscFree((*p_ddof)->dofinze);
  PetscFree((*p_ddof)->ndofopmlxl);
  PetscFree((*p_ddof)->ndofopmlxr);
  PetscFree((*p_ddof)->ndofopmlyl);
  PetscFree((*p_ddof)->ndofopmlyr);
  PetscFree((*p_ddof)->ndofopmlzl);
  PetscFree((*p_ddof)->ndofopmlzr);
  if ((*p_ddof)->subdof)
    for (i = 0; i < (*p_ddof)->sloc; ++i) {
      MyDOFDestroy((*p_ddof)->subdof+i);
      ISDestroy((*p_ddof)->lis+i);
      ISDestroy((*p_ddof)->gis+i);
      ISDestroy((*p_ddof)->lis_in+i);
      ISDestroy((*p_ddof)->gis_in+i);
    }
  PetscFree((*p_ddof)->subdof);
  PetscFree((*p_ddof)->lis); PetscFree((*p_ddof)->gis);
  PetscFree((*p_ddof)->lis_in); PetscFree((*p_ddof)->gis_in);
  PetscFree(*p_ddof);
  PetscFunctionReturn(0);
}

inline MyErrCode MyDDOFSetGDOF(MyDDOF ddof, MyDOF gdof)
{
  ddof->gdof= gdof; ddof->initialized= PETSC_FALSE; return 0;
}

inline MyErrCode MyDDOFSetDDM(MyDDOF ddof, MyDDM ddm)
{
  ddof->ddm= ddm; ddof->sloc= ddm->sloc; ddof->sxloc= ddm->sxloc;
  ddof->syloc= ddm->syloc; ddof->szloc= ddm->szloc;
  ddof->initialized= PETSC_FALSE; return 0;
}

#undef __FUNCT__
#define __FUNCT__ "MyDDOFSetGDOFDDM"
MyErrCode MyDDOFSetGDOFDDM(MyDDOF ddof, MyDOF gdof, MyDDM ddm)
{
  PetscFunctionBegin;
  if (gdof->dm!=ddm->gdm) return NotTheSameDM;
  ddof->gdof= gdof; ddof->ddm= ddm; ddof->sloc= ddm->sloc;
  ddof->sxloc= ddm->sxloc; ddof->syloc= ddm->syloc; ddof->szloc= ddm->szloc;
  ddof->initialized= PETSC_FALSE;
  PetscFunctionReturn(0);
}

static MyErrCode MyDDOFCreateSubBounds(MyDDOF ddof);
static MyErrCode MyDDOFCreateSubIS(MyDDOF ddof);
static MyErrCode MyDDOFRestrictSubIS(MyDDOF ddof);

/*
   Setup MyDDOF: subdof, lis, gis.

   Collective in the __original__ domain.
*/
#undef __FUNCT__
#define __FUNCT__  "MyDDOFSetup"
MyErrCode MyDDOFSetup(MyDDOF ddof)
{
  PetscInt i;
  MyDOFType doftype;
  MyErrCode myerr;

  PetscFunctionBegin;
  if (ddof->gdof->dm!=ddof->ddm->gdm) return NotTheSameDM;
  if (!ddof->gdof || !ddof->ddm || !ddof->gdof->initialized || !ddof->ddm->initialized) return DDOFNeedsInputs;
  if (ddof->sloc!=ddof->ddm->sloc) ddof->sloc= ddof->ddm->sloc;

  /* allocate and zero memory for arrays */
  PetscMalloc(ddof->sloc*sizeof(MyDOF),&(ddof->subdof));
  PetscMalloc(ddof->sloc*sizeof(IS),&(ddof->lis));
  PetscMemzero(ddof->lis,ddof->sloc*sizeof(IS));
  PetscMalloc(ddof->sloc*sizeof(IS),&(ddof->gis));
  PetscMemzero(ddof->gis,ddof->sloc*sizeof(IS));
  PetscMalloc(ddof->sloc*sizeof(IS),&(ddof->lis_in));
  PetscMemzero(ddof->lis_in,ddof->sloc*sizeof(IS));
  PetscMalloc(ddof->sloc*sizeof(IS),&(ddof->gis_in));
  PetscMemzero(ddof->gis_in,ddof->sloc*sizeof(IS));

  /* create subdof's */
  for (i = 0, MyDOFGetType(ddof->gdof,&doftype); i < ddof->sloc; ++i) {
    MyDOFCreate(ddof->subdof+i);
    MyDOFSetDM(ddof->subdof[i],ddof->ddm->subdm[i]);
    MyDOFSetType(ddof->subdof[i],doftype);
    MyDOFSetup(ddof->subdof[i]); /* collective in the subdomain, we are safe
				  * because ID of the subdomain is the same on
				  * every processor supporting it */
    /* printf("on subdof %d, proc %d, dofins=%d, dofine=%d\n",i,ddof->subdof[i]->dm->rank,ddof->subdof[i]->dofins,ddof->subdof[i]->dofine); */
  }
  ddof->has_subdof= PETSC_TRUE;

  /* create 3D bounds of subdomains in the orginal domain */
  myerr= MyDDOFCreateSubBounds(ddof); CHKMyErrQ(myerr);
  /* create lis and gis for subdomains */
  myerr= MyDDOFCreateSubIS(ddof); CHKMyErrQ(myerr);
  /* restrict lis and gis for dof non-overlapping subdomains */
  myerr= MyDDOFRestrictSubIS(ddof); CHKMyErrQ(myerr);
  ddof->initialized= PETSC_TRUE;
  PetscFunctionReturn(0);
}

static MyErrCode MyDDOFCreateSubBoundsQ1(MyDDOF ddof)
{
  PetscInt i, ix, iy, iz; PetscErrorCode ierr;
  if (!ddof->dofxs) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofxs));CHKERRQ(ierr);}
  if (!ddof->dofxe) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofxe));CHKERRQ(ierr);}
  if (!ddof->dofys) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofys));CHKERRQ(ierr);}
  if (!ddof->dofye) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofye));CHKERRQ(ierr);}
  if (!ddof->dofzs) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofzs));CHKERRQ(ierr);}
  if (!ddof->dofze) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofze));CHKERRQ(ierr);}
  if (!ddof->dofinxs) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofinxs));CHKERRQ(ierr);}
  if (!ddof->dofinxe) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofinxe));CHKERRQ(ierr);}
  if (!ddof->dofinys) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofinys));CHKERRQ(ierr);}
  if (!ddof->dofinye) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofinye));CHKERRQ(ierr);}
  if (!ddof->dofinzs) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofinzs));CHKERRQ(ierr);}
  if (!ddof->dofinze) {ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->dofinze));CHKERRQ(ierr);}
  if(!ddof->ndofopmlxl){ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->ndofopmlxl));CHKERRQ(ierr);}
  if(!ddof->ndofopmlxr){ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->ndofopmlxr));CHKERRQ(ierr);}
  if(!ddof->ndofopmlyl){ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->ndofopmlyl));CHKERRQ(ierr);}
  if(!ddof->ndofopmlyr){ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->ndofopmlyr));CHKERRQ(ierr);}
  if(!ddof->ndofopmlzl){ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->ndofopmlzl));CHKERRQ(ierr);}
  if(!ddof->ndofopmlzr){ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(ddof->ndofopmlzr));CHKERRQ(ierr);}

  for (ix=0; ix<ddof->ddm->sxloc; ++ix) {
    ddof->ndofopmlxl[ix]= ddof->ddm->opmlxl[ix]; ddof->ndofopmlxr[ix]= ddof->ddm->opmlxr[ix];
  }
  for (iy=0; iy<ddof->ddm->syloc; ++iy) {
    ddof->ndofopmlyl[iy]= ddof->ddm->opmlyl[iy]; ddof->ndofopmlyr[iy]= ddof->ddm->opmlyr[iy];
  }
  for (iz=0; iz<ddof->ddm->szloc; ++iz) {
    ddof->ndofopmlzl[iz]= ddof->ddm->opmlzl[iz]; ddof->ndofopmlzr[iz]= ddof->ddm->opmlzr[iz];
  }

  for (i=0, ix=0; ix<ddof->ddm->sxloc; ++ix)
    for (iy=0; iy<ddof->ddm->syloc; ++iy)
      for (iz=0; iz<ddof->ddm->szloc; ++iz, ++i) {
	ddof->dofxs[i]= ddof->ddm->xs[ix]-ddof->ddm->opmlxl[ix]+ddof->ddm->gdm->eta_xl;
	ddof->dofxe[i]= ddof->ddm->xe[ix]+ddof->ddm->opmlxr[ix]+1+ddof->ddm->gdm->eta_xl;
	ddof->dofys[i]= ddof->ddm->ys[iy]-ddof->ddm->opmlyl[iy]+ddof->ddm->gdm->eta_yl;
	ddof->dofye[i]= ddof->ddm->ye[iy]+ddof->ddm->opmlyr[iy]+1+ddof->ddm->gdm->eta_yr;
	ddof->dofzs[i]= ddof->ddm->zs[iz]-ddof->ddm->opmlzl[iz]+ddof->ddm->gdm->eta_zl;
	ddof->dofze[i]= ddof->ddm->ze[iz]+ddof->ddm->opmlzr[iz]+1+ddof->ddm->gdm->eta_zl;
	/* printf("3D subscripts of begin of current subdomain (%d,%d,%d)\n",ddof->dofxs[i],ddof->dofys[i],ddof->dofzs[i]); */
	/* printf("ddof->ddm->xe_in[%d]=%d\n",ix,ddof->ddm->xe_in[ix]); */
	ddof->dofinxs[i]= ddof->ddm->xs_in[ix]-ddof->ddm->opmlxl[ix]+ddof->ddm->gdm->eta_xl;
	ddof->dofinxe[i]= ddof->ddm->xe_in[ix]+ddof->ddm->opmlxr[ix]+(ddof->ddm->xe_in[ix]==ddof->ddm->gdm->nx_all?1:0)+ddof->ddm->gdm->eta_xl;
	ddof->dofinys[i]= ddof->ddm->ys_in[iy]-ddof->ddm->opmlyl[iy]+ddof->ddm->gdm->eta_yl;
	ddof->dofinye[i]= ddof->ddm->ye_in[iy]+ddof->ddm->opmlyr[iy]+(ddof->ddm->ye_in[iy]==ddof->ddm->gdm->ny_all?1:0)+ddof->ddm->gdm->eta_yl;
	ddof->dofinzs[i]= ddof->ddm->zs_in[iz]-ddof->ddm->opmlzl[iz]+ddof->ddm->gdm->eta_zl;
	ddof->dofinze[i]= ddof->ddm->ze_in[iz]+ddof->ddm->opmlzr[iz]+(ddof->ddm->ze_in[iz]==ddof->ddm->gdm->nz_all?1:0)+ddof->ddm->gdm->eta_zl;
	/* printf("ddof->dofinxe[%d]=%d\n",i,ddof->dofinxe[i]); */
      }
  return 0;
}

#undef __FUNCT__
#define __FUNCT__  "MyDDOFCreateSubBounds"
static MyErrCode MyDDOFCreateSubBounds(MyDDOF ddof)
{
  PetscFunctionBegin;
  if (!ddof->has_subdof) return SubDOFNull;
  switch (ddof->gdof->type) {
  case MyDOFQ1: default:
    MyDDOFCreateSubBoundsQ1(ddof); break;
  }
  ddof->has_bounds= PETSC_TRUE;
  PetscFunctionReturn(0);
}

/*
   Create subdomains local (on this processor) IS's (lis,gis).

   Not collective.
*/
#undef __FUNCT__
#define __FUNCT__  "MyDDOFCreateSubIS"
static MyErrCode MyDDOFCreateSubIS(MyDDOF ddof)
{
  PetscInt i, j, ix, iy, iz, sx, sy, sz, *lidx=0, *gidx=0, nidx;
  PetscInt ixbegin, iybegin, izbegin, ixend, iyend, izend, ixdisp, iydisp, izdisp;
  PetscErrorCode ierr;
  MyDOF subdof;
  PetscFunctionBegin;
  if (!ddof->gdof || !ddof->ddm || !ddof->gdof->initialized || !ddof->ddm->initialized) return DDOFNeedsInputs;
  if (!ddof->has_subdof) return SubDOFNull;
  if (!ddof->has_bounds) return SubDOFBoundsNull;
  for (i=0; i<ddof->sloc; ++i) {
    subdof= ddof->subdof[i];
    if (ddof->has_is) {
      ierr=ISDestroy(&(ddof->lis[i]));CHKERRQ(ierr); ierr=ISDestroy(&(ddof->gis[i]));CHKERRQ(ierr);
      ddof->has_is= PETSC_FALSE;
    }
    MyArrayind2sub(ddof->sxloc,ddof->syloc,ddof->szloc,&sx,&sy,&sz,i);
    /* ranges of original dof of subdomain on this proc, relative to subdof with
       pml, we assume pml on one direction is either original or from DD
       algorithm but never both */
    ixbegin= subdof->dofxins+(ddof->ndofopmlxl[sx]?0:subdof->npmlxl_in);
    iybegin= subdof->dofyins+(ddof->ndofopmlyl[sy]?0:subdof->npmlyl_in);
    izbegin= subdof->dofzins+(ddof->ndofopmlzl[sz]?0:subdof->npmlzl_in);
    ixend=   subdof->dofxine-(ddof->ndofopmlxr[sx]?0:subdof->npmlxr_in);
    iyend=   subdof->dofyine-(ddof->ndofopmlyr[sy]?0:subdof->npmlyr_in);
    izend=   subdof->dofzine-(ddof->ndofopmlzr[sz]?0:subdof->npmlzr_in);
    /* index relative to subdof with pml -> index relative to subdof with only original pml */
    ixdisp= ddof->ndofopmlxl[sx]-subdof->ndofpmlxl;
    iydisp= ddof->ndofopmlyl[sy]-subdof->ndofpmlyl;
    izdisp= ddof->ndofopmlzl[sz]-subdof->ndofpmlzl;
    /* malloc for indices */
    nidx= (ixend-ixbegin)*(iyend-iybegin)*(izend-izbegin);
    ierr=PetscMalloc(nidx*sizeof(PetscInt),&lidx);CHKERRQ(ierr);
    ierr=PetscMalloc(nidx*sizeof(PetscInt),&gidx);CHKERRQ(ierr);
    j= 0;
    for (ix=ixbegin; ix<ixend; ++ix) {
      for (iy=iybegin; iy<iyend; ++iy) {
	for (iz=izbegin; iz<izend; ++iz, ++j) {
	  lidx[j]= (ix-subdof->dofxins)*(subdof->ndof_zin)*(subdof->ndof_yin) + (iy-subdof->dofyins)*(subdof->ndof_zin) + iz-subdof->dofzins;
	  /* printf("3D subscripts of current dof (%d,%d,%d)->",ix+ixdisp+ddof->dofxs[i],iy+iydisp+ddof->dofys[i],iz+izdisp+ddof->dofzs[i]); */
	  ddof->gdof->petscid(ddof->gdof,ix+ixdisp+ddof->dofxs[i],iy+iydisp+ddof->dofys[i],iz+izdisp+ddof->dofzs[i],gidx+j);
	  /* printf("1D index %d\n",gidx[j]); */
	}
      }
    }
    ierr= ISCreateGeneral(PETSC_COMM_SELF,nidx,lidx,PETSC_OWN_POINTER,ddof->lis+i);CHKERRQ(ierr);
    ierr= ISCreateGeneral(PETSC_COMM_SELF,nidx,gidx,PETSC_OWN_POINTER,ddof->gis+i);CHKERRQ(ierr);
  }
  ddof->has_is=PETSC_TRUE;
  PetscFunctionReturn(0);
}

/*
   Restrict dof non-overlapping subdomain local IS's (lis,gis) to
   (lis_in,gis_in) based on 3D bounds (dofin?s, dofin?e).
*/
#undef __FUNCT__
#define __FUNCT__  "MyDDOFRestrictSubIS"
static MyErrCode MyDDOFRestrictSubIS(MyDDOF ddof)
{
  PetscInt i, j, k, ix, iy, iz, *lidx_in=0, *gidx_in=0, size, sx, sy, sz;
  PetscInt ixbegin, iybegin, izbegin, ixend, iyend, izend, ixdisp, iydisp, izdisp;
  const PetscInt *lidx=0, *gidx=0;
  MyDOF subdof;
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if (!ddof->has_subdof) return SubDOFNull;
  if (!ddof->has_is) return SubISNull;
  if (!ddof->has_bounds) return SubDOFBoundsNull;
  ddof->has_is_in= PETSC_FALSE;
  if (!ddof->lis_in) {
    PetscMalloc(ddof->sloc*sizeof(IS),&(ddof->lis_in));
    PetscMemzero(ddof->lis_in,ddof->sloc*sizeof(IS));
  }
  if (!ddof->gis_in) {
    PetscMalloc(ddof->sloc*sizeof(IS),&(ddof->gis_in));
    PetscMemzero(ddof->gis_in,ddof->sloc*sizeof(IS));
  }
  for (i=0; i<ddof->sloc; ++i) {
    subdof= ddof->subdof[i];
    ierr= ISGetLocalSize(ddof->lis[i],&size);CHKERRQ(ierr);
    ierr= ISGetIndices(ddof->lis[i],&lidx);CHKERRQ(ierr);
    ierr= ISGetIndices(ddof->gis[i],&gidx);CHKERRQ(ierr);
    if (ddof->lis_in[i]) {ierr=ISDestroy(&(ddof->lis_in[i]));CHKERRQ(ierr);}
    ierr=PetscMalloc(size*sizeof(PetscInt),&lidx_in);CHKERRQ(ierr);
    if (ddof->gis_in[i]) {ierr=ISDestroy(&(ddof->gis_in[i]));CHKERRQ(ierr);}
    ierr=PetscMalloc(size*sizeof(PetscInt),&gidx_in);CHKERRQ(ierr);
    /* printf("local subdof %d,subdof->dofxins=%d, dofxine=%d, ddof->dofxs[i]=%d, dofxe[i]=%d, ddof->dofinxs[i]=%d, dofinxe[i]=%d\n", */
    /* 	   i,subdof->dofxins,subdof->dofxine,ddof->dofxs[i],ddof->dofxe[i],ddof->dofinxs[i],ddof->dofinxe[i]); */

    /* use the same ranges as in CreateSubIS, loop and filter */
    MyArrayind2sub(ddof->sxloc,ddof->syloc,ddof->szloc,&sx,&sy,&sz,i);
    ixbegin= subdof->dofxins+(ddof->ndofopmlxl[sx]?0:subdof->npmlxl_in);
    iybegin= subdof->dofyins+(ddof->ndofopmlyl[sy]?0:subdof->npmlyl_in);
    izbegin= subdof->dofzins+(ddof->ndofopmlzl[sz]?0:subdof->npmlzl_in);
    ixend=   subdof->dofxine-(ddof->ndofopmlxr[sx]?0:subdof->npmlxr_in);
    iyend=   subdof->dofyine-(ddof->ndofopmlyr[sy]?0:subdof->npmlyr_in);
    izend=   subdof->dofzine-(ddof->ndofopmlzr[sz]?0:subdof->npmlzr_in);
    ixdisp=  ddof->ndofopmlxl[sx]-subdof->ndofpmlxl;
    iydisp=  ddof->ndofopmlyl[sy]-subdof->ndofpmlyl;
    izdisp=  ddof->ndofopmlzl[sz]-subdof->ndofpmlzl;
    k= 0;
    for (ix=ixbegin; ix<ixend; ++ix) {
      if (ix+ixdisp+ddof->dofxs[i]<ddof->dofinxs[i] || ix+ixdisp+ddof->dofxs[i]>=ddof->dofinxe[i]) continue;
      for (iy=iybegin; iy<iyend; ++iy) {
	if (iy+iydisp+ddof->dofys[i]<ddof->dofinys[i] || iy+iydisp+ddof->dofys[i]>=ddof->dofinye[i]) continue;
	for (iz=izbegin; iz<izend; ++iz) {
	  if (iz+izdisp+ddof->dofzs[i]<ddof->dofinzs[i] || iz+izdisp+ddof->dofzs[i]>=ddof->dofinze[i]) continue;
	  j= (ix-ixbegin)*(iyend-iybegin)*(izend-izbegin) + (iy-iybegin)*(izend-izbegin) + iz-izbegin;
	  lidx_in[k]= lidx[j];
	  /* printf("3D subscripts of current dof (%d,%d,%d)->",ix+ixdisp+ddof->dofxs[i],iy+iydisp+ddof->dofys[i],iz+izdisp+ddof->dofzs[i]); */
	  gidx_in[k]= gidx[j];
	  /* printf("1D index %d\n",gidx_in[k]); */
	  ++k;			/* count filered */
	}
      }
    }
    ierr= ISRestoreIndices(ddof->lis[i],&lidx);CHKERRQ(ierr);
    ierr= ISRestoreIndices(ddof->gis[i],&gidx);CHKERRQ(ierr);
    ierr= ISCreateGeneral(PETSC_COMM_SELF,k,lidx_in,PETSC_OWN_POINTER,ddof->lis_in+i);CHKERRQ(ierr);
    ierr= ISCreateGeneral(PETSC_COMM_SELF,k,gidx_in,PETSC_OWN_POINTER,ddof->gis_in+i);CHKERRQ(ierr);
  }
  ddof->has_is_in= PETSC_TRUE;
  PetscFunctionReturn(0);
}

/*
   Create VecScatter's from the original domain dof's to subdomain on
   proc. dof's.

   Collective in ddof->dm->comm.
*/
#undef __FUNCT__
#define __FUNCT__  "MyDDOFCreateScatters"
MyErrCode MyDDOFCreateScatters(MyDDOF ddof, MyDDOFRType rtype)
{
  PetscInt i;
  PetscErrorCode ierr;
  Vec vl=0;
  PetscFunctionBegin;
  if (!ddof->initialized) return DDOFNotInitialized;
  switch (rtype) {
  case MyDDOFRestricted:
    ierr=PetscMalloc(ddof->sloc*sizeof(VecScatter),&(ddof->scres));CHKERRQ(ierr);
    for (i=0;i<ddof->sloc;++i) {
      MyVecGetLocalSubVector(ddof->subdof[i]->v,&vl);
      ierr=VecScatterCreate(ddof->gdof->v,ddof->gis_in[i],vl,ddof->lis_in[i],ddof->scres+i);CHKERRQ(ierr);
      MyVecRestoreLocalSubVector(ddof->subdof[i]->v,&vl);
    }
    break;
  case MyDDOFFull: default:
    ierr=PetscMalloc(ddof->sloc*sizeof(VecScatter),&(ddof->scfull));CHKERRQ(ierr);
    for (i=0;i<ddof->sloc;++i) {
      MyVecGetLocalSubVector(ddof->subdof[i]->v,&vl);
      ierr=VecScatterCreate(ddof->gdof->v,ddof->gis[i],vl,ddof->lis[i],ddof->scfull+i);CHKERRQ(ierr);
      MyVecRestoreLocalSubVector(ddof->subdof[i]->v,&vl);
    }
    break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyVecGetLocalSubVector"
MyErrCode MyVecGetLocalSubVector(Vec v, Vec *vl)
{
  PetscErrorCode ierr;
  PetscInt size;
  PetscScalar *a=0;
  PetscFunctionBegin;
  ierr= VecGetLocalSize(v,&size);CHKERRQ(ierr);
  ierr= VecGetArray(v,&a);CHKERRQ(ierr);
  ierr= VecCreateSeqWithArray(PETSC_COMM_SELF,1,size,a,vl);
  ierr= VecRestoreArray(v,&a);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyVecRestoreLocalSubVector"
MyErrCode MyVecRestoreLocalSubVector(Vec v, Vec *vl)
{
  PetscErrorCode ierr, size;
  PetscScalar *a1, *a2;
  PetscFunctionBegin;
  ierr=VecGetArray(v,&a1);CHKERRQ(ierr);
  ierr=VecGetLocalSize(v,&size);CHKERRQ(ierr);
  ierr=PetscMalloc(size*sizeof(PetscScalar),&a2);CHKERRQ(ierr);
  ierr=PetscMemcpy(a2,a1,size*sizeof(PetscScalar));CHKERRQ(ierr);
  ierr=VecRestoreArray(v,&a1);CHKERRQ(ierr);
  ierr=VecReplaceArray(v,a2);CHKERRQ(ierr);
  VecDestroy(vl);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MyCoarseDOFCreate"
MyErrCode MyCoarseDOFCreate(MyCoarseDOF *p_dof)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr=PetscMalloc(sizeof(structMyCoarseDOF),p_dof);CHKERRQ(ierr);
  PetscMemzero(*p_dof,sizeof(structMyCoarseDOF));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MyCoarseDOFDestroy"
MyErrCode MyCoarseDOFDestroy(MyCoarseDOF *p_dof)
{
  PetscFunctionBegin;
  if (!*p_dof) return(0);
  PetscFree((*p_dof)->Q); PetscFree((*p_dof)->idof_sub); PetscFree((*p_dof)->idof_gc);
  PetscFree(*p_dof);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MyDualDOFCreate"
MyErrCode MyDualDOFCreate(MyDualDOF *p_dof)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr=PetscMalloc(sizeof(structMyDualDOF),p_dof);CHKERRQ(ierr);
  PetscMemzero(*p_dof,sizeof(structMyDualDOF));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MyDualDOFDestroy"
MyErrCode MyDualDOFDestroy(MyDualDOF *p_dof)
{
  PetscFunctionBegin;
  if (!*p_dof) return(0);
  PetscFree((*p_dof)->sign); PetscFree((*p_dof)->idof_sub);
  PetscFree((*p_dof)->idofs_gl); PetscFree((*p_dof)->idofe_gl);
  PetscFree(*p_dof);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MySectionDOFCreate"
MyErrCode MySectionDOFCreate(MySectionDOF *p_dof)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr=PetscMalloc(sizeof(structMySectionDOF),p_dof);CHKERRQ(ierr);
  PetscMemzero(*p_dof,sizeof(structMySectionDOF));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MySectionDOFDestroy"
MyErrCode MySectionDOFDestroy(MySectionDOF *p_dof)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if (!*p_dof) return(0);
  ierr=PetscFree((*p_dof)->dofin);CHKERRQ(ierr);
  ierr=PetscFree(*p_dof);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
