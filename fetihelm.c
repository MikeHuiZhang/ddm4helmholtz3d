#include "fetihelm.h"

MyErrCode FETIHelmCreate(FETIHelm *ctx)
{
  PetscErrorCode ierr;
  ierr=PetscMalloc(sizeof(structFETIHelm),ctx);CHKERRQ(ierr);
  ierr=PetscMemzero(*ctx,sizeof(structFETIHelm));CHKERRQ(ierr);
  return 0;
}

MyErrCode FETIHelmDestroy(FETIHelm *ctx)
{
  PetscErrorCode ierr;
  PetscInt i;
  if (!*ctx) return(0);
  ierr=PetscFree((*ctx)->prefix);CHKERRQ(ierr);
  if ((*ctx)->sbvp) for (i=0; i<(*ctx)->fetidof->ddof->sloc; ++i) HelmBVPDestroy((*ctx)->sbvp+i);
  ierr=PetscFree((*ctx)->sbvp);CHKERRQ(ierr);
  if ((*ctx)->destroycapp) (*ctx)->destroycapp(&((*ctx)->capp)); else PetscFree((*ctx)->capp);
  ierr=PetscFree(*ctx);CHKERRQ(ierr);
  return 0;
}

/* ---------------------------------------------------------------------------
   INPUT members/options.
   --------------------------------------------------------------------------- */

MyErrCode FETIHelmSetBVPDOF(FETIHelm ctx, const HelmBVP bvp, const FETIDOF dof)
{
  ctx->bvp= bvp; ctx->fetidof= dof; return 0;
}

#undef __FUNCT__
#define __FUNCT__  "FETIHelmSetPrefix"
MyErrCode FETIHelmSetPrefix(FETIHelm ctx,const char prefix[])
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if(prefix){
    if(prefix[0]) {
      if (!(ctx->prefix)) {
	ierr= PetscMalloc(HELMBVP_MAX_NAMELEN*sizeof(char),&(ctx->prefix));
	CHKERRQ(ierr);
      }
      PetscStrcpy(ctx->prefix,prefix);
    }
    else PetscFree(ctx->prefix); /* set to PETSC_NULL */
  }
  else PetscFree(ctx->prefix);
  PetscFunctionReturn(0);
}

MyErrCode FETIHelmSetCoarseFunc(FETIHelm fetihelm, ArrayCtxFun cff,
				FETIHelmCreateCoarseCtx createcfctx,
				FETIHelmDestroyCoarseCtx destroycfctx,
				ArrayCtxFun cfe,FETIHelmCreateCoarseCtx createcectx,
				FETIHelmDestroyCoarseCtx destroycectx)
{
  fetihelm->cff= cff; fetihelm->cfe=cfe; fetihelm->createcfctx= createcfctx;
  fetihelm->destroycfctx= destroycfctx; fetihelm->createcectx= createcectx;
  fetihelm->destroycectx= destroycectx; return 0;
}

/*
   prototypes of selectable function members
*/
typedef struct {
  PetscInt n1, n2;		/* number of dof nodes in two directions */
  PetscScalar *t1, *t2;		/* coordinates of dof nodes, (x,y), (y,z) or (x,z) */
  PetscScalar *w;		/* wavenumber at dof nodes, first t2, then t1 */
  PetscInt nplw;		/* number of directions */
  PetscScalar *l1,*l2;		/* unit norm when completed with normal component */
} structPlaneWavesCtx,*PlaneWavesCtx;

static inline MyErrCode PlaneWaves(PetscScalar*, const void*);
static MyErrCode PlaneWavesCreateCFCtx(void**,PetscInt,PetscInt,const struct FETIHelm_*);
static MyErrCode PlaneWavesCreateCECtx(void**,PetscInt,PetscInt,const struct FETIHelm_*);
static inline MyErrCode PlaneWavesDestroyCtx(void**);
static MyErrCode PlaneWavesAppend(struct FETIHelm_* fetihelm);
static MyErrCode PlaneWavesAppDestroy(void**app);

/*
   set all from options
*/
#undef __FUNCT__
#define __FUNCT__  "FETIHelmSetFromOptions"
MyErrCode FETIHelmSetFromOptions(FETIHelm ctx, const char *file, const char prefix[])
{
  PetscErrorCode ierr;
  PetscBool set;
  PetscInt i;
  PetscFunctionBegin;
  if (file) {
    if (file[0]) {
      ierr= PetscOptionsInsertFile(PETSC_COMM_WORLD,file,PETSC_TRUE); CHKERRQ(ierr);
    }
  }
  FETIHelmSetPrefix(ctx,prefix);
  ierr= PetscOptionsGetScalar(ctx->prefix,"-reg4op",&(ctx->reg4op),&set);
  CHKERRQ(ierr);
  ierr= PetscOptionsGetScalar(ctx->prefix,"-reg4pc",&(ctx->reg4pc),&set);
  CHKERRQ(ierr);
  ierr=PetscOptionsGetInt(ctx->prefix,"-nwf",&(ctx->nwf),&set);CHKERRQ(ierr);
  ierr=PetscOptionsGetInt(ctx->prefix,"-nwe",&(ctx->nwe),&set);CHKERRQ(ierr);
  ierr=PetscOptionsGetInt(ctx->prefix,"-nwv",&(ctx->nwv),&set);CHKERRQ(ierr);
  ctx->nwv= PetscMin(1,ctx->nwv); /* at most one dof at a vertex */
  ierr=PetscOptionsGetBool(ctx->prefix,"-pwf",&(ctx->pwf),&set);CHKERRQ(ierr);
  ierr=PetscOptionsGetBool(ctx->prefix,"-pwe",&(ctx->pwe),&set);CHKERRQ(ierr);
  ierr=PetscOptionsGetBool(ctx->prefix,"-pwv",&(ctx->pwv),&set);CHKERRQ(ierr);
  ierr=PetscOptionsGetInt(ctx->prefix,"-wfun",&i,&set);CHKERRQ(ierr);
  switch (i) {
  case 0: default:
    FETIHelmSetCoarseFunc(ctx,PlaneWaves,PlaneWavesCreateCFCtx,PlaneWavesDestroyCtx,PlaneWaves,PlaneWavesCreateCECtx,PlaneWavesDestroyCtx);
    PlaneWavesAppend(ctx); ctx->destroycapp= PlaneWavesAppDestroy;
    break;
  }
  PetscFunctionReturn(0);
}

/* ---------------------------------------------------------------------------
   Setup: create/modify members.
   --------------------------------------------------------------------------- */

#undef __FUNCT__
#define __FUNCT__  "FETIHelmSetup"
MyErrCode FETIHelmSetup(FETIHelm ctx)
{
  PetscFunctionBegin;
  FETIHelmSetDirichlet(ctx);
  FETIHelmCreateSubBVP(ctx);  	/* also load cell data for sub-bvp */
  FETIHelmCreateCoarseDOF(ctx);
  PetscFunctionReturn(0);
}

/* ---------------------------------------------------------------------------
   Selectable function members.
   --------------------------------------------------------------------------- */

/*
   generate plane waves on interfaces

   TODO: If we have pml around the original domain, there are some parts of
   interfaces lying in the pml.  We should take the complex stretching xi into
   account.
*/
static inline MyErrCode PlaneWaves(PetscScalar *array, const void *ctx)
{
  PetscInt i, j1, j2, k, l;
  PetscScalar x, y;
  PlaneWavesCtx ctx2=0;
  ctx2= (PlaneWavesCtx)ctx;
  l= 0;
  for (i=0; i<ctx2->nplw; ++i) {
    k= 0;
    for (j1=0; j1<ctx2->n1; ++j1) {
      x= (ctx2->t1[j1]-ctx2->t1[0])*(ctx2->l1[i]); /* lower-left corner as origin */
      for (j2=0; j2<ctx2->n2; ++j2, ++k, ++l) {
	y= (ctx2->t2[j2]-ctx2->t2[0])*(ctx2->l2[i]);
	array[l]= cexp(ctx2->w[k]*PETSC_i*(x+y));
      }
    }
  }
  return 0;
}

/*
   get options of plane waves
*/
typedef struct
{
  PetscInt ncost;
  PetscReal *cost;		/* cosine projection to interface */
} structPlaneWavesApp, *PlaneWavesApp;

static MyErrCode PlaneWavesAppDestroy(void**app)
{
  PetscFree(((PlaneWavesApp)(*app))->cost); PetscFree(*app); return 0;
}

static MyErrCode PlaneWavesAppend(struct FETIHelm_* fetihelm)
{
  PlaneWavesApp app=0;
  PetscErrorCode ierr;
  PetscBool set;
  ierr=PetscMalloc(sizeof(structPlaneWavesApp),&app);CHKERRQ(ierr);
  PetscMemzero(app,sizeof(structPlaneWavesApp));
  ierr=PetscOptionsGetInt(fetihelm->prefix,"-ncost",&(app->ncost),&set);CHKERRQ(ierr);
  if (set) {
    ierr=PetscOptionsGetRealArray(fetihelm->prefix,"-cost",app->cost,&(app->ncost),&set);CHKERRQ(ierr);
  }
  if (!set) {
    ierr=PetscMalloc(sizeof(PetscReal),&(app->cost));CHKERRQ(ierr);
    app->cost[0]= 0.8; app->ncost= 1;
  }
  fetihelm->capp= app; return 0;
}

/*
   i,j: on proc j-th section of the on proc i-th subdomain
*/
#undef __FUNCT__
#define __FUNCT__  "PlaneWavesCreateCFCtx"
static MyErrCode PlaneWavesCreateCFCtx(void **ctx,PetscInt i,PetscInt j,const struct FETIHelm_* fetihelm)
{
  PlaneWavesCtx pctx=0;
  PetscErrorCode ierr;
  MyDOF subdof=0;
  MyDM dm=0;
  MySectionDOF sec=0;
  PetscInt k,m,n,nk,nm,*elem=0, *elek=0;
  PlaneWavesApp app=0;
  PetscReal base;
  PetscFunctionBegin;
  subdof=fetihelm->fetidof->ddof->subdof[i]; sec=fetihelm->fetidof->dof_sf[i][j]; app=fetihelm->capp;
  ierr=PetscMalloc(sizeof(structPlaneWavesCtx),&pctx);CHKERRQ(ierr);
  ierr=PetscMemzero(pctx,sizeof(structPlaneWavesCtx));CHKERRQ(ierr);
  pctx->n1=sec->i1e-sec->i1s; pctx->n2=sec->i2e-sec->i2s; pctx->nplw=fetihelm->nwf;
  ierr=PetscMalloc(pctx->nplw*sizeof(PetscScalar),&(pctx->l1));CHKERRQ(ierr);
  ierr=PetscMalloc(pctx->nplw*sizeof(PetscScalar),&(pctx->l2));CHKERRQ(ierr);
  ierr=PetscMalloc(pctx->n1*(pctx->n2)*sizeof(PetscScalar),&(pctx->w));CHKERRQ(ierr);
  /* select directions */
  n= PetscMin(app->ncost,fetihelm->nwf);
  for (m=0; m<n; ++m) {
    nk= pctx->nplw/n; if (m<pctx->nplw%n) nk+= 1; base= 2*PETSC_PI/nk;
    for (k=0; k<nk; ++k) {
      pctx->l1[k]= app->cost[m]*cos(k*base); pctx->l2[k]= app->cost[m]*sin(k*base);
    }
  }
  /* set coordinates and wavenumber */
  dm= subdof->dm;
  switch (sec->isec) {

#define PlaneWavesGetcw(d1,d2,is1,is2,is0,ns1,ns2)			\
  subdof->getcoord1d(&(pctx->t1),subdof,d1,sec->i1s,sec->i1e);		\
  subdof->getcoord1d(&(pctx->t2),subdof,d2,sec->i2s,sec->i2e);		\
  for (n=0, m=sec->i1s; m<sec->i1e; ++m) {				\
    subdof->getelem1d(&nm,&elem,subdof,d1,m);				\
    elem[nm-1]= elem[nm-1]-is1;						\
    for (k=sec->i2s; k<sec->i2e; ++k,++n) {				\
      subdof->getelem1d(&nk,&elek,subdof,d2,k);				\
      elek[nk-1]= elek[nk-1]-is2;					\
      pctx->w[n]= fetihelm->sbvp[i]->w[elek[nk-1]*(ns2)+elem[nm-1]*(ns1)+is0]; \
      PetscFree(elek);							\
    }									\
    PetscFree(elem);							\
  }

  case 0:			/* (0,y,z) */
    PlaneWavesGetcw(1,2,dm->iybegin,dm->izbegin,0,dm->nzloc,1);
    break;
  case 1:			/* (1,y,z) */
    PlaneWavesGetcw(1,2,dm->iybegin,dm->izbegin,(dm->nxloc-1)*(dm->nyloc)*(dm->nzloc),dm->nzloc,1);
    break;
  case 2:			/* (x,0,z) */
#if 1
    PlaneWavesGetcw(0,2,dm->ixbegin,dm->izbegin,0,(dm->nyloc)*(dm->nzloc),1);
#else  /* for test of the macro */
    subdof->getcoord1d(&(pctx->t1),subdof,0,sec->i1s,sec->i1e);
    subdof->getcoord1d(&(pctx->t2),subdof,2,sec->i2s,sec->i2e);
    printf("i2s, i2e: %d, %d\n",sec->i2s,sec->i2e);
    for (n=0, m=sec->i1s; m<sec->i1e; ++m) {
      subdof->getelem1d(&nm,&elem,subdof,0,m);
      elem[nm-1]= elem[nm-1]-dm->ixbegin;
      printf("x: %d of %d\n",elem[nm-1],dm->nxloc);
      for (k=sec->i2s; k<sec->i2e; ++k,++n) {
	subdof->getelem1d(&nk,&elek,subdof,2,k);
	elek[nk-1]= elek[nk-1]-dm->izbegin;
	pctx->w[n]= fetihelm->sbvp[i]->w[elek[nk-1]+elem[nm-1]*(dm->nyloc)*(dm->nzloc)];
	printf("z: %d of %d\n",elek[nk-1],dm->nzloc);
	printf("%d of %d\n",elek[nk-1]+elem[nm-1]*(dm->nyloc)*(dm->nzloc),dm->nloc);
	printf("%f\n",PetscRealPart(fetihelm->sbvp[i]->w[elek[nk-1]+elem[nm-1]*(dm->nyloc)*(dm->nzloc)]));
	PetscFree(elek);
      }
      PetscFree(elem);
    }
#endif
    break;
  case 3:			/* (x,1,z) */
    PlaneWavesGetcw(0,2,dm->ixbegin,dm->izbegin,(dm->nyloc-1)*(dm->nzloc),(dm->nyloc)*(dm->nzloc),1);
    break;
  case 4:			/* (x,y,0) */
    PlaneWavesGetcw(0,1,dm->ixbegin,dm->iybegin,0,(dm->nyloc)*(dm->nzloc),dm->nzloc);
    break;
  case 5:			/* (x,y,1) */
    PlaneWavesGetcw(0,1,dm->ixbegin,dm->iybegin,1,(dm->nyloc)*(dm->nzloc),dm->nzloc);
    break;
  default:
    return MySectionWrong;
#undef PlaneWavesGetcw
  }
  (*ctx)= (void*)pctx;
  PetscFunctionReturn(0);
}

static inline MyErrCode PlaneWavesDestroyCtx(void **ctx)
{
  PlaneWavesCtx ctx2= (PlaneWavesCtx)(*ctx);
  PetscFree(ctx2->t1);PetscFree(ctx2->t2);PetscFree(ctx2->l1);
  PetscFree(ctx2->l2);PetscFree(ctx2->w);PetscFree(*ctx);return 0;
}

#undef __FUNCT__
#define __FUNCT__  "PlaneWavesCreateCECtx"
static MyErrCode PlaneWavesCreateCECtx(void **ctx,PetscInt i,PetscInt j,const struct FETIHelm_* fetihelm)
{
  PlaneWavesCtx pctx=0;
  PetscErrorCode ierr;
  MyDOF subdof=0;
  MyDM dm=0;
  MySectionDOF sec=0;
  PetscInt k,m,n,nk,nm,*elem=0;
  PlaneWavesApp app=0;
  PetscReal base;
  PetscFunctionBegin;
  subdof=fetihelm->fetidof->ddof->subdof[i]; sec=fetihelm->fetidof->dof_se[i][j]; app=fetihelm->capp;
  ierr=PetscMalloc(sizeof(structPlaneWavesCtx),&pctx);CHKERRQ(ierr);
  ierr=PetscMemzero(pctx,sizeof(structPlaneWavesCtx));CHKERRQ(ierr);
  pctx->n1=sec->i1e-sec->i1s; pctx->n2=1; pctx->nplw=fetihelm->nwf;
  ierr=PetscMalloc(pctx->nplw*sizeof(PetscScalar),&(pctx->l1));CHKERRQ(ierr);
  ierr=PetscMalloc(pctx->nplw*sizeof(PetscScalar),&(pctx->l2));CHKERRQ(ierr);
  ierr=PetscMalloc(pctx->n1*sizeof(PetscScalar),&(pctx->w));CHKERRQ(ierr);
  /* select directions */
  n= PetscMin(app->ncost,pctx->nplw);
  for (m=0; m<n; ++m) {
    nk= pctx->nplw/n; if (m<pctx->nplw%n) nk+= 1; base= 2*PETSC_PI/nk;
    for (k=0; k<nk; ++k) {
      pctx->l1[k]= app->cost[m]*cos(k*base); pctx->l2[k]= 0.;
    }
  }
  /* set coordinates and wavenumber */
  dm= subdof->dm;
  switch (sec->isec) {

#define PlaneWavesGetcwe(dir,ibegin,nn,start)				\
    subdof->getcoord1d(&(pctx->t1),subdof,dir,sec->i1s,sec->i1e);	\
    ierr=PetscMalloc(sizeof(PetscScalar),&(pctx->t2));CHKERRQ(ierr);	\
    pctx->t2[0]= 0.;							\
    for (n=0,m=sec->i1s; m<sec->i1e; ++m,++n) {				\
      subdof->getelem1d(&nm,&elem,subdof,dir,m);			\
      elem[nm-1]= elem[nm-1]-ibegin;					\
      pctx->w[n]= fetihelm->sbvp[i]->w[elem[nm-1]*nn+start];		\
      PetscFree(elem);							\
    }

  case 0: 
    PlaneWavesGetcwe(0,dm->ixbegin,dm->nyloc*(dm->nzloc),0);
    break;
  case 1:
    PlaneWavesGetcwe(0,dm->ixbegin,dm->nyloc*(dm->nzloc),dm->nzloc-1);
    break;
  case 2:
    PlaneWavesGetcwe(0,dm->ixbegin,dm->nyloc*(dm->nzloc),(dm->nyloc-1)*(dm->nzloc));
    break;
  case 3:
    PlaneWavesGetcwe(0,dm->ixbegin,dm->nyloc*(dm->nzloc),dm->nyloc*(dm->nzloc)-1);
    break;
  case 4:
    PlaneWavesGetcwe(1,dm->iybegin,dm->nzloc,0);
    break;
  case 5:
    PlaneWavesGetcwe(1,dm->iybegin,dm->nzloc,dm->nzloc-1);
    break;
  case 6:
    PlaneWavesGetcwe(1,dm->iybegin,dm->nzloc,(dm->nxloc-1)*(dm->nyloc)*(dm->nzloc));
    break;
  case 7:
    PlaneWavesGetcwe(1,dm->iybegin,dm->nzloc,(dm->nxloc-1)*(dm->nyloc)*(dm->nzloc)+dm->nzloc-1);
    break;
  case 8:			/* (0,0,z) */
#if 1
    PlaneWavesGetcwe(2,dm->izbegin,1,0);
#else  /* for test of the macro */
    subdof->getcoord1d(&(pctx->t1),subdof,2,sec->i1s,sec->i1e);
    ierr=PetscMalloc(sizeof(PetscScalar),&(pctx->t2));CHKERRQ(ierr);
    pctx->t2[0]= 0.;
    printf("i1s, i1e: %d, %d\n",sec->i1s,sec->i1e);
    for (n=0,m=sec->i1s; m<sec->i1e; ++m,++n) {
      subdof->getelem1d(&nm,&elem,subdof,2,m);
      elem[nm-1]= elem[nm-1]-dm->izbegin;
      printf("z: %d of %d\n",elem[nm-1],dm->nzloc);
      pctx->w[n]= fetihelm->sbvp[i]->w[elem[nm-1]];
      printf("cell: %d of %d\n",elem[nm-1],dm->nloc);
      printf("%f\n",PetscRealPart(fetihelm->sbvp[i]->w[elem[nm-1]]));
      PetscFree(elem);
    }
#endif
    break;
  case 9:
    PlaneWavesGetcwe(2,dm->izbegin,1,(dm->nyloc-1)*(dm->nzloc));
    break;
  case 10:
    PlaneWavesGetcwe(2,dm->izbegin,1,(dm->nxloc-1)*(dm->nyloc)*(dm->nzloc));
    break;
  case 11:
    PlaneWavesGetcwe(2,dm->izbegin,1,dm->nloc-dm->nzloc);
    break;
  default:
    return MySectionWrong;
#undef PlaneWavesGetcwe
  }
  (*ctx)= (void*)pctx;
  PetscFunctionReturn(0);
}


/* ---------------------------------------------------------------------------
   Functions for setup.
   --------------------------------------------------------------------------- */

/*
   subdomain face context for Robin conditions
*/
typedef struct {
  PetscScalar reg4op, reg4pc;
  PetscBool ispc;
} structFETIHelmBVPAppendix, *FETIHelmBVPAppendix, structFETIHelmRobinAppendix, *FETIHelmRobinAppendix;

static inline MyErrCode fetihelm_robincreateapp(void **app)
{
  PetscErrorCode ierr;
  ierr=PetscMalloc(sizeof(structFETIHelmRobinAppendix),app);CHKERRQ(ierr);
  return 0;
}

static inline MyErrCode fetihelm_robingetapp(struct HelmBVP_* bvp, RobinCtx* robin,PetscInt xmode,PetscInt ymode,PetscInt zmode)
{
  FETIHelmRobinAppendix app= (FETIHelmRobinAppendix)(robin->appendix);
  FETIHelmBVPAppendix bvpapp= (FETIHelmBVPAppendix)(bvp->appendix);
  app->reg4op= bvpapp->reg4op; app->reg4pc= bvpapp->reg4pc;
  app->ispc= bvpapp->ispc; return 0;
}

/*
   face cell context
*/
typedef struct {
  PetscScalar reg;
} structFETIHelmRobinCellCtx, *FETIHelmRobinCellCtx;

static inline MyErrCode fetihelm_robin_createcellctx(void**robin)
{
  PetscErrorCode ierr;
  ierr=PetscMalloc(sizeof(structFETIHelmRobinCellCtx),robin);CHKERRQ(ierr);
  return 0;
}

static inline MyErrCode fetihelm_robin_getcellctx(void*cellctx,RobinCtx*ctx,PetscInt i)
{
  FETIHelmRobinCellCtx cell= (FETIHelmRobinCellCtx)cellctx;
  FETIHelmRobinAppendix app= (FETIHelmRobinAppendix)ctx;
  cell->reg= app->ispc ? app->reg4pc : app->reg4op; return 0;
}

/*
   map cell context to Robin parameters
*/
static inline MyErrCode fetihelm_reg(PetscScalar *pf, const void *ctx)
{
  pf[0]= ((FETIHelmRobinCellCtx)ctx)->reg; pf[1]= 0; return 0;
}

/*
   create subdomain bvp's
*/
#undef __FUNCT__
#define __FUNCT__  "FETIHelmCreateSubBVP"
MyErrCode FETIHelmCreateSubBVP(FETIHelm ctx)
{
  PetscInt i,ix,iy,iz,j,k;
  PetscErrorCode ierr;
  MyDM dm=0;
  MyDDOF ddof=0;
  PetscReal x,y,z,xmin,xmax,ymin,ymax,zmin,zmax;
  MyErrCode myerr;
  PetscBool xlopen, xropen, ylopen, yropen, zlopen, zropen;
  PetscFunctionBegin;
  ddof= ctx->fetidof->ddof;
  ierr=PetscMalloc(ddof->sloc*sizeof(HelmBVP),&(ctx->sbvp));CHKERRQ(ierr);
  for (i=0; i<ddof->sloc; ++i) {
    HelmBVPCreate(ctx->sbvp+i);
    dm= ddof->subdof[i]->dm;
    /* physical domain */
    xmin= PetscRealPart(dm->x[dm->eta_xl]); xmax= PetscRealPart(dm->x[dm->nx_phy+dm->eta_xl]);
    ymin= PetscRealPart(dm->y[dm->eta_yl]); ymax= PetscRealPart(dm->y[dm->ny_phy+dm->eta_yl]);
    zmin= PetscRealPart(dm->z[dm->eta_zl]); zmax= PetscRealPart(dm->z[dm->nz_phy+dm->eta_zl]);
    myerr=HelmBVPSetDomain(ctx->sbvp[i],xmin,xmax,ymin,ymax,zmin,zmax);CHKMyErrQ(myerr);
    /* freq, wavenumber, density */
    HelmBVPSetFreq(ctx->sbvp[i],ctx->bvp->freq);
    HelmBVPSetCellDataFiles(ctx->sbvp[i],
			    ctx->bvp->wavespeedfilename,ctx->bvp->siz_wavespeed,
			    ctx->bvp->densityfilename,ctx->bvp->siz_density,ctx->bvp->avm_w,
			    ctx->bvp->avmctx_w,ctx->bvp->avm_rho,ctx->bvp->avmctx_rho);
    myerr=HelmBVPSetDataDomain(ctx->sbvp[i],ctx->bvp->wdomain,ctx->bvp->rhodomain);CHKMyErrQ(myerr);
    ctx->sbvp[i]->AttenuateWavespeed= ctx->bvp->AttenuateWavespeed; ctx->sbvp[i]->ComputeWavenumber= ctx->bvp->ComputeWavenumber;
    /* pml and boundary conditions */
    ctx->sbvp[i]->xi= ctx->bvp->xi;
    MyArrayind2sub(ddof->sxloc,ddof->syloc,ddof->szloc,&ix,&iy,&iz,i); ix+= ddof->ddm->isxs; iy+= ddof->ddm->isys; iz+= ddof->ddm->iszs;
    xlopen= (ctx->bvp->bctype_xl==Robin || ddof->ddm->gdm->eta_xl);
    xropen= (ctx->bvp->bctype_xr==Robin || ddof->ddm->gdm->eta_xr);
    ctx->sbvp[i]->bctype_xl= ix==0 ? ctx->bvp->bctype_xl : ((ix==1 && xlopen) || (ix==ddof->ddm->sx-1 && xropen) ? Neumann : Robin);
    ctx->sbvp[i]->bctype_xr= ix==ddof->ddm->sx-1 ? ctx->bvp->bctype_xr : ((ix==ddof->ddm->sx-2 && xropen) || (ix==0 && xlopen) ? Neumann : Robin);
    ylopen= (ctx->bvp->bctype_yl==Robin || ddof->ddm->gdm->eta_yl);
    yropen= (ctx->bvp->bctype_yr==Robin || ddof->ddm->gdm->eta_yr);
    ctx->sbvp[i]->bctype_yl= iy==0 ? ctx->bvp->bctype_yl : ((iy==1 && ylopen) || (iy==ddof->ddm->sy-1 && yropen) ? Neumann : Robin);
    ctx->sbvp[i]->bctype_yr= iy==ddof->ddm->sy-1 ? ctx->bvp->bctype_yr : ((iy==ddof->ddm->sy-2 && yropen) || (iy==0 && ylopen) ? Neumann : Robin);
    zlopen= (ctx->bvp->bctype_zl==Robin || ddof->ddm->gdm->eta_zl);
    zropen= (ctx->bvp->bctype_zr==Robin || ddof->ddm->gdm->eta_zr);
    ctx->sbvp[i]->bctype_zl= iz==0 ? ctx->bvp->bctype_zl : ((iz==1 && zlopen) || (iz==ddof->ddm->sz-1 && zropen) ? Neumann : Robin);
    ctx->sbvp[i]->bctype_zr= iz==ddof->ddm->sz-1 ? ctx->bvp->bctype_zr : ((iz==ddof->ddm->sz-2 && zropen) || (iz==0 && zlopen) ? Neumann : Robin);
    /* appendix of fetihelm bvp */
    {
      FETIHelmBVPAppendix appendix=0; ierr=PetscMalloc(sizeof(structFETIHelmBVPAppendix),&appendix);CHKERRQ(ierr);
      appendix->reg4op= ctx->reg4op; appendix->reg4pc= ctx->reg4pc; appendix->ispc= PETSC_FALSE;
      ctx->sbvp[i]->appendix= (void*)appendix;
    }
    /* robin boundary: effect only if bctype is Robin */
    if (ix && ix-ddof->ddm->sx+1 && iy && iy-ddof->ddm->sy+1 && iz && iz-ddof->ddm->sz+1) {
      /* regularization on interfaces */
      ctx->sbvp[i]->sign= (ix+iy+iz)%2 ? 1 : -1;
      ctx->sbvp[i]->robin_order= 0;
      ctx->sbvp[i]->robin_pf= fetihelm_reg;
      ctx->sbvp[i]->robincreateapp= fetihelm_robincreateapp;
      ctx->sbvp[i]->robingetapp= fetihelm_robingetapp;
      ctx->sbvp[i]->robin_createcellctx= fetihelm_robin_createcellctx;
      ctx->sbvp[i]->robin_getcellctx= fetihelm_robin_getcellctx;
    }
    else {			/* on the original boundary */
      ctx->sbvp[i]->sign= ctx->bvp->sign;
      ctx->sbvp[i]->robin_order= ctx->bvp->robin_order;
      ctx->sbvp[i]->robin_pf= ctx->bvp->robin_pf; ctx->sbvp[i]->robin_pe= ctx->bvp->robin_pe; ctx->sbvp[i]->robin_pv= ctx->bvp->robin_pv;
    }
    /* source terms: if a point is on interface, assign it to the right subdomain */
    for (j=0; j<ctx->bvp->numsrcpts; ++j) {
      x= PetscRealPart(ctx->bvp->srcpts_x[j]); y= PetscRealPart(ctx->bvp->srcpts_y[j]); z= PetscRealPart(ctx->bvp->srcpts_z[j]);
      if (x>=xmin && x<xmax && y>=ymin && y<ymax && z>=zmin && z<zmax) {
	ctx->sbvp[i]->numsrcpts++;
      }
    }
    ierr=PetscMalloc(ctx->sbvp[i]->numsrcpts*sizeof(PetscScalar),&(ctx->sbvp[i]->srcpts_x));CHKERRQ(ierr);
    ierr=PetscMalloc(ctx->sbvp[i]->numsrcpts*sizeof(PetscScalar),&(ctx->sbvp[i]->srcpts_y));CHKERRQ(ierr);
    ierr=PetscMalloc(ctx->sbvp[i]->numsrcpts*sizeof(PetscScalar),&(ctx->sbvp[i]->srcpts_z));CHKERRQ(ierr);
    ierr=PetscMalloc(ctx->sbvp[i]->numsrcpts*sizeof(PetscScalar),&(ctx->sbvp[i]->srcpts_amplitude));CHKERRQ(ierr);
    for (k=0,j=0; j<ctx->bvp->numsrcpts; ++j) {
      x= PetscRealPart(ctx->bvp->srcpts_x[j]); y= PetscRealPart(ctx->bvp->srcpts_y[j]); z= PetscRealPart(ctx->bvp->srcpts_z[j]);
      if (x>=xmin && x<xmax && y>=ymin && y<ymax && z>=zmin && z<zmax) {
	ctx->sbvp[i]->srcpts_x[k]= ctx->bvp->srcpts_x[j];
	ctx->sbvp[i]->srcpts_y[k]= ctx->bvp->srcpts_y[j];
	ctx->sbvp[i]->srcpts_z[k]= ctx->bvp->srcpts_z[j];
	ctx->sbvp[i]->srcpts_amplitude[k]= ctx->bvp->srcpts_amplitude[j];
	k++;
      }
    }
    /* quadrature order */
    ctx->sbvp[i]->quad_order= ctx->bvp->quad_order;
    /* Dirichlet strategy */
    ctx->sbvp[i]->diri_strat= ctx->bvp->diri_strat;
    ctx->sbvp[i]->tgv= ctx->bvp->tgv;
    /* load cell data for this bvp: needed for coarse dof */
    myerr= HelmBVPInitCellData(ctx->sbvp[i],dm); CHKMyErrQ(myerr);
    /* test: view this bvp */
#if 0
    HelmBVPViewOnScreen(ctx->sbvp[i],dm->comm);
#endif
    /* test: assembly of this bvp */
#if 0
    {
      Mat A=0; Vec b=0;
      myerr=HelmBVPAssembly(ctx->sbvp[i],ddof->subdof[i],&A,&b);CHKMyErrQ(myerr);
      MatDestroy(&A); VecDestroy(&b);
    }
#endif
  }
  PetscFunctionReturn(0);
}

/*
   Mark intersections (edges & vertices) on the original Dirichlet boundary.

   TODO: complete this
*/
#undef __FUNCT__
#define __FUNCT__  "FETIHelmSetDirichlet"
MyErrCode FETIHelmSetDirichlet(FETIHelm ctx)
{
  PetscFunctionBegin;

  PetscFunctionReturn(0);
}

/*
   Create coarse dof's of fetidof for solving the Helmholtz bvp.  But the coarse
   dof's are incomplete with idof_sub and idof_gc unsetted.  The coarse basis we
   give have not been filtered for linear independence.

   Note that the coarse dof is mastered by one neighboring subdomain and the
   other neighboring subdomains ghost them, which keeps the coarse dof unique in
   spite of the heterogeneous wavenumber.
*/
#undef __FUNCT__
#define __FUNCT__  "FETIHelmCreateCoarseDOF"
MyErrCode FETIHelmCreateCoarseDOF(FETIHelm ctx)
{
  PetscInt i, j, k, *nsubcf=0, *nsubce=0, *nsubcv=0;
  MyCoarseDOF cdof=0, **dof_cf=0, **dof_ce=0, **dof_cv=0;
  MyDDOF ddof=0;
  void *cctx=0;			/* coarse section context */
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if (!ctx->fetidof->has_section) return FETIDOFNeedsInput;
  if ((0==ctx->nwf && 0==ctx->nwe && 0==ctx->nwv) || 1==ctx->fetidof->ddof->sloc) {
    ctx->fetidof->has_coarse= PETSC_FALSE; return 0;
  }
  ddof= ctx->fetidof->ddof;
  ierr= PetscMalloc(ddof->sloc*sizeof(PetscInt),&nsubcf); CHKERRQ(ierr);
  ierr= PetscMalloc(ddof->sloc*sizeof(PetscInt),&nsubce); CHKERRQ(ierr);
  ierr= PetscMalloc(ddof->sloc*sizeof(PetscInt),&nsubcv); CHKERRQ(ierr);
  ierr= PetscMalloc(ddof->sloc*sizeof(MyCoarseDOF*),&dof_cf); CHKERRQ(ierr);
  ierr= PetscMalloc(ddof->sloc*sizeof(MyCoarseDOF*),&dof_ce); CHKERRQ(ierr);
  ierr= PetscMalloc(ddof->sloc*sizeof(MyCoarseDOF*),&dof_cv); CHKERRQ(ierr);
  ierr= PetscMemzero(nsubcf,ddof->sloc*sizeof(PetscInt)); CHKERRQ(ierr);
  ierr= PetscMemzero(nsubce,ddof->sloc*sizeof(PetscInt)); CHKERRQ(ierr);
  ierr= PetscMemzero(nsubcv,ddof->sloc*sizeof(PetscInt)); CHKERRQ(ierr);
  ierr= PetscMemzero(dof_cf,ddof->sloc*sizeof(MyCoarseDOF*)); CHKERRQ(ierr);
  ierr= PetscMemzero(dof_ce,ddof->sloc*sizeof(MyCoarseDOF*)); CHKERRQ(ierr);
  ierr= PetscMemzero(dof_cv,ddof->sloc*sizeof(MyCoarseDOF*)); CHKERRQ(ierr);
  for (i=0; i<ddof->sloc; ++i) {
    /* coarse basis on faces */
    if (ctx->nwf>0 && ctx->fetidof->nsf[i]) {
      nsubcf[i]= ctx->fetidof->nsf[i];
      ierr= PetscMalloc(nsubcf[i]*sizeof(MyCoarseDOF),dof_cf+i); CHKERRQ(ierr);
      for (j=0; j<nsubcf[i]; ++j) {
	MyCoarseDOFCreate(&cdof);
	cdof->isec= ctx->fetidof->dof_sf[i][j]->isec; cdof->primal= ctx->pwf;
	cdof->m= ctx->fetidof->dof_sf[i][j]->ndofin; cdof->n= ctx->nwf;
	ierr= PetscMalloc(cdof->m*(cdof->n)*sizeof(PetscScalar),&(cdof->Q)); CHKERRQ(ierr);
	PetscMemzero(cdof->Q,cdof->m*(cdof->n)*sizeof(PetscScalar));
	if (ctx->fetidof->dof_sf[i][j]->mastered) {
	  /* only set the mastered coarse basis */
	  ctx->createcfctx(&cctx,i,j,ctx);
	  ctx->cff(cdof->Q,cctx);
	  ctx->destroycfctx(&cctx);
	}
	dof_cf[i][j]= cdof; cdof= 0;
      }
    }
    /* coarse basis on edges */
    if (ctx->nwe>0 && ctx->fetidof->nse[i]) {
      nsubce[i]= 0;
      for (j=0; j<ctx->fetidof->nse[i]; ++j) {
	/* edge on the original Dirichlet has not a coarse dof */
	if (ctx->fetidof->dof_se[i][j]->dirichlet) continue;
	nsubce[i]++;
      }
      ierr= PetscMalloc(nsubce[i]*sizeof(MyCoarseDOF),dof_ce+i); CHKERRQ(ierr);
      for (k=0,j=0; j<ctx->fetidof->nse[i]; ++j) {
	if (ctx->fetidof->dof_se[i][j]->dirichlet) continue;
	MyCoarseDOFCreate(&cdof);
	cdof->isec= ctx->fetidof->dof_se[i][j]->isec; cdof->primal= ctx->pwe;
	cdof->m= ctx->fetidof->dof_se[i][j]->ndofin; cdof->n= ctx->nwe;
	ierr= PetscMalloc(cdof->m*(cdof->n)*sizeof(PetscScalar),&(cdof->Q)); CHKERRQ(ierr);
	PetscMemzero(cdof->Q,cdof->m*(cdof->n)*sizeof(PetscScalar));
	if (ctx->fetidof->dof_se[i][j]->mastered) {
	  /* set only the mastered coarse basis */
	  ctx->createcectx(&cctx,i,j,ctx);
	  ctx->cfe(cdof->Q,cctx);
	  ctx->destroycectx(&cctx);
	}
	dof_ce[i][k]= cdof; cdof= 0; ++k;
      }
    }
    /* coarse basis on vertices */
    if (ctx->nwv>0 && ctx->fetidof->nsv[i]) {
      nsubcv[i]= 0;
      for (j=0; j<ctx->fetidof->nsv[i]; ++j) {
	if (ctx->fetidof->dof_sv[i][j]->dirichlet) continue;
	nsubcv[i]++;
      }
      ierr=PetscMalloc(nsubcv[i]*sizeof(MyCoarseDOF),dof_cv+i);CHKERRQ(ierr);
      for (k=0,j=0; j<ctx->fetidof->nsv[i]; ++j) {
	if (ctx->fetidof->dof_sv[i][j]->dirichlet) continue;
	MyCoarseDOFCreate(&cdof);
	cdof->isec= ctx->fetidof->dof_sv[i][j]->isec; cdof->primal= ctx->pwv;
	cdof->m= 1;		/* scalar field, one dof at a vertex */
	cdof->n= 1;
	ierr= PetscMalloc(sizeof(PetscScalar),&(cdof->Q)); CHKERRQ(ierr);
	cdof->Q[0]= 1.0;	/* all neighboring subdomains have the same value */
	dof_cv[i][j]= cdof; cdof= 0; ++k;
      }      
    }
  }
  FETIDOFSetCoarse(ctx->fetidof,nsubcf,dof_cf,nsubce,dof_ce,nsubcv,dof_cv);
  ctx->fetidof->has_coarse= PETSC_TRUE;
  PetscFunctionReturn(0);
}
