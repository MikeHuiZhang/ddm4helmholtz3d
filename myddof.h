#ifndef _MYDDOF
#define _MYDDOF

#include <petscvec.h>
#include "myddm.h"
#include "mydof.h"

typedef enum
{
  MyDDOFRestricted,
  MyDDOFFull,
} MyDDOFRType;

typedef struct
{
  /*
     gdof->dm must be the same as ddm->gdm
  */
  MyDOF gdof; MyDDM ddm;

  /*
     number of subdomains on the proc, ==ddm->sloc
  */
  PetscInt sloc,sxloc,syloc,szloc;

  /*
     bounds of overlapping subdomain dof's, excluding DD pml but including the
     original pml, relative to the original domain with pml
  */
  PetscInt *dofxs, *dofxe, *dofys, *dofye, *dofzs, *dofze;

  /*
     bounds of restricted subdomain dof's excluding DD pml but including the
     original pml, relative to the original domain with pml
  */
  PetscInt *dofinxs, *dofinxe, *dofinys, *dofinye, *dofinzs, *dofinze;

  /*
     ndof of the original pml, for subdomains on this proc., this number should
     be zero for interior subdomains, referenced in each dimension, size: sxloc,
     syloc, szloc
  */
  PetscInt *ndofopmlxl, *ndofopmlxr, *ndofopmlyl, *ndofopmlyr, *ndofopmlzl, *ndofopmlzr;

  /*
     dof of subdomains
  */
  MyDOF *subdof;

  /*
     indices of subdomain's on proc. dof, excluding DD pml but including the
     original pml, relative to subdomain on proc. dof
  */
  IS *lis;

  /*
     translated indices of 'lis' to the "original domain with pml" in petsc
     ordering
  */
  IS *gis;

  /*
     'lis' for restricted subdomain's on proc. dof
  */
  IS *lis_in;

  /*
     'gis' for restricted subdomain's on proc. dof
  */
  IS *gis_in;

  /*
     VecScatters from gdof to subdomains' on proc. dof
  */
  VecScatter *scfull, *scres;

  /*
     status
  */
  PetscBool initialized, has_bounds, has_subdof, has_is, has_is_in, has_scatters;

} structMyDDOF, *MyDDOF;

typedef struct
{
  /*
       section number in subdomain

   0~5 for faces: left,right,front,back,bottom,top,

   0~11 for edges: (x,0,0), (x,0,1), (x,1,0), (x,1,1), (0,y,0), ...

   0~8 for vertices: (0,0,0), (0,0,1), (0,1,0), (0,1,1), ...

  */
  PetscInt isec;

  /*
     coarse basis, Q is m-by-n, columns are new dof's
  */
  PetscScalar *Q;
  PetscInt m, n;

  /*
     are these dof's primal?
  */
  PetscBool primal;

  /*
     subdomain numbering of the old dof's that will be replaced by columns of Q
     if these are primal, otherwise null
  */
  PetscInt *idof_sub;

  /*
     numbering of the coarse dof's in all the coarse dof's shared by this
     subdomain
  */
  PetscInt *idof_sc;

  /*
     numbering of the new dof's in the global coarse coefficient vector
  */
  PetscInt *idof_gc;

} structMyCoarseDOF, *MyCoarseDOF;

typedef struct
{
  /*
     section number relative to subdomain
  */
  PetscInt isec;

  /*
     dual dof of the section, indexed relative to subdomain
  */
  PetscInt ndof, *idof_sub;

  /*
     number of neighbors excluding myself
  */
  PetscInt nnb;

  /*
     sign for myself relative to each neighbor
  */
  PetscInt *sign;

/*
   index bounds of Lagrange multipliers with each neighbor, numbered in global
   Lagrange multipliers
*/
  PetscInt *idofs_gl, *idofe_gl;

} structMyDualDOF, *MyDualDOF;

typedef struct
{
  PetscInt isec;
  PetscInt i1s, i1e, i2s, i2e;	/* (x,y), (y,z) or (x,z) */
  PetscInt ndofin, *dofin;	/* relative to subdomain's on proc dof */
  PetscInt nnb;
  PetscBool dirichlet;		/* on the original Dirichlet? */
  PetscBool mastered;		/* the section is mastered by this subdomain? */
} structMySectionDOF, *MySectionDOF;

extern MyErrCode MyDDOFCreate(MyDDOF*);
extern MyErrCode MyDDOFSetGDOF(MyDDOF ddof, MyDOF gdof);
extern MyErrCode MyDDOFSetDDM(MyDDOF ddof, MyDDM ddm);
extern MyErrCode MyDDOFSetGDOFDDM(MyDDOF ddof, MyDOF gdof, MyDDM ddm);
extern MyErrCode MyDDOFSetup(MyDDOF);
extern MyErrCode MyDDOFDestroy(MyDDOF*);
extern MyErrCode MyDDOFCreateScatters(MyDDOF, MyDDOFRType);
extern MyErrCode MyVecGetLocalSubVector(Vec v, Vec *vl);
extern MyErrCode MyVecRestoreLocalSubVector(Vec v, Vec *vl);
extern MyErrCode MyCoarseDOFCreate(MyCoarseDOF *p_dof);
extern MyErrCode MyCoarseDOFDestroy(MyCoarseDOF *p_dof);
extern MyErrCode MyDualDOFCreate(MyDualDOF *p_dof);
extern MyErrCode MyDualDOFDestroy(MyDualDOF *p_dof);
extern MyErrCode MySectionDOFCreate(MySectionDOF *p_dof);
extern MyErrCode MySectionDOFDestroy(MySectionDOF *p_dof);

#endif
