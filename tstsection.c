  {
    Vec v=0, *vi=0, vl=0;
    PetscScalar *a=0;
    PetscInt i, j, k, idx, low, high;
    ierr=PetscMalloc(ddof->sloc*sizeof(Vec),&vi);CHKERRQ(ierr);
    myerr=MyDDOFCreateScatters(ddof,MyDDOFFull);CHKMyErrQ(myerr);
    myerr=MyDDOFCreateScatters(ddof,MyDDOFRestricted);CHKMyErrQ(myerr);
    VecDuplicate(dof->v,&v); VecGetOwnershipRange(v,&low,&high);
    VecGetArray(v,&a);
    for (i=low;i<high;++i) a[i-low]= i;
    VecRestoreArray(v,&a); //VecAssemblyBegin(v); VecAssemblyEnd(v);
//    VecView(v,PETSC_VIEWER_STDOUT_WORLD);
    for (i=0;i<ddof->sloc;++i) {
      VecDuplicate(ddof->subdof[i]->v,vi+i);
      VecGetOwnershipRange(vi[i],&low,&high);
      VecSet(vi[i],-1);
      MyVecGetLocalSubVector(vi[i],&vl);
      VecScatterBegin(ddof->scfull[i],v,vl,INSERT_VALUES,SCATTER_FORWARD);
      VecScatterEnd(ddof->scfull[i],v,vl,INSERT_VALUES,SCATTER_FORWARD);
      MyVecRestoreLocalSubVector(vi[i],&vl);
    }
    VecSet(v,0);
    for (i=0;i<ddof->sloc;++i) {
      VecGetArray(vi[i],&a);
      for (j=0;j<fetidof->nsf[i];++j) {
	for (k=0;k<fetidof->dof_sf[i][j]->ndofin;++k) {
	  idx= fetidof->dof_sf[i][j]->dofin[k];
	  a[idx]= a[idx]/(fetidof->dof_sf[i][j]->nnb+1);
	}
      }
      for (j=0;j<fetidof->nse[i];++j) {
	for (k=0;k<fetidof->dof_se[i][j]->ndofin;++k) {
	  idx= fetidof->dof_se[i][j]->dofin[k];
	  a[idx]= a[idx]/(fetidof->dof_se[i][j]->nnb+1);
	}
      }
      for (j=0;j<fetidof->nsv[i];++j) {
	for (k=0;k<fetidof->dof_sv[i][j]->ndofin;++k) {
	  idx= fetidof->dof_sv[i][j]->dofin[k];
	  /* printf("proc=%d,i=%d,nsv=%d,isec=%d,idx=%d,nnb=%d\n",ddof->subdof[i]->dm->rank,i,fetidof->nsv[i],fetidof->dof_sv[i][j]->isec,idx,fetidof->dof_sv[i][j]->nnb); */
	  a[idx]= a[idx]/(fetidof->dof_sv[i][j]->nnb+1);
	}
      }
      VecRestoreArray(vi[i],&a);
      MyVecGetLocalSubVector(vi[i],&vl);
      VecScatterBegin(ddof->scfull[i],vl,v,ADD_VALUES,SCATTER_REVERSE);
      VecScatterEnd(ddof->scfull[i],vl,v,ADD_VALUES,SCATTER_REVERSE);
      MyVecRestoreLocalSubVector(vi[i],&vl);
      VecDestroy(vi+i);
    }
    PetscFree(vi);
    VecView(v,PETSC_VIEWER_STDOUT_WORLD);
  }
