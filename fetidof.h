#ifndef _FETIDOF
#define _FETIDOF

#include "myddof.h"

#define FETIDOF_MAX_NAMELEN 100

/*
   Note. Direct 'typedef struct' won't work because of function members with
   parameters pointing to the said struct.  It works only with a plain struct
   definition.
*/
struct FETIDOF_
{
  /*
     name, prefix
  */
  char *myname;
  char *prefix;

  /*
     status
  */
  PetscBool initialized, has_section, has_coarse;

  /*
     MyDDOF: assumed to have neither pml nor overlap
  */
  MyDDOF ddof;

  /* -------------------------------------------------------------------------
               intersection dof 

    These dof's are the only places with non-one weights in recoverging the
    global solution.  All the intersections of subdomains are counted including
    edges and vertices on the original boundary.
    --------------------------------------------------------------------------- */

  /*
     numbers of intersections of subdomains, only intersections shared by this
     proc. are counted
  */
  PetscInt *nsf, *nse, *nsv;

  /* 
     dof's on each intersection of each subdomain
  */
  MySectionDOF **dof_sf, **dof_se, **dof_sv; 

  /* -------------------------------------------------------------------------
               primal/dual interface coarse dof

     A primal interface dof must be completely representable in each of
     neighboring subdomains.  In other words, all the original dof's involved to
     represent a primal dof must have the same set of neighboring subdomains.
     In the case of cartesian domain decomposition, vertices, edge inteior and
     face interior must be treated separately.

     The original Dirichlet dof can __not__ be primal.  One can query from
     MySectionDOF whether a section is on the original Dirichlet boundary.

     We store primal dofs 'Q' in all subdomains sharing it.  The consistency
     between two neighboring subdomains must be maintained in some way.

     A primal dof is described by a new basis matrix T defined on an
     intersection.  Given some new basis vectors Q, we need to complete Q to the
     full columns basis matrix T, which can be done by first initializing T to
     the identity matrix and let each column vector q of Q to replace the column
     in T corresponding to the largest component of q (in other words, the
     chosen column of T has the smallest angle to q).  The original vector u is
     represented by u = T unew, where unew is the coefficient vector of u under
     the basis T.

     Change of basis will be done at the subdomain level.

     We assume the dual coarse dof is also compactly supported in each
     intersection.  So Q*FQ is block sparse.
    --------------------------------------------------------------------------- */

  /*
     global coarse coefficient Vec: primal and dual
  */
  Vec vc;
  IS isp, isdc;

  /*
     subdomain coarse Vec's: primal and dual
     
     vsp[i] is the primal Vec restricted to subdomain i, to be multiplied with
     A_{rp}^{i}, so the partition of vsp[i] should be the same as the partition
     of the columns of A_{rp}^{i}, here we assume proc. 0 of subdomains obtain
     all vsp[i] and the sizes on the other proc. are zero.
  */
  Vec *vsp, *vsdc;

  /*
     scatters from global coarse to subdomain primal and dual coarse
  */
  VecScatter *gc2sp, *gc2sdc;

  /*
     coarse face interior dof
  */
  PetscInt *nsubcf;		/* numbers of coarse faces for each subdomain,
				 * allow multiple MyCoarseDOF defined on the
				 * same intersection and all are counted */
  MyCoarseDOF **dof_subcf;	/* coarse face dof's on each coarse face of
				 * each subdomain */
  PetscInt *ndof_subcf;		/* numbers of coarse face dof's for each subdomain */
  PetscInt ndof_gcf;		/* number of global coarse face dof's */

  /*
     coarse edge inteior dof
  */
  PetscInt *nsubce;		/* numbers of coarse edges for each subdomain */
  MyCoarseDOF **dof_subce;	/* coarse edge dof's on each coarse edge of each
				 * subdomain */
  PetscInt *ndof_subce;		/* numbers of coarse edge dof's for each subdomain */
  PetscInt ndof_gce;		/* number of global coarse edge dof's */

  /*
     coarse vertex dof
  */
  PetscInt *nsubcv;
  MyCoarseDOF **dof_subcv;
  PetscInt *ndof_subcv;
  PetscInt ndof_gcv;

  /* -------------------------------------------------------------------------
            dual interface dof

     These places are where the Lagrange multipliers are defined.

     For an edge interior dof or a vertex dof, we always use redundant Lagrange
     multipliers, i.e. one for each pair of neighboring subdomains-- total N
     select 2 pairs if N is the number of subdomains adjacent to the dof.

     A special case is dof's at original Dirichlet boundary, which are __not__
     counted as dual dof.  So we need to know whether an intersection is on the
     original Dirichlet, which can be queried from the corresponding
     intersection dof MySectionDOF.

     We assume that dof's in each intersection are in the same order as they are
     in subdomains.  This simplifies the map between global dual and subdomains.

   --------------------------------------------------------------------------- */

  /*
     global Vec of Lagrange multipliers
  */
  Vec vl;

  /*
     scatters from global Lagrange multipliers to subdomain non-primal
     dof (put zeros for non-shared dof)
  */
  VecScatter *gl2snp;

  /*
     dual face interior dof
  */
  PetscInt *nsubdf;		/* numbers of dual faces of subdomains */
  MyDualDOF **dof_subdf;	/* dual dof's on each dual face of each subdomain */
  PetscInt *ndof_subdf;		/* numbers of dual face dof's on each subdomain */
  PetscInt ndof_glf;		/* number of global face Lagrange multipliers */

  /*
     dual edge interior dof

     The word 'dual' might be mis-leading here.  Usually each of the original
     edge dof is quadrupled.
  */
  PetscInt *nsubde;		/* numbers of dual edges of subdomains */
  MyDualDOF **dof_subde;	/* dual dof's on each dual edge of each subdomain */
  PetscInt *ndof_subde;		/* numbers of dual edge dof's on each subdomain */  
  PetscInt ndof_gle;		/* number of global edge Lagrange multipliers */

  /*
     dual vertex dof
  */
  PetscInt *nsubdv;		/* numbers of dual vertices of subdomains */
  MyDualDOF **dof_subdv;	/* a dual dof on each vertex of each subdomain */
  PetscInt *ndof_subdv;		/* numbers of dual vertex dof of subdomains */
  PetscInt ndof_glv;

  /* -------------------------------------------------------------------------
               non-shared dof

    This is useful for Dirichlet preconditioner.  The dof's on the original
    Dirichlet boundary are included here.
    --------------------------------------------------------------------------- */

  PetscInt **idof_si;		/* numbering in each subdomain */

};

typedef struct FETIDOF_ structFETIDOF, *FETIDOF;

extern MyErrCode FETIDOFCreate(FETIDOF *p_fetidof);
extern MyErrCode FETIDOFDestroy(FETIDOF *p_fetidof);
extern MyErrCode FETIDOFSetDDOF(FETIDOF fetidof, MyDDOF ddof);
extern MyErrCode FETIDOFSetPrefix(FETIDOF dof,const char prefix[]);
extern MyErrCode FETIDOFSetDirichlet(FETIDOF fetidof,MyErrCode (*f)(struct FETIDOF_*,void*),void*ctx);
extern MyErrCode FETIDOFSetupSection(FETIDOF fetidof);
extern MyErrCode FETIDOFSetCoarse(FETIDOF,PetscInt *ndof_cf,MyCoarseDOF **dof_cf,PetscInt *ndof_ce,MyCoarseDOF **dof_ce,PetscInt *ndof_cv,MyCoarseDOF **dof_cv);
extern MyErrCode FETIDOFSetup(FETIDOF fetidof);
#endif
