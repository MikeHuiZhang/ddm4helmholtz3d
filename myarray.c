#include "myarray.h"

/*
   An analogy of Matlab's sub2ind.  But the base index here is zero.
   We first count along z, then y, x, compatible with the SEG-SALT data.
*/
inline MyErrCode MyArraysub2ind(PetscInt xlen, PetscInt ylen, PetscInt zlen, PetscInt ix, PetscInt iy, PetscInt iz, PetscInt *ind)
{
  if(ix<0 || ix>=xlen || iy<0 || iy>=ylen || iz<0 || iz>=zlen){return IllegalIndex;}
  *ind= ix*ylen*zlen + iy*zlen + iz;
  return 0;
}

inline MyErrCode MyArrayind2sub(PetscInt xlen, PetscInt ylen, PetscInt zlen, PetscInt *ix, PetscInt *iy, PetscInt *iz, PetscInt ind)
{
  if(ind<0 || ind>=xlen*ylen*zlen){return IllegalIndex;}
  *iz= ind%zlen;
  *iy= (ind/zlen)%ylen;
  *ix= ind/(ylen*zlen);
  return 0;
}


/*
   Create an IS for a 3D subarray in comm.

   nx, ny, nz -- size of the (logical) 3D array

   xs, xe -- start and end (last index + 1) indices

   IS -- 1D indices, counting along z first then y, x

   comm -- communicator of IS
*/
#undef __FUNCT__
#define __FUNCT__ "MyArraySubIS"
MyErrCode MyArraySubIS(IS *subis, MPI_Comm comm, PetscInt nx, PetscInt ny, PetscInt nz, PetscInt xs, PetscInt xe, PetscInt ys, PetscInt ye, PetscInt zs, PetscInt ze)
{
  PetscInt i=0, j= 0, ix, iy, iz, *idx=0; 
  PetscErrorCode ierr;
  MyErrCode myerr;
  const PetscInt subn= (xe-xs)*(ye-ys)*(ze-zs);
  
  PetscFunctionBegin;
  if (comm==MPI_UNDEFINED) return CommUndefined;
  if (xs<0 || xs>nx-1 || xe<0 || xe>nx || xs>=xe) return IllegalIndex;
  if (ys<0 || ys>ny-1 || ye<0 || ye>ny || ys>=ye) return IllegalIndex;
  if (zs<0 || zs>nz-1 || ze<0 || ze>nz || zs>=ze) return IllegalIndex;
  ierr= PetscMalloc(subn*sizeof(PetscInt),&idx); CHKERRQ(ierr);
  for (ix = xs; ix < xe; ++ix) {
    for (iy = ys; iy < ye; ++iy) {
      myerr= MyArraysub2ind(nx,ny,nz,ix,iy,zs,&i); CHKMyErrQ(myerr);
      for (iz = zs; iz < ze; ++iz) {
	idx[j++]= i++;
      }
    }
  }
  ierr= ISCreateGeneral(comm,subn,idx,PETSC_OWN_POINTER,subis); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
   Extract a subarray of type PetscScalar.
*/
#undef __FUNCT__
#define __FUNCT__  "MyArraySubArray"
MyErrCode MyArraySubArray(PetscScalar **subarray,PetscScalar array[],PetscInt nx,PetscInt ny,PetscInt nz,PetscInt xs,PetscInt xe, PetscInt ys,PetscInt ye,PetscInt zs,PetscInt ze)
{
  PetscInt i=0, j=0, ix, iy, iz;
  PetscErrorCode ierr;
  MyErrCode myerr;
  const PetscInt subn= (xe-xs)*(ye-ys)*(ze-zs);
  
  PetscFunctionBegin;
  if (xs<0 || xs>nx-1 || xe<0 || xe>nx || xs>=xe) return IllegalIndex;
  if (ys<0 || ys>ny-1 || ye<0 || ye>ny || ys>=ye) return IllegalIndex;
  if (zs<0 || zs>nz-1 || ze<0 || ze>nz || zs>=ze) return IllegalIndex;
  ierr= PetscMalloc(subn*sizeof(PetscScalar),subarray); CHKERRQ(ierr);
  myerr= MyArraysub2ind(nx,ny,nz,xs,ys,zs,&i); CHKMyErrQ(myerr);
  for (ix = xs; ix < xe; ++ix) {
    for (iy = ys; iy < ye; ++iy) {
      myerr= MyArraysub2ind(nx,ny,nz,ix,iy,zs,&i); CHKMyErrQ(myerr);
      for (iz = zs; iz < ze; ++iz) {
	(*subarray)[j++]= array[i++];
      }
    }
  }
  PetscFunctionReturn(0);
}
