f= 2.0; w= 2*pi*f;
nx= 25; ny= nx; nz= nx;
x= linspace(0,1,nx+1); y= linspace(0,1,ny+1); z= linspace(0,1,nz+1);
[X Y Z]= meshgrid(x,y,z);
r= sqrt((X-0.5).^2+(Y-0.5).^2+(Z-0.5).^2);
uu= exp(1i*w*r)./r/4/pi;

eta= 5*ones(1,6);
% $$$ upetsc= PetscBinaryRead('solutions/unitf2/uT2.bin','complex',true);
upetsc= PetscBinaryRead('u.bin','complex',true);
upetsc= reshape(upetsc,nz+eta(5)+eta(6)+1,ny+eta(3)+eta(4)+1,nx+eta(1)+ ...
                eta(2)+1);
upetsc= permute(upetsc,[2 3 1]);
upetsc= upetsc(eta(3)+1:eta(3)+ny+1,eta(1)+1:eta(1)+nx+1,eta(5)+1:eta(5)+nz+1);

max(abs(uu(:)-upetsc(:)))
max(abs(conj(uu(:))-upetsc(:)))
norm(uu(:)-upetsc(:))
figure, subplot(1,2,1), surf(x,y,real(uu(:,:,10)))
subplot(1,2,2), surf(x,y,real(upetsc(:,:,10)))
figure, surf(x,y,real(uu(:,:,10)-upetsc(:,:,10)))