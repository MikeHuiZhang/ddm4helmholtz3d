#ifndef _MYDDM
#define _MYDDM

#include "mydm.h"

#define MYDDM_MAX_NAMELEN 100

typedef struct
{
  /*
     name, prefix
  */
  char *myname;
  char *prefix;
  
  /*
     status
  */
  PetscBool has_numsub,has_extension,has_partition,has_interface,initialized;
  
  /*
     original DM (distributed mesh)
  */
  MyDM gdm;

  /*
     number of global subdomains in each direction, assuming divides or is
     dividable by px, py, pz, resp.
  */
  PetscInt sx, sy, sz;

  /*
     full overlap size and pml size counted by number of elements along normal
     direction in the region, overlap == sum of numbers of extended physcial
     elements from the left and the right
  */
  PetscInt nolp, npml;

  /*
     for each subdomain we create a sub-DM and create or *refer* to a
     sub-communicator, think first in each direcion then tensor product
  */

  /* 
     num. of subdomains that use this proc. 
  */
  PetscInt sxloc, syloc, szloc, sloc;

  /* 
     numbering subdomains in 3D and define subdomains that use this proc 
  */
  PetscInt isxs, isxe, isys, isye, iszs, isze;

  /* 
     num. of proc for each subdomain, same for all subdomains because we assume
     either px|sx or sx|px
  */
  PetscInt sp, spx, spy, spz;

  /* 
     1D partition and search functions 
  */
  void (*part1d)(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *nxp, PetscInt *startx,PetscInt *endx);
  void (*search1d)(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *rx, PetscInt *startx,PetscInt *endx);

  /* 
     element bounds of non-overlapping physical subdomains intersecting this
     processor, relative to the original __physical__ domain, reference by one
     of 3D subscripts of the subdomain, e.g. xs_in[i] is for subdomains of
     number i in x direction
  */
  PetscInt *xs_in, *xe_in, *ys_in, *ye_in, *zs_in, *ze_in;

  /* 
     element bounds of overlapping physical subdomains intersecting this proc.,
     relative to the original __physical__ domain, reference in each dimension
  */
  PetscInt *xs, *xe, *ys, *ye, *zs, *ze;

  /*
    original pml sizes in each dimension, for overlapping subdomains on this
    proc., this is zero for interior subdomains 
  */
  PetscInt *opmlxl, *opmlxr, *opmlyl, *opmlyr, *opmlzl, *opmlzr;

  /* 
     on each subdomain, there are bounds for this processor, subdomains are
     ordered first in z, then in y, x 
  */
  MyDM *subdm;

} structMyDDM, *MyDDM;

extern MyErrCode MyDDMCreate(MyDDM *p_ddm);

extern MyErrCode MyDDMDestroy(MyDDM *p_ddm);

extern MyErrCode MyDDMSetFromOptions(MyDDM ddm,MyDM gdm,char filename[],char prefix[]);

extern MyErrCode MyDDMComputePartition(MyDDM ddm);

extern MyErrCode MyDDMSetup(MyDDM ddm);

extern MyErrCode MyDDMViewOnScreen(MyDDM ddm);

#endif
