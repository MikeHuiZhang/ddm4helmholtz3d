#include "fetidof.h"

#undef __FUNCT__
#define __FUNCT__ "FETIDOFCreate"
MyErrCode FETIDOFCreate(FETIDOF *p_fetidof)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr=PetscMalloc(sizeof(structFETIDOF),p_fetidof);CHKERRQ(ierr);
  PetscMemzero(*p_fetidof,sizeof(structFETIDOF));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FETIDOFDestroy"
MyErrCode FETIDOFDestroy(FETIDOF *p_fetidof)
{
  PetscInt i,j;
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if (!*p_fetidof) return(0);
  /* vec and scatters */
  ierr=VecDestroy(&((*p_fetidof)->vc));CHKERRQ(ierr);
  ierr=VecDestroy(&((*p_fetidof)->vl));CHKERRQ(ierr);
  if ((*p_fetidof)->vsp) {
    for (i=0; i<(*p_fetidof)->ddof->sloc;++i) {
      ierr=VecDestroy((*p_fetidof)->vsp+i);CHKERRQ(ierr);
    }
    PetscFree((*p_fetidof)->vsp);
  }
  if ((*p_fetidof)->vsdc) {
    for (i=0; i<(*p_fetidof)->ddof->sloc;++i) {
      ierr=VecDestroy((*p_fetidof)->vsdc+i);CHKERRQ(ierr);
    }
    PetscFree((*p_fetidof)->vsdc);
  }  
  if ((*p_fetidof)->gc2sp) {
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      ierr=VecScatterDestroy((*p_fetidof)->gc2sp+i);CHKERRQ(ierr);
    }
    PetscFree((*p_fetidof)->gc2sp);
  }
  if ((*p_fetidof)->gc2sdc) {
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      ierr=VecScatterDestroy((*p_fetidof)->gc2sdc+i);CHKERRQ(ierr);
    }
    PetscFree((*p_fetidof)->gc2sdc);
  }
  if ((*p_fetidof)->gl2snp)
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      ierr=VecScatterDestroy((*p_fetidof)->gl2snp+i);CHKERRQ(ierr);
    }
  /* coarse dof */
  if ((*p_fetidof)->nsubcf)
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      for (j=0;j<(*p_fetidof)->nsubcf[i];++j) {
	MyCoarseDOFDestroy((*p_fetidof)->dof_subcf[i]+j);
      }
      ierr=PetscFree((*p_fetidof)->dof_subcf[i]);CHKERRQ(ierr);
    }
  if ((*p_fetidof)->nsubce)
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      for (j=0;j<(*p_fetidof)->nsubce[i];++j) {
	MyCoarseDOFDestroy((*p_fetidof)->dof_subce[i]+j);
      }
      PetscFree((*p_fetidof)->dof_subce[i]);
    }
  if ((*p_fetidof)->nsubcv)
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      for (j=0;j<(*p_fetidof)->nsubcv[i];++j) {
	MyCoarseDOFDestroy((*p_fetidof)->dof_subcv[i]+j);
      }
      PetscFree((*p_fetidof)->dof_subcv[i]);
    }
  /* dual dof */
  if ((*p_fetidof)->nsubdf)
  for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
    for (j=0;j<(*p_fetidof)->nsubdf[i];++j) {
      MyDualDOFDestroy((*p_fetidof)->dof_subdf[i]+j);
    }
    PetscFree((*p_fetidof)->dof_subdf[i]);
  }
  if ((*p_fetidof)->nsubde)
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      for (j=0;j<(*p_fetidof)->nsubde[i];++j) {
	MyDualDOFDestroy((*p_fetidof)->dof_subde[i]+j);
      }
      PetscFree((*p_fetidof)->dof_subde[i]);
    }
  if ((*p_fetidof)->nsubdv)
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      for (j=0;j<(*p_fetidof)->nsubdv[i];++j) {
	MyDualDOFDestroy((*p_fetidof)->dof_subdv[i]+j);
      }
      PetscFree((*p_fetidof)->dof_subdv[i]);
    }
  /* non-shared dof */
  if ((*p_fetidof)->idof_si)
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      PetscFree((*p_fetidof)->idof_si[i]);
    }
  /* section dof */
  if ((*p_fetidof)->nsf)
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      for (j=0;j<(*p_fetidof)->nsf[i];++j) {
	MySectionDOFDestroy((*p_fetidof)->dof_sf[i]+j);
      }
      PetscFree((*p_fetidof)->dof_sf[i]);
    }
  if ((*p_fetidof)->nse)
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      for (j=0;j<(*p_fetidof)->nse[i];++j) {
	MySectionDOFDestroy((*p_fetidof)->dof_se[i]+j);
      }
      PetscFree((*p_fetidof)->dof_se[i]);
    }
  if ((*p_fetidof)->nsv)
    for (i=0;i<(*p_fetidof)->ddof->sloc;++i) {
      for (j=0;j<(*p_fetidof)->nsv[i];++j) {
	MySectionDOFDestroy((*p_fetidof)->dof_sv[i]+j);
      }
      PetscFree((*p_fetidof)->dof_sv[i]);
    }
  PetscFree((*p_fetidof)->dof_subcf);
  PetscFree((*p_fetidof)->dof_subce);
  PetscFree((*p_fetidof)->dof_subcv);
  PetscFree((*p_fetidof)->dof_subdf);
  PetscFree((*p_fetidof)->dof_subde);
  PetscFree((*p_fetidof)->dof_subdv);
  PetscFree((*p_fetidof)->ndof_subcf); PetscFree((*p_fetidof)->nsubcf);
  PetscFree((*p_fetidof)->ndof_subce); PetscFree((*p_fetidof)->nsubce);
  PetscFree((*p_fetidof)->ndof_subcv); PetscFree((*p_fetidof)->nsubcv);
  PetscFree((*p_fetidof)->ndof_subdf); PetscFree((*p_fetidof)->nsubdf);
  PetscFree((*p_fetidof)->ndof_subde); PetscFree((*p_fetidof)->nsubde);
  PetscFree((*p_fetidof)->ndof_subdv); PetscFree((*p_fetidof)->nsubdv);
  PetscFree((*p_fetidof)->idof_si);
  PetscFree((*p_fetidof)->dof_sf); PetscFree((*p_fetidof)->nsf);
  PetscFree((*p_fetidof)->dof_se); PetscFree((*p_fetidof)->nse);
  PetscFree((*p_fetidof)->dof_sv); PetscFree((*p_fetidof)->nsv);
  PetscFree(*p_fetidof);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "FETIDOFSetPrefix"
inline MyErrCode FETIDOFSetPrefix(FETIDOF dof,const char prefix[])
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if(prefix){
    if(prefix[0]) {
      if (!(dof->prefix)) {
	ierr= PetscMalloc(FETIDOF_MAX_NAMELEN*sizeof(char),&(dof->prefix));
	CHKERRQ(ierr);
      }
      PetscStrcpy(dof->prefix,prefix);
    }
    else PetscFree(dof->prefix); /* set to PETSC_NULL */
  }
  else PetscFree(dof->prefix);
  PetscFunctionReturn(0);
}

inline MyErrCode FETIDOFSetDDOF(FETIDOF fetidof, MyDDOF ddof)
{
  fetidof->ddof= ddof; fetidof->initialized= PETSC_FALSE; return 0;
}

inline MyErrCode FETIDOFSetCoarse(FETIDOF fetidof,PetscInt *nsubcf,MyCoarseDOF **dof_cf,PetscInt *nsubce,MyCoarseDOF **dof_ce,PetscInt *nsubcv,MyCoarseDOF **dof_cv)
{
  fetidof->nsubcf= nsubcf; fetidof->nsubce= nsubce; fetidof->nsubcv=nsubcv;
  fetidof->dof_subcf= dof_cf; fetidof->dof_subce= dof_ce; fetidof->dof_subcv= dof_cv;
  return 0;
}

/*
   Note. In principle, augmenting pml to subdomains should be allowed which is
   another form of regularization different from Robin.  But is the high-order
   regularization useful?
*/
#undef __FUNCT__
#define __FUNCT__ "FETIDOFSetupSection"
MyErrCode FETIDOFSetupSection(FETIDOF fetidof)
{
  PetscInt i, ix, iy, iz, j, k, kx, ky, kz, *nnb=0, xms[2], yms[2], zms[2];
  PetscErrorCode ierr;
  MyDDOF ddof=0;
  MyDOF subdof=0;
  MySectionDOF sec=0;
  PetscBool xml[2], yml[2], zml[2];
  PetscFunctionBegin;
  if (!fetidof->ddof || !fetidof->ddof->initialized) return FETIDOFNeedsInput;
  if (fetidof->ddof->ddm->nolp) return FETIDOFCannotOverlap;
  if (fetidof->ddof->ddm->npml) return FETIDOFPMLNotImpl;
  ddof= fetidof->ddof;
  ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(fetidof->nsf));CHKERRQ(ierr);
  ierr=PetscMemzero(fetidof->nsf,ddof->sloc*sizeof(PetscInt));CHKERRQ(ierr);
  ierr=PetscMalloc(ddof->sloc*sizeof(MySectionDOF*),&(fetidof->dof_sf));CHKERRQ(ierr);
  ierr=PetscMemzero(fetidof->dof_sf,ddof->sloc*sizeof(MySectionDOF*));CHKERRQ(ierr);
  ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(fetidof->nse));CHKERRQ(ierr);
  ierr=PetscMemzero(fetidof->nse,ddof->sloc*sizeof(PetscInt));CHKERRQ(ierr);
  ierr=PetscMalloc(ddof->sloc*sizeof(MySectionDOF*),&(fetidof->dof_se));CHKERRQ(ierr);
  ierr=PetscMemzero(fetidof->dof_se,ddof->sloc*sizeof(MySectionDOF*));CHKERRQ(ierr);
  ierr=PetscMalloc(ddof->sloc*sizeof(PetscInt),&(fetidof->nsv));CHKERRQ(ierr);
  ierr=PetscMemzero(fetidof->nsv,ddof->sloc*sizeof(PetscInt));CHKERRQ(ierr);
  ierr=PetscMalloc(ddof->sloc*sizeof(MySectionDOF*),&(fetidof->dof_sv));CHKERRQ(ierr);
  ierr=PetscMemzero(fetidof->dof_sv,ddof->sloc*sizeof(MySectionDOF*));CHKERRQ(ierr);
  ierr= PetscMalloc(12*sizeof(PetscInt),&nnb);CHKERRQ(ierr);
  for (i=0; i<ddof->sloc; ++i) {
    MyArrayind2sub(ddof->sxloc,ddof->syloc,ddof->szloc,&ix,&iy,&iz,i);
    ix+= ddof->ddm->isxs; iy+= ddof->ddm->isys; iz+= ddof->ddm->iszs;
    subdof= ddof->subdof[i];
    xml[0]= (subdof->dofxins==0);
    xml[1]= (subdof->dofxine==subdof->ndof_x);
    yml[0]= (subdof->dofyins==0);
    yml[1]= (subdof->dofyine==subdof->ndof_y);
    zml[0]= (subdof->dofzins==0);
    zml[1]= (subdof->dofzine==subdof->ndof_z);
    xms[0]= (ix>0 && xml[0]?1:0);
    xms[1]= (ix<ddof->ddm->sx-1 && xml[1]?1:0);
    yms[0]= (iy>0 && yml[0]?1:0);
    yms[1]= (iy<ddof->ddm->sy-1 && yml[1]?1:0);
    zms[0]= (iz>0 && zml[0]?1:0);
    zms[1]= (iz<ddof->ddm->sz-1 && zml[1]?1:0);
    /* printf("proc=%d,i=%d,xms:%d,%d, yms:%d,%d, zms:%d,%d\n",subdof->dm->rank,i,xms[0],xms[1],yms[0],yms[1],zms[0],zms[1]); */
    /* count number of faces shared with neighbors */
    fetidof->nsf[i]= xms[0]+xms[1]+yms[0]+yms[1]+zms[0]+zms[1];
    if (fetidof->nsf[i]) {
      /* compute faces between subdomains */
      ierr=PetscMalloc(fetidof->nsf[i]*sizeof(MySectionDOF),fetidof->dof_sf+i);CHKERRQ(ierr);
      j= 0;
      if (xms[0]) {
	MySectionDOFCreate(&sec);
	sec->isec= 0; sec->nnb= 1; sec->mastered= PETSC_TRUE;
	sec->i1s= (subdof->dofyins?0:1);
	sec->i2s= (subdof->dofzins?0:1);
	sec->i1e= subdof->ndof_yin-(subdof->dofyine!=subdof->ndof_y?0:1);
	sec->i2e= subdof->ndof_zin-(subdof->dofzine!=subdof->ndof_z?0:1);
	sec->ndofin= (sec->i1e - sec->i1s)*(sec->i2e - sec->i2s);
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, ky=sec->i1s;ky<sec->i1e;++ky)
	  for (kz=sec->i2s;kz<sec->i2e;++kz, ++k) {
	    sec->dofin[k]= ky*(subdof->ndof_zin)+kz;
	  }
	fetidof->dof_sf[i][j]= sec; sec= 0; j++;
      }
      if (xms[1]) {
	MySectionDOFCreate(&sec);
	sec->isec= 1; sec->nnb= 1; sec->mastered= PETSC_FALSE;
	sec->i1s= (subdof->dofyins?0:1);
	sec->i2s= (subdof->dofzins?0:1);
	sec->i1e= subdof->ndof_yin-(subdof->dofyine!=subdof->ndof_y?0:1);
	sec->i2e= subdof->ndof_zin-(subdof->dofzine!=subdof->ndof_z?0:1);
	sec->ndofin= (sec->i1e-sec->i1s)*(sec->i2e-sec->i2s);
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, ky=sec->i1s;ky<sec->i1e;++ky)
	  for (kz=sec->i2s;kz<sec->i2e;++kz, ++k) {
	    sec->dofin[k]= (subdof->ndof_xin-1)*(subdof->ndof_yin)*(subdof->ndof_zin)
	      +ky*(subdof->ndof_zin)+kz;
	  }
	fetidof->dof_sf[i][j]= sec; sec= 0; j++;
      }
      if (yms[0]) {
	MySectionDOFCreate(&sec);
	sec->isec= 2; sec->nnb= 1; sec->mastered= PETSC_TRUE;
	sec->i1s= (subdof->dofxins?0:1);
	sec->i2s= (subdof->dofzins?0:1);
	sec->i1e= subdof->ndof_xin-(subdof->dofxine!=subdof->ndof_x?0:1);
	sec->i2e= subdof->ndof_zin-(subdof->dofzine!=subdof->ndof_z?0:1);
	sec->ndofin= (sec->i1e-sec->i1s)*(sec->i2e-sec->i2s);
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kx=sec->i1s;kx<sec->i1e;++kx)
	  for (kz=sec->i2s;kz<sec->i2e;++kz, ++k) {
	    sec->dofin[k]= kx*(subdof->ndof_yin)*(subdof->ndof_zin)+kz;
	  }
	fetidof->dof_sf[i][j]= sec; sec= 0; j++;
      }
      if (yms[1]) {
	MySectionDOFCreate(&sec);
	sec->isec= 3; sec->nnb= 1; sec->mastered= PETSC_FALSE;
	sec->i1s= (subdof->dofxins?0:1);
	sec->i2s= (subdof->dofzins?0:1);
	sec->i1e= subdof->ndof_xin-(subdof->dofxine!=subdof->ndof_x?0:1);
	sec->i2e= subdof->ndof_zin-(subdof->dofzine!=subdof->ndof_z?0:1);
	sec->ndofin= (sec->i1e-sec->i1s)*(sec->i2e-sec->i2s);
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kx=sec->i1s;kx<sec->i1e;++kx)
	  for (kz=sec->i2s;kz<sec->i2e;++kz, ++k) {
	    sec->dofin[k]= kx*(subdof->ndof_yin)*(subdof->ndof_zin)+(subdof->ndof_yin-1)*(subdof->ndof_zin)+kz;
	  }
	fetidof->dof_sf[i][j]= sec; sec= 0; j++;
      }
      if (zms[0]) {
	MySectionDOFCreate(&sec);
	sec->isec= 4; sec->nnb= 1; sec->mastered= PETSC_TRUE;
	sec->i1s= (subdof->dofxins?0:1);
	sec->i2s= (subdof->dofyins?0:1);
	sec->i1e= subdof->ndof_xin-(subdof->dofxine!=subdof->ndof_x?0:1);
	sec->i2e= subdof->ndof_yin-(subdof->dofyine!=subdof->ndof_y?0:1);
	sec->ndofin= (sec->i1e-sec->i1s)*(sec->i2e-sec->i2s);
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kx=sec->i1s;kx<sec->i1e;++kx)
	  for (ky=sec->i2s;ky<sec->i2e;++ky, ++k) {
	    sec->dofin[k]= kx*(subdof->ndof_zin)*(subdof->ndof_yin)+ky*(subdof->ndof_zin);
	  }
	fetidof->dof_sf[i][j]= sec; sec= 0; j++;
      }
      if (zms[1]) {
	MySectionDOFCreate(&sec);
	sec->isec= 5; sec->nnb= 1; sec->mastered= PETSC_FALSE;
	sec->i1s= (subdof->dofxins?0:1);
	sec->i2s= (subdof->dofyins?0:1);
	sec->i1e= subdof->ndof_xin-(subdof->dofxine!=subdof->ndof_x?0:1);
	sec->i2e= subdof->ndof_yin-(subdof->dofyine!=subdof->ndof_y?0:1);
	sec->ndofin= (sec->i1e-sec->i1s)*(sec->i2e-sec->i2s);
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kx=sec->i1s;kx<sec->i1e;++kx)
	  for (ky=sec->i2s;ky<sec->i2e;++ky, ++k) {
	    sec->dofin[k]= kx*(subdof->ndof_zin)*(subdof->ndof_yin)+ky*(subdof->ndof_zin)+subdof->ndof_zin-1;
	  }
	fetidof->dof_sf[i][j]= sec; sec= 0; j++;
      }
      /* compute edges between subdomains */
      nnb[0]= yml[0] && zml[0] ? (yms[0]+1)*(zms[0]+1)-1:0;
      nnb[1]= yml[0] && zml[1] ? (yms[0]+1)*(zms[1]+1)-1:0;
      nnb[2]= yml[1] && zml[0] ? (yms[1]+1)*(zms[0]+1)-1:0;
      nnb[3]= yml[1] && zml[1] ? (yms[1]+1)*(zms[1]+1)-1:0;
      nnb[4]= xml[0] && zml[0] ? (xms[0]+1)*(zms[0]+1)-1:0;
      nnb[5]= xml[0] && zml[1] ? (xms[0]+1)*(zms[1]+1)-1:0;
      nnb[6]= xml[1] && zml[0] ? (xms[1]+1)*(zms[0]+1)-1:0;
      nnb[7]= xml[1] && zml[1] ? (xms[1]+1)*(zms[1]+1)-1:0;
      nnb[8]= xml[0] && yml[0] ? (xms[0]+1)*(yms[0]+1)-1:0;
      nnb[9]= xml[0] && yml[1] ? (xms[0]+1)*(yms[1]+1)-1:0;
      nnb[10]=xml[1] && yml[0] ? (xms[1]+1)*(yms[0]+1)-1:0;
      nnb[11]=xml[1] && yml[1] ? (xms[1]+1)*(yms[1]+1)-1:0;
      for (j=0;j<12;++j) fetidof->nse[i]+= (nnb[j]>0?1:0);
      if (fetidof->nse[i]) {
	ierr=PetscMalloc(fetidof->nse[i]*sizeof(MySectionDOF),fetidof->dof_se+i);CHKERRQ(ierr);
      }
      j= 0;
      if (nnb[0]>0) { /* (x,0,0) */
	MySectionDOFCreate(&sec); sec->isec=0; sec->nnb=nnb[0]; sec->mastered=PETSC_TRUE;
	sec->i1s= (subdof->dofxins?0:1);
	sec->i1e= subdof->ndof_xin-(subdof->dofxine!=subdof->ndof_x?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kx=sec->i1s; kx<sec->i1e; ++kx, ++k) {
	  sec->dofin[k]= kx*(subdof->ndof_yin)*(subdof->ndof_zin);
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      if (nnb[1]>0) { /* (x,0,1) */
	MySectionDOFCreate(&sec); sec->isec=1; sec->nnb=nnb[1]; sec->mastered=0==zms[1];
	sec->i1s= (subdof->dofxins?0:1);
	sec->i1e= subdof->ndof_xin-(subdof->dofxine!=subdof->ndof_x?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kx=sec->i1s; kx<sec->i1e; ++kx, ++k) {
	  sec->dofin[k]= kx*(subdof->ndof_yin)*(subdof->ndof_zin)+subdof->ndof_zin-1;
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      if (nnb[2]>0) { /* (x,1,0) */
	MySectionDOFCreate(&sec); sec->isec=2; sec->nnb=nnb[2]; sec->mastered=0==yms[1];
	sec->i1s= (subdof->dofxins?0:1);
	sec->i1e= subdof->ndof_xin-(subdof->dofxine!=subdof->ndof_x?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kx=sec->i1s; kx<sec->i1e; ++kx, ++k) {
	  sec->dofin[k]= kx*(subdof->ndof_yin)*(subdof->ndof_zin) + (subdof->ndof_yin-1)*(subdof->ndof_zin);
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      if (nnb[3]>0) { /* (x,1,1) */
	MySectionDOFCreate(&sec); sec->isec=3; sec->nnb=nnb[3];	sec->mastered=PETSC_FALSE;
	sec->i1s= (subdof->dofxins?0:1);
	sec->i1e= subdof->ndof_xin-(subdof->dofxine!=subdof->ndof_x?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kx=sec->i1s; kx<sec->i1e; ++kx, ++k) {
	  sec->dofin[k]= kx*(subdof->ndof_yin)*(subdof->ndof_zin)+(subdof->ndof_yin-1)*(subdof->ndof_zin)+subdof->ndof_zin-1;
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      if (nnb[4]>0) { /* (0,y,0) */
	MySectionDOFCreate(&sec); sec->isec=4; sec->nnb=nnb[4];	sec->mastered=PETSC_TRUE;
	sec->i1s= (subdof->dofyins?0:1);
	sec->i1e= subdof->ndof_yin-(subdof->dofyine!=subdof->ndof_y?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, ky=sec->i1s; ky<sec->i1e; ++ky, ++k) {
	  sec->dofin[k]= ky*(subdof->ndof_zin);
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      if (nnb[5]>0) { /* (0,y,1) */
	MySectionDOFCreate(&sec); sec->isec=5; sec->nnb=nnb[5];	sec->mastered=0==zms[1];
	sec->i1s= (subdof->dofyins?0:1);
	sec->i1e= subdof->ndof_yin-(subdof->dofyine!=subdof->ndof_y?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, ky=sec->i1s; ky<sec->i1e; ++ky, ++k) {
	  sec->dofin[k]= ky*(subdof->ndof_zin)+subdof->ndof_zin-1;
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      if (nnb[6]>0) { /* (1,y,0) */
	MySectionDOFCreate(&sec); sec->isec=6; sec->nnb=nnb[6];	sec->mastered=0==xms[1];
	sec->i1s= (subdof->dofyins?0:1);
	sec->i1e= subdof->ndof_yin-(subdof->dofyine!=subdof->ndof_y?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, ky=sec->i1s; ky<sec->i1e; ++ky, ++k) {
	  sec->dofin[k]= (subdof->ndof_xin-1)*(subdof->ndof_yin)*(subdof->ndof_zin) + ky*(subdof->ndof_zin);
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      if (nnb[7]>0) { /* (1,y,1) */
	MySectionDOFCreate(&sec); sec->isec=7; sec->nnb=nnb[7];	sec->mastered=PETSC_FALSE;
	sec->i1s= (subdof->dofyins?0:1);
	sec->i1e= subdof->ndof_yin-(subdof->dofyine!=subdof->ndof_y?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, ky=sec->i1s; ky<sec->i1e; ++ky, ++k) {
	  sec->dofin[k]= (subdof->ndof_xin-1)*(subdof->ndof_yin)*(subdof->ndof_zin)+ky*(subdof->ndof_zin)+subdof->ndof_zin-1;
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      if (nnb[8]>0) { /* (0,0,z) */
	MySectionDOFCreate(&sec); sec->isec=8; sec->nnb=nnb[8];	sec->mastered=PETSC_TRUE;
	sec->i1s= (subdof->dofzins?0:1);
	sec->i1e= subdof->ndof_zin-(subdof->dofzine!=subdof->ndof_z?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kz=sec->i1s; kz<sec->i1e; ++kz, ++k) {
	  sec->dofin[k]= kz;
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      if (nnb[9]>0) { /* (0,1,z) */
	MySectionDOFCreate(&sec); sec->isec=9; sec->nnb=nnb[9];	sec->mastered=0==yms[1];
	sec->i1s= (subdof->dofzins?0:1);
	sec->i1e= subdof->ndof_zin-(subdof->dofzine!=subdof->ndof_z?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kz=sec->i1s; kz<sec->i1e; ++kz, ++k) {
	  sec->dofin[k]= (subdof->ndof_yin-1)*(subdof->ndof_zin)+kz;
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      if (nnb[10]>0){ /* (1,0,z) */
	MySectionDOFCreate(&sec); sec->isec=10; sec->nnb=nnb[10]; sec->mastered=0==xms[1];
	sec->i1s= (subdof->dofzins?0:1);
	sec->i1e= subdof->ndof_zin-(subdof->dofzine!=subdof->ndof_z?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kz=sec->i1s; kz<sec->i1e; ++kz, ++k) {
	  sec->dofin[k]= (subdof->ndof_xin-1)*(subdof->ndof_yin)*(subdof->ndof_zin) + kz;
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      if (nnb[11]>0){ /* (1,1,z) */
	MySectionDOFCreate(&sec); sec->isec=11; sec->nnb=nnb[11]; sec->mastered=PETSC_FALSE;
	sec->i1s= (subdof->dofzins?0:1);
	sec->i1e= subdof->ndof_zin-(subdof->dofzine!=subdof->ndof_z?0:1);
	sec->ndofin= sec->i1e - sec->i1s;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	for (k=0, kz=sec->i1s; kz<sec->i1e; ++kz, ++k) {
	  sec->dofin[k]= (subdof->ndof_xin-1)*(subdof->ndof_yin)*(subdof->ndof_zin)+(subdof->ndof_yin-1)*(subdof->ndof_zin)+kz;
	}
	fetidof->dof_se[i][j]= sec; sec= 0; j++;
      }
      /* compute vertices between subdomains */
      nnb[0]= xml[0] && yml[0] && zml[0] ? (xms[0]+1)*(yms[0]+1)*(zms[0]+1)-1:0;
      nnb[1]= xml[0] && yml[0] && zml[1] ? (xms[0]+1)*(yms[0]+1)*(zms[1]+1)-1:0;
      nnb[2]= xml[0] && yml[1] && zml[0] ? (xms[0]+1)*(yms[1]+1)*(zms[0]+1)-1:0;
      nnb[3]= xml[0] && yml[1] && zml[1] ? (xms[0]+1)*(yms[1]+1)*(zms[1]+1)-1:0;
      nnb[4]= xml[1] && yml[0] && zml[0] ? (xms[1]+1)*(yms[0]+1)*(zms[0]+1)-1:0;
      nnb[5]= xml[1] && yml[0] && zml[1] ? (xms[1]+1)*(yms[0]+1)*(zms[1]+1)-1:0;
      nnb[6]= xml[1] && yml[1] && zml[0] ? (xms[1]+1)*(yms[1]+1)*(zms[0]+1)-1:0;
      nnb[7]= xml[1] && yml[1] && zml[1] ? (xms[1]+1)*(yms[1]+1)*(zms[1]+1)-1:0;
      for (j=0;j<8;++j) fetidof->nsv[i]+= (nnb[j]>0?1:0);
      if (fetidof->nsv[i]) {
	ierr=PetscMalloc(fetidof->nsv[i]*sizeof(MySectionDOF),fetidof->dof_sv+i);CHKERRQ(ierr);
      }
      j= 0;
      if (nnb[0]>0) {		/* (0,0,0) */
	MySectionDOFCreate(&sec);
	sec->isec=0; sec->nnb=nnb[0]; sec->mastered=PETSC_TRUE; sec->ndofin=1;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	sec->dofin[0]= 0;
	fetidof->dof_sv[i][j]= sec; sec= 0; j++;
      }
      if (nnb[1]>0) {		/* (0,0,1) */
	MySectionDOFCreate(&sec);
	sec->isec= 1; sec->nnb= nnb[1]; sec->mastered= 0==zms[1]; sec->ndofin= 1;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	sec->dofin[0]= subdof->ndof_zin-1;
	/* printf("proc=%d,i=%d,dofin=%d,nsv=%d\n",subdof->dm->rank,i,sec->dofin[0],fetidof->nsv[i]); */
	fetidof->dof_sv[i][j]= sec; sec= 0; j++;
      }
      if (nnb[2]>0) {		/* (0,1,0) */
	MySectionDOFCreate(&sec);
	sec->isec= 2; sec->nnb= nnb[2]; sec->mastered=0==yms[1]; sec->ndofin= 1;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	sec->dofin[0]= (subdof->ndof_yin-1)*(subdof->ndof_zin);
	fetidof->dof_sv[i][j]= sec; sec= 0; j++;
      }
      if (nnb[3]>0) {		/* (0,1,1) */
	MySectionDOFCreate(&sec);
	sec->isec= 3; sec->nnb= nnb[3];	sec->mastered=(0==yms[1]&&0==zms[1]); sec->ndofin= 1;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	sec->dofin[0]= (subdof->ndof_yin-1)*(subdof->ndof_zin)+subdof->ndof_zin-1;
	fetidof->dof_sv[i][j]= sec; sec= 0; j++;
      }
      if (nnb[4]>0) {		/* (1,0,0) */
	MySectionDOFCreate(&sec);
	sec->isec= 4; sec->nnb= nnb[4]; sec->mastered=0==xms[1]; sec->ndofin= 1;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	sec->dofin[0]= (subdof->ndof_xin-1)*(subdof->ndof_yin)*(subdof->ndof_zin);
	fetidof->dof_sv[i][j]= sec; sec= 0; j++;
      }
      if (nnb[5]>0) {		/* (1,0,1) */
	MySectionDOFCreate(&sec);
	sec->isec= 5; sec->nnb= nnb[5]; sec->mastered=0==xms[1]&&0==zms[1]; sec->ndofin= 1;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	sec->dofin[0]= (subdof->ndof_xin-1)*(subdof->ndof_yin)*(subdof->ndof_zin)+subdof->ndof_zin-1;
	fetidof->dof_sv[i][j]= sec; sec= 0; j++;
      }
      if (nnb[6]>0) {		/* (1,1,0) */
	MySectionDOFCreate(&sec);
	sec->isec= 6; sec->nnb= nnb[6]; sec->mastered=0==xms[1]&&0==yms[1]; sec->ndofin= 1;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	sec->dofin[0]= (subdof->ndof_xin-1)*(subdof->ndof_yin)*(subdof->ndof_zin)+(subdof->ndof_yin-1)*(subdof->ndof_zin);
	fetidof->dof_sv[i][j]= sec; sec= 0; j++;
      }
      if (nnb[7]>0) {		/* (1,1,1) */
	MySectionDOFCreate(&sec);
	sec->isec= 7; sec->nnb= nnb[7];	sec->mastered= PETSC_FALSE; sec->ndofin= 1;
	ierr=PetscMalloc(sec->ndofin*sizeof(PetscInt),&(sec->dofin));CHKERRQ(ierr);
	sec->dofin[0]= (subdof->ndof_xin-1)*(subdof->ndof_yin)*(subdof->ndof_zin)+(subdof->ndof_yin-1)*(subdof->ndof_zin)+subdof->ndof_zin-1;
	fetidof->dof_sv[i][j]= sec; sec= 0; j++;
      }
    }
  }
  PetscFree(nnb);
  fetidof->has_section= PETSC_TRUE;
  PetscFunctionReturn(0);
}

/*
   Given the initial coarse dof, we gather them to proc. 0 of subdomains.  Then
   we orthogonalize, filter and number them.

   Collective in each subdomain.
*/
#undef __FUNCT__
#define __FUNCT__ "FETIDOFSetupCoarse"
static MyErrCode FETIDOFSetupCoarse(FETIDOF fetidof)
{
  PetscFunctionBegin;
  if (!fetidof->has_coarse) return(0);


  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FETIDOFSetupDual"
static MyErrCode FETIDOFSetupDual(FETIDOF fetidof)
{
  PetscFunctionBegin;


  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FETIDOFSetupNonshared"
static MyErrCode FETIDOFSetupNonshared(FETIDOF fetidof)
{
  PetscFunctionBegin;


  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "FETIDOFSetup"
MyErrCode FETIDOFSetup(FETIDOF fetidof)
{
  MyErrCode myerr;
  PetscFunctionBegin;
  myerr= FETIDOFSetupCoarse(fetidof); CHKMyErrQ(myerr);
  myerr= FETIDOFSetupDual(fetidof); CHKMyErrQ(myerr);
  myerr= FETIDOFSetupNonshared(fetidof); CHKMyErrQ(myerr);
  fetidof->initialized= PETSC_TRUE;
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__  "FETIDOFCreateVecPrimal"
MyErrCode FETIDOFCreateVecPrimal(FETIDOF fetidof, Vec *vc)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if (!fetidof) return FETIDOFNull;
  if (!vc) return FETIDOFNeedsInput;
  if (!fetidof->initialized) return FETIDOFNotInit;
  if (!fetidof->has_coarse) {*vc=0; return 0;}
  if (!fetidof->vc) return FETIDOFWrong;
  ierr=VecDuplicate(fetidof->vc,vc);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "FETIDOFGetScattersPrimal"
MyErrCode FETIDOFGetScattersPrimal(FETIDOF fetidof, VecScatter **gc2sp)
{
  PetscFunctionBegin;
  if (!fetidof) return FETIDOFNull;
  if (!gc2sp) return FETIDOFNeedsInput;
  if (!fetidof->initialized) return FETIDOFNotInit;
  if (!fetidof->has_coarse) {*gc2sp=0; return 0;}
  if (!fetidof->gc2sp) return FETIDOFWrong;
  *gc2sp= fetidof->gc2sp;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "FETIDOFRestoreScattersPrimal"
MyErrCode FETIDOFRestoreScattersPrimal(FETIDOF fetidof, VecScatter **gc2sp)
{
  PetscFunctionBegin;
  if (!gc2sp) return FETIDOFNeedsInput;
  *gc2sp= 0;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "FETIDOFCreateVecDual"
MyErrCode FETIDOFCreateVecDual(FETIDOF fetidof, Vec *vl)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if (!fetidof) return FETIDOFNull;
  if (!vl) return FETIDOFNeedsInput;
  if (!fetidof->initialized) return FETIDOFNotInit;
  if (!fetidof->vl) return FETIDOFWrong;
  ierr=VecDuplicate(fetidof->vl,vl);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "FETIDOFGetScattersDual"
MyErrCode FETIDOFGetScattersDual(FETIDOF fetidof, VecScatter **gl2snp)
{
  PetscFunctionBegin;
  if (!fetidof) return FETIDOFNull;
  if (!gl2snp) return FETIDOFNeedsInput;
  if (!fetidof->initialized) return FETIDOFNotInit;
  if (!fetidof->gl2snp) return FETIDOFWrong;
  *gl2snp= fetidof->gl2snp;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "FETIDOFRestoreScattersDual"
MyErrCode FETIDOFRestoreScattersDual(FETIDOF fetidof, VecScatter **gl2snp)
{
  PetscFunctionBegin;
  if (!gl2snp) return FETIDOFNeedsInput;
  *gl2snp= 0;
  PetscFunctionReturn(0);
}
