#include "helmbvp.h"

/*
   Notes.  When 'malloc', it is possible that we are doing a secondary 'malloc'
   so that the first allocated memory is lost.  To avoid this, we check whether
   the pointer is null and if so we allocate memory; otherwise we deem the user
   has already done so.
*/

#undef __FUNCT__
#define __FUNCT__  "HelmBVPCreate"
MyErrCode HelmBVPCreate(HelmBVP *p_bvp)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr= PetscMalloc(sizeof(structHelmBVP),p_bvp);CHKERRQ(ierr);
  ierr= PetscMemzero(*p_bvp,sizeof(structHelmBVP));CHKERRQ(ierr);
  ierr= PetscMalloc(sizeof(char)*HELMBVP_MAX_NAMELEN,&((*p_bvp)->myname));
  CHKERRQ(ierr);
  PetscMemzero((*p_bvp)->myname,sizeof(char)*HELMBVP_MAX_NAMELEN);
  PetscFunctionReturn((MyErrCode)0);
}

#undef __FUNCT__
#define __FUNCT__  "HelmBVPDestroy"
MyErrCode HelmBVPDestroy(HelmBVP *p_bvp)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if (!*p_bvp) return(0);
  ierr= PetscFree((*p_bvp)->w);CHKERRQ(ierr);
  ierr= PetscFree((*p_bvp)->rho);CHKERRQ(ierr);
  ierr= PetscFree((*p_bvp)->avmctx_rho);CHKERRQ(ierr);
  ierr= PetscFree((*p_bvp)->avmctx_w);CHKERRQ(ierr);
  ierr= PetscFree((*p_bvp)->wavespeedfilename);CHKERRQ(ierr);
  ierr= PetscFree((*p_bvp)->densityfilename);CHKERRQ(ierr);
  ierr= PetscFree((*p_bvp)->srcpts_x);CHKERRQ(ierr);
  ierr= PetscFree((*p_bvp)->srcpts_y);CHKERRQ(ierr);
  ierr= PetscFree((*p_bvp)->srcpts_z);CHKERRQ(ierr);
  ierr= PetscFree((*p_bvp)->srcpts_amplitude);CHKERRQ(ierr);
  ierr= PetscFree((*p_bvp)->myname);CHKERRQ(ierr);
  ierr= PetscFree((*p_bvp)->prefix);CHKERRQ(ierr);
  if ((*p_bvp)->appendix) {
    if ((*p_bvp)->destroyappendix)
      (*p_bvp)->destroyappendix(&((*p_bvp)->appendix));
    else {
      ierr=PetscFree((*p_bvp)->appendix);CHKERRQ(ierr);
    }    
  }
  ierr= PetscFree(*p_bvp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* ---------------------------------------------------------------------------
   Selectable function members of a HelmBVP.
   --------------------------------------------------------------------------- */

/*
   Default xi function for PML.
*/
static inline PetscScalar xi_default(xiDefaultCtx *ctx)
{
  return 1.0/(1.0-PETSC_i*(ctx->sign)/(ctx->wi)*6.0*PETSC_PI/(ctx->L)*(ctx->t/ctx->L)*(ctx->t/ctx->L));
}

/*
   Default ComputeWavenumber function.
*/

static MyErrCode ComputeWavenumber_default(PetscScalar *c_in_w_out, c2wCtx *userctx)
{
  PetscInt i;
  MyErrCode myerr=0;
  if (!c_in_w_out) {myerr= EmptyArray; return myerr;}
  if (!userctx) {myerr= EmptyCtx; return myerr;}
  for (i=0; i<userctx->dm->nloc; ++i){
    if (PetscRealPart(c_in_w_out[i]))
      c_in_w_out[i]= 2.0*PETSC_PI*(userctx->freq)/c_in_w_out[i];
    else {myerr= DividedByZero; return myerr;}
  }
  return 0;
}

/*
   Robin parameters Taylor of zero-order on faces

   We assume the memory of pf is already allocated.
*/
static inline MyErrCode robin_pf_T0(PetscScalar *pf, TaylorCtx *ctx)
{
  pf[0]= PETSC_i*(ctx->wi)*(ctx->sign)/(ctx->rho); pf[1]= 0.0; return 0;
}

/*
   Robin parameters Taylor of second-order on faces.  Assume it is p0 + p2
   Laplacian, so 1/rho is contained in p0 and p2.

   We assume the memory of pf is already allocated.
*/
static inline MyErrCode robin_pf_T2(PetscScalar *pf, TaylorCtx *ctx)
{
  pf[0]= PETSC_i*(ctx->wi)*(ctx->sign)/(ctx->rho);
  pf[1]= -0.5*PETSC_i*(ctx->sign)/(ctx->wi)/(ctx->rho); return 0;
}

/*
   Robin parameters Taylor of second-order on edges.  It is from p2*Laplacian on
   faces, so contains only one 1/rho (from p2).  The parameters are coefficients
   for the zeroth- and second-order integrals, not for the strong form.

   We assume the memory of pe is already allocated.
*/
static inline MyErrCode robin_pe_T2(PetscScalar *pe, TaylorCtx *ctx)
{
  pe[0]= 0.75/(ctx->rho);
  pe[1]= -0.25/(ctx->wi)/(ctx->wi)/(ctx->rho);

  return 0;
}

/*
   Robin parameters Taylor of second-order on faces as coefficient of integral.

   We assume the memory of pv is already allocated.
*/
static inline MyErrCode robin_pv_T2(PetscScalar *pv, TaylorCtx *ctx)
{
  pv[0]= -0.5*PETSC_i/(ctx->wi)*(ctx->sign)/ctx->rho;

  return 0;
}


/* ---------------------------------------------------------------------------
   Set a single or a group of options of a HelmBVP.
   --------------------------------------------------------------------------- */

inline MyErrCode HelmBVPSetSign(HelmBVP bvp, PetscInt sign)
{
  bvp->sign= sign>0 ? 1 : -1;
  return 0;
}

inline MyErrCode HelmBVPSetQuadOrder(HelmBVP bvp, PetscInt quad_order)
{
  bvp->quad_order= quad_order;
  return 0;
}

inline MyErrCode HelmBVPSetPrefix(HelmBVP bvp, const char prefix[])
{
  PetscErrorCode ierr;

  if(prefix){
    if(prefix[0]) {
      if (!(bvp->prefix)){ierr=PetscMalloc(sizeof(char)*HELMBVP_MAX_NAMELEN,&(bvp->prefix));CHKERRQ(ierr);}
      PetscStrcpy(bvp->prefix,prefix);
    }
    else PetscFree(bvp->prefix);
  }
  else PetscFree(bvp->prefix);

  return 0;
}

/*
   If pointer is null, we allocate memory for them; othewise, we assume the user
   is responsible for memory allocation.
*/
#undef __FUNCT__
#define __FUNCT__ "HelmBVPSetCellDataFiles"
inline MyErrCode HelmBVPSetCellDataFiles(HelmBVP bvp,const char *wavespeedfilename,const PetscInt siz_w[], const char *densityfilename,const PetscInt siz_rho[],AverageMethod method_w,const AverageCtx *ctx_w,AverageMethod method_rho, const AverageCtx *ctx_rho)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if(wavespeedfilename){
    if(!(bvp->wavespeedfilename)){ierr= PetscMalloc(sizeof(char)*MYIO_MAX_FILENAME,&(bvp->wavespeedfilename));CHKERRQ(ierr);}
    ierr= PetscStrcpy(bvp->wavespeedfilename,wavespeedfilename);CHKERRQ(ierr);
    bvp->siz_wavespeed[0]= siz_w[0]; bvp->siz_wavespeed[1]= siz_w[1]; bvp->siz_wavespeed[2]= siz_w[2];
    bvp->avm_w= method_w; 
    if (ctx_w) {
      ierr=PetscMalloc(sizeof(AverageCtx),&(bvp->avmctx_w));CHKERRQ(ierr);
      ierr=PetscMemcpy(bvp->avmctx_w,ctx_w,sizeof(AverageCtx));CHKERRQ(ierr);      
    }
  }
  if(densityfilename){
    if(!(bvp->densityfilename)){ierr= PetscMalloc(sizeof(char)*MYIO_MAX_FILENAME,&(bvp->densityfilename));CHKERRQ(ierr);}
    ierr= PetscStrcpy(bvp->densityfilename,densityfilename);CHKERRQ(ierr);
    bvp->siz_density[0]= siz_rho[0]; bvp->siz_density[1]= siz_rho[1]; bvp->siz_density[2]= siz_rho[2];
    bvp->avm_rho= method_rho;
    if (ctx_rho) {
      ierr=PetscMalloc(sizeof(AverageCtx),&(bvp->avmctx_rho));CHKERRQ(ierr);
      ierr=PetscMemcpy(bvp->avmctx_rho,ctx_rho,sizeof(AverageCtx));CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

inline MyErrCode HelmBVPAppend(HelmBVP bvp, void *append)
{
  bvp->appendix= append; return(0);
}

inline MyErrCode HelmBVPSetDataDomain(HelmBVP bvp,const PetscScalar wdomain[6],const PetscScalar rhodomain[6])
{
  PetscMemcpy(bvp->wdomain,wdomain,6*sizeof(PetscScalar)); 
  PetscMemcpy(bvp->rhodomain,rhodomain,6*sizeof(PetscScalar)); return 0;
}

inline MyErrCode HelmBVPSetDomain(HelmBVP bvp, PetscScalar xmin, PetscScalar xmax, PetscScalar ymin, PetscScalar ymax, PetscScalar zmin, PetscScalar zmax)
{
  MyErrCode myerr=0;
  if(PetscRealPart(xmin)>=PetscRealPart(xmax) || PetscRealPart(ymin)>=PetscRealPart(ymax) || PetscRealPart(zmin)>=PetscRealPart(zmax)){myerr= WrongDomain; return myerr;}
  bvp->xmin= xmin; bvp->xmax= xmax;
  bvp->ymin= ymin; bvp->ymax= ymax;
  bvp->zmin= zmin; bvp->zmax= zmax;
  return 0;
}

inline MyErrCode HelmBVPSetFreq(HelmBVP bvp, PetscScalar freq)
{
  bvp->freq= freq;
  return 0;
}

inline MyErrCode HelmBVPSetWavNumFun(HelmBVP bvp, ArrayCtxFun wavnum)
{
  bvp->ComputeWavenumber= wavnum;
  return 0;
}

inline MyErrCode HelmBVPSetPMLxi(HelmBVP bvp, CtxFun xi)
{
  bvp->xi= xi; return 0;
}

/* We assume the memory is already allocated if pointer is non-zero. */
inline MyErrCode HelmBVPSetName(HelmBVP bvp, const char *myname)
{
  PetscErrorCode ierr;
  if (!(bvp->myname)){ierr=PetscMalloc(sizeof(char)*HELMBVP_MAX_NAMELEN,&(bvp->myname));CHKERRQ(ierr);}
  PetscStrcpy(bvp->myname,myname);
  return 0;
}


/* ---------------------------------------------------------------------------
   Set all options of a HelmBVP.
   --------------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__  "HelmBVPSetDefaults"
inline MyErrCode HelmBVPSetDefaults(HelmBVP bvp)
{
  PetscInt ierr;

  PetscFunctionBegin;

  if (!(bvp->myname)){ierr=PetscMalloc(sizeof(char)*HELMBVP_MAX_NAMELEN,&(bvp->myname));CHKERRQ(ierr);}
  PetscStrcpy(bvp->myname,"original");
  if (!(bvp->prefix)){ierr=PetscMalloc(sizeof(char)*HELMBVP_MAX_NAMELEN,&(bvp->prefix));CHKERRQ(ierr);}
  PetscStrcpy(bvp->prefix,"bvp_");
  bvp->xmin= 0.0; bvp->xmax= 1.0;
  bvp->ymin= 0.0; bvp->ymax= 1.0;
  bvp->zmin= 0.0; bvp->zmax= 1.0;
  bvp->freq= 1.0;
  bvp->wavespeedfilename= PETSC_NULL;
  bvp->wdomain[0]= bvp->xmin; bvp->wdomain[1]= bvp->xmax; bvp->wdomain[2]= bvp->ymin; bvp->wdomain[3]= bvp->ymax; bvp->wdomain[4]= bvp->zmin; bvp->wdomain[5]= bvp->zmax;
  bvp->densityfilename= PETSC_NULL;
  PetscMemcpy(bvp->rhodomain,bvp->wdomain,sizeof(PetscScalar)*6);
  bvp->AttenuateWavespeed= PETSC_NULL;
  bvp->ComputeWavenumber= (ArrayCtxFun)ComputeWavenumber_default;
  bvp->sign= 1;
  bvp->xi= (CtxFun)xi_default;
  bvp->bctype_xl= Robin; bvp->bctype_xr= Robin;
  bvp->bctype_yl= Robin; bvp->bctype_yr= Robin;
  bvp->bctype_zl= Robin; bvp->bctype_zr= Robin;
  bvp->robin_order= 0;
  bvp->robin_pf= (ArrayCtxFun)robin_pf_T0;
  bvp->robin_pe= PETSC_NULL;
  bvp->robin_pv= PETSC_NULL;
  bvp->numsrcpts= 1;
  if(!(bvp->srcpts_x))PetscMalloc(bvp->numsrcpts*sizeof(PetscScalar),&(bvp->srcpts_x));
  if(!(bvp->srcpts_y))PetscMalloc(bvp->numsrcpts*sizeof(PetscScalar),&(bvp->srcpts_y));
  if(!(bvp->srcpts_z))PetscMalloc(bvp->numsrcpts*sizeof(PetscScalar),&(bvp->srcpts_z));
  if(!(bvp->srcpts_amplitude))PetscMalloc(bvp->numsrcpts*sizeof(PetscScalar),&(bvp->srcpts_amplitude));
  bvp->srcpts_x[0]= 0.3; bvp->srcpts_y[0]= 0.3; bvp->srcpts_z[0]= 0.3; bvp->srcpts_amplitude[0]= 1.0;

  PetscFunctionReturn(0);
}

/*
   Set HelmBVP from petsc option database.
*/
#undef __FUNCT__
#define __FUNCT__  "HelmBVPSetFromOptions"
MyErrCode HelmBVPSetFromOptions(HelmBVP bvp, const char filename[], const char *prefix)
{
  PetscReal cube[6], *vals;
  PetscInt nmax, i;
  PetscBool set, flg;
  PetscErrorCode ierr;
  char *atten,*wavnum,*xi,*robin;

  PetscFunctionBegin;

  /*
     prefix
  */
  HelmBVPSetPrefix(bvp,prefix);

  /*
     insert the option file
  */
  if(filename){
    if(filename[0]){ierr=PetscOptionsInsertFile(PETSC_COMM_WORLD,filename,PETSC_TRUE);CHKERRQ(ierr);}
  }

  /*
     name
  */
  if(!(bvp->myname)){ierr=PetscMalloc(HELMBVP_MAX_NAMELEN*sizeof(char),&(bvp->myname));CHKERRQ(ierr);i=1;}
  PetscOptionsGetString(bvp->prefix,"-name",bvp->myname,HELMBVP_MAX_NAMELEN,&set);
  if(!set && i==1){PetscFree(bvp->myname);}

  /*
     physical domain
  */
  nmax=6; PetscOptionsGetRealArray(bvp->prefix,"-cube",cube,&nmax,&set);
  if(set){
    if (nmax==6) {
    bvp->xmin= (PetscScalar)(cube[0]); bvp->xmax= (PetscScalar)(cube[1]);
    bvp->ymin= (PetscScalar)(cube[2]); bvp->ymax= (PetscScalar)(cube[3]);
    bvp->zmin= (PetscScalar)(cube[4]); bvp->zmax= (PetscScalar)(cube[5]);
    }
    else return OptArrayInComplete;
  }

  /*
     frequency
  */
  PetscOptionsGetScalar(bvp->prefix,"-freq",&(bvp->freq),&set);
  if(!set) bvp->freq= 1.0;	/* one when -freq not appearing */

  /*
     wavespeed
  */
  if(!(bvp->wavespeedfilename)){ierr=PetscMalloc(sizeof(char)*MYIO_MAX_FILENAME,&(bvp->wavespeedfilename));CHKERRQ(ierr);i=1;}
  ierr=PetscOptionsGetString(bvp->prefix,"-wavespeed",bvp->wavespeedfilename,MYIO_MAX_FILENAME*sizeof(char),&set);CHKERRQ(ierr);
  if(!set && i) PetscFree(bvp->wavespeedfilename);
  else {
    nmax=3; PetscOptionsGetIntArray(bvp->prefix,"-siz_w",bvp->siz_wavespeed,&nmax,&set); if (set && nmax<3) return OptArrayInComplete;
    PetscOptionsGetInt(bvp->prefix,"-avm_w",&i,&set); if(set) bvp->avm_w= i;
    nmax= 6; PetscOptionsGetRealArray(bvp->prefix,"-cube_w",cube,&nmax,&set);
    if(set){
      if(nmax==6) {
	for(i=0;i<6;i++) bvp->wdomain[i]= (PetscScalar)(cube[i]);
      }
      else return OptArrayInComplete;
    }
    else return WavespeedDomainUnsetted;
  }

  /*
     function AttenuateWavespeed
  */
  ierr=PetscMalloc(MYIO_MAX_FILENAME*sizeof(char),&atten);CHKERRQ(ierr);
  PetscOptionsGetString(bvp->prefix,"-atten",atten,MYIO_MAX_FILENAME*sizeof(char),&set);
  if(set){
    PetscStrcmp(atten,"default",&flg); if(atten[0]==0) flg= 1;
    if(flg) bvp->AttenuateWavespeed= PETSC_NULL;
    else {PetscPrintf(PETSC_COMM_WORLD,"-atten, options other than (null), default are not supported yet, use default (NULL)\n"); bvp->AttenuateWavespeed= PETSC_NULL;}
  }
  PetscFree(atten);


  /*
     function ComputeWavenumber
  */
  ierr=PetscMalloc(MYIO_MAX_FILENAME*sizeof(char),&wavnum);CHKERRQ(ierr);
  PetscOptionsGetString(bvp->prefix,"-wavnum",wavnum,MYIO_MAX_FILENAME*sizeof(char),&set);
  if(set){
    PetscStrcmp(wavnum,"default",&flg); if(wavnum[0]==0) flg= 1;
    if(flg) bvp->ComputeWavenumber= (ArrayCtxFun)ComputeWavenumber_default;
    else {PetscPrintf(PETSC_COMM_WORLD,"-wavnum, options other than (null), default are not supported yet, use default\n"); bvp->ComputeWavenumber= (ArrayCtxFun)ComputeWavenumber_default;}
  }
  PetscFree(wavnum);


  /*
     density
  */
  if(!(bvp->densityfilename)){ierr=PetscMalloc(sizeof(char)*MYIO_MAX_FILENAME,&(bvp->densityfilename));CHKERRQ(ierr);i=1;}
  PetscOptionsGetString(bvp->prefix,"-density",bvp->densityfilename,MYIO_MAX_FILENAME,&set);
  if(!set && i)PetscFree(bvp->densityfilename);
  else{
    nmax=3; PetscOptionsGetIntArray(bvp->prefix,"-siz_rho",bvp->siz_density,&nmax,&set); if (set && nmax<3) return OptArrayInComplete;
    PetscOptionsGetInt(bvp->prefix,"-avm_rho",&i,&set); if(set) bvp->avm_rho= i;
    nmax=6; PetscOptionsGetRealArray(bvp->prefix,"-cube_rho",cube,&nmax,&set);
    if(set) {
      if(nmax==6) {
	for(i=0;i<6;i++) bvp->rhodomain[i]= (PetscScalar)(cube[i]);
      }
      else return OptArrayInComplete;
    }
  }

  /*
     sign convention
  */
  PetscOptionsGetInt(bvp->prefix,"-sign",&i,&set);
  if(set) bvp->sign= i>=0 ? 1:-1;

  /*
     function for complex stretching in PML
  */
  ierr=PetscMalloc(MYIO_MAX_FILENAME*sizeof(char),&xi);CHKERRQ(ierr);
  PetscOptionsGetString(bvp->prefix,"-xi",xi,MYIO_MAX_FILENAME*sizeof(char),&set);
  if(set){
    PetscStrcmp(xi,"default",&flg); if(xi[0]==0) flg= 1;
    if(flg) bvp->xi= (CtxFun)xi_default;
    else {PetscPrintf(PETSC_COMM_WORLD,"-xi, options other than (null), default are not supported yet, use default\n"); bvp->xi= (CtxFun)xi_default;}
  }
  PetscFree(xi);

  /*
     boundary conditions
  */
  nmax=6; PetscOptionsGetRealArray(bvp->prefix,"-bctype",cube,&nmax,&set);
  if(set) {
    if(nmax==6) {
      bvp->bctype_xl= (PetscInt)cube[0];
      bvp->bctype_xr= (PetscInt)cube[1];
      bvp->bctype_yl= (PetscInt)cube[2];
      bvp->bctype_yr= (PetscInt)cube[3];
      bvp->bctype_zl= (PetscInt)cube[4];
      bvp->bctype_zr= (PetscInt)cube[5];
    }
    else return OptArrayInComplete;
  }

  /*
     Robin conditions
  */
  PetscOptionsGetInt(bvp->prefix,"-robin_order",&(bvp->robin_order),&set);
  if(bvp->robin_order>2){
    PetscPrintf(PETSC_COMM_WORLD,"-robin_order other than 0,2 is not supported, use default 0\n");bvp->robin_order= 0;
  }
  ierr=PetscMalloc(MYIO_MAX_FILENAME*sizeof(char),&robin);CHKERRQ(ierr);
  /* robin_pf */
  PetscOptionsGetString(bvp->prefix,"-robin_pf",robin,MYIO_MAX_FILENAME*sizeof(char),&set);
  if(set){
    PetscStrcmp(robin,"default",&flg); if(robin[0]==0) flg= 1;
    if(bvp->robin_order==0) {
      if(!flg) PetscStrcmp(robin,"T0",&flg);
      if(flg) bvp->robin_pf= (ArrayCtxFun)robin_pf_T0;
      else {PetscPrintf(PETSC_COMM_WORLD,"-robin_pf, options other than (null), default, T0, when robin_order==0, are not supported yet, use default T0\n"); bvp->robin_pf= (ArrayCtxFun)robin_pf_T0;}
    }
    else {
      if(!flg) PetscStrcmp(robin,"T2",&flg);
      if(flg) bvp->robin_pf= (ArrayCtxFun)robin_pf_T2;
      else {PetscPrintf(PETSC_COMM_WORLD,"-robin_pf, options other than (null), default, T0, when robin_order==2, are not supported yet, use default T2\n"); bvp->robin_pf= (ArrayCtxFun)robin_pf_T2;}
    }
  }
  /* robin_pe */
  PetscOptionsGetString(bvp->prefix,"-robin_pe",robin,MYIO_MAX_FILENAME*sizeof(char),&set);
  if(set){
    PetscStrcmp(robin,"default",&flg); if(robin[0]==0)flg= 1;
    if(bvp->robin_order==0) {
      if(!flg) PetscStrcmp(robin,"T0",&flg);
      if(flg) bvp->robin_pe= PETSC_NULL;
      else {PetscPrintf(PETSC_COMM_WORLD,"-robin_pe, options other than (null), default, T0, when robin_order==0, are not supported yet, use default T0 (NULL)\n"); bvp->robin_pe= PETSC_NULL;}
    }
    else {
      if(!flg) PetscStrcmp(robin,"T2",&flg);
      if(flg) bvp->robin_pe= (ArrayCtxFun)robin_pe_T2;
      else {PetscPrintf(PETSC_COMM_WORLD,"-robin_pe, options other than (null), default, T2, when robin_order==2, are not supported yet, use default T2\n"); bvp->robin_pe= (ArrayCtxFun)robin_pe_T2;}
    }
  }
  /* robin_pv */
  PetscOptionsGetString(bvp->prefix,"-robin_pv",robin,MYIO_MAX_FILENAME*sizeof(char),&set);
  if(set){
    PetscStrcmp(robin,"default",&flg); if(robin[0]==0)flg= 1;
    if(bvp->robin_order==0) {
      if(!flg) PetscStrcmp(robin,"T0",&flg);
      if(flg) bvp->robin_pv= PETSC_NULL;
      else {PetscPrintf(PETSC_COMM_WORLD,"-robin_pv, options other than (null), default, T0, when robin_order==0, are not supported yet, use default T0 (NULL)\n"); bvp->robin_pv= PETSC_NULL;}
    }
    else {
      if(!flg) PetscStrcmp(robin,"T2",&flg);
      if(flg) bvp->robin_pv= (ArrayCtxFun)robin_pv_T2;
      else {PetscPrintf(PETSC_COMM_WORLD,"-robin_pv, options other than (null), default, T2, when robin_order==2, are not supported yet, use default T2\n"); bvp->robin_pv= (ArrayCtxFun)robin_pv_T2;}
    }
  }
  PetscFree(robin);


  /*
     source terms
  */
  PetscOptionsGetInt(bvp->prefix,"-srcn",&(bvp->numsrcpts),&set);
  if(set){
    PetscFree(bvp->srcpts_x); PetscFree(bvp->srcpts_y); PetscFree(bvp->srcpts_z);
    PetscFree(bvp->srcpts_amplitude);
    PetscMalloc(bvp->numsrcpts*sizeof(PetscScalar),&(bvp->srcpts_x));
    PetscMalloc(bvp->numsrcpts*sizeof(PetscScalar),&(bvp->srcpts_y));
    PetscMalloc(bvp->numsrcpts*sizeof(PetscScalar),&(bvp->srcpts_z));
    PetscMalloc(bvp->numsrcpts*sizeof(PetscScalar),&(bvp->srcpts_amplitude));
  }
  if(set && bvp->numsrcpts){
    PetscMalloc(bvp->numsrcpts*sizeof(PetscReal),&vals);
    nmax= bvp->numsrcpts; PetscOptionsGetRealArray(bvp->prefix,"-srcx",vals,&nmax,&set);
    if(set) {
      if(nmax==bvp->numsrcpts) {
	for(i=0;i<nmax;i++) bvp->srcpts_x[i]= (PetscScalar)vals[i];
      }
      else if(nmax>bvp->numsrcpts) return OptArrayIgnored;
      else return OptArrayInComplete;
    }
    else return SrcPtsUnsetted;
    nmax= bvp->numsrcpts; PetscOptionsGetRealArray(bvp->prefix,"-srcy",vals,&nmax,&set);
    if(set) {
      if(nmax==bvp->numsrcpts) {
	for(i=0;i<nmax;i++) bvp->srcpts_y[i]= (PetscScalar)vals[i];
      }
      else if(nmax>bvp->numsrcpts) return OptArrayIgnored;
      else return OptArrayInComplete;
    }
    else return SrcPtsUnsetted;
    nmax= bvp->numsrcpts; PetscOptionsGetRealArray(bvp->prefix,"-srcz",vals,&nmax,&set);
    if(set) {
      if(nmax==bvp->numsrcpts) {
	for(i=0;i<nmax;i++) bvp->srcpts_z[i]= (PetscScalar)vals[i];
      }
      else if(nmax>bvp->numsrcpts) return OptArrayIgnored;
      else return OptArrayInComplete;
    }
    else return SrcPtsUnsetted;
    nmax= bvp->numsrcpts; PetscOptionsGetRealArray(bvp->prefix,"-srca",vals,&nmax,&set);
    if(set) {
      if(nmax==bvp->numsrcpts) {
	for(i=0;i<nmax;i++) bvp->srcpts_amplitude[i]= (PetscScalar)vals[i];
      }
      else if(nmax==0) {for(i=0;i<bvp->numsrcpts;i++) bvp->srcpts_amplitude[i]= 1.0;}
      else if(nmax>bvp->numsrcpts) return OptArrayIgnored;
      else return OptArrayInComplete;
    }
    else {for(i=0;i<bvp->numsrcpts;i++) bvp->srcpts_amplitude[i]= 1.0;}
    PetscFree(vals);
  }

  /*
     quadrature order
  */
  PetscOptionsGetInt(bvp->prefix,"-quad_order",&(bvp->quad_order),&set);
  if(!set) bvp->quad_order= 2;

  /*
     Dirichlet strategy
  */
  PetscOptionsGetInt(bvp->prefix,"-diri_strat",(PetscInt*)&(bvp->diri_strat),&set);
  if(!set) {bvp->diri_strat= ZeroRowsColumns;}
  PetscOptionsGetScalar(bvp->prefix,"-tgv",&(bvp->tgv),&set);
  if(!set) bvp->tgv= 1.0;

  PetscFunctionReturn(0);
}


/* ---------------------------------------------------------------------------
   Viewer of a HelmBVP.
   --------------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__  "HelmBVPViewOnScreen"
MyErrCode HelmBVPViewOnScreen(HelmBVP bvp, MPI_Comm comm)
{
  const char *Dstrat[2]= {"ZeroRowsColumns","ZeroRows"};
  PetscFunctionBegin;

  PetscPrintf(comm,"/ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n");
  PetscPrintf(comm,"/       HelmBVP %s \n",bvp->myname);
  PetscPrintf(comm,"/\n");
  PetscPrintf(comm,"/  Domain: (%.2f,%.2f,%.2f,%.2f,%.2f,%.2f)\n",PetscRealPart(bvp->xmin),PetscRealPart(bvp->xmax),PetscRealPart(bvp->ymin),PetscRealPart(bvp->ymax),PetscRealPart(bvp->zmin),PetscRealPart(bvp->zmax));
  PetscPrintf(comm,"/  Frequency: %f\n",PetscRealPart(bvp->freq));
  PetscPrintf(comm,"/  Wave-speed file name: %s\n",bvp->wavespeedfilename);
  PetscPrintf(comm,"/  Wave-speed domain: (%.0f,%.0f,%.0f,%.0f,%.0f,%.0f)\n",PetscRealPart(bvp->wdomain[0]),PetscRealPart(bvp->wdomain[1]),PetscRealPart(bvp->wdomain[2]),PetscRealPart(bvp->wdomain[3]),PetscRealPart(bvp->wdomain[4]),PetscRealPart(bvp->wdomain[5]));
  PetscPrintf(comm,"/  Wave-speed number of cells: (%d,%d,%d)\n",bvp->siz_wavespeed[0],bvp->siz_wavespeed[1],bvp->siz_wavespeed[2]);
  PetscPrintf(comm,"/  Wave-speed averaging method from raw data to mesh:");
  switch (bvp->avm_w) {
  case ArithemeticMean: PetscPrintf(comm," ArithemeticMean\n"); break;
  case GeometricMean: PetscPrintf(comm," GeometricMean\n"); break;
  case HarmonicMean: PetscPrintf(comm," HarmonicMean\n"); break;
  case pArithemeticMean: PetscPrintf(comm," pArithemeticMean\n"); break;
  default: PetscPrintf(comm,"(wrong value!)\n");
  }
  PetscPrintf(comm,"/  Wavenumber function: ");
  if (bvp->ComputeWavenumber==(ArrayCtxFun)ComputeWavenumber_default) PetscPrintf(comm,"ComputeWavenumber_default\n");
  else if (bvp->ComputeWavenumber==PETSC_NULL) PetscPrintf(comm,"(null)");
  else PetscPrintf(comm,"(wrong value!)\n");
  PetscPrintf(comm,"/  Density file name: %s\n",bvp->densityfilename);
  PetscPrintf(comm,"/  Sign convention: %d\n",bvp->sign);
  PetscPrintf(comm,"/  Complex stretching function in PML: ");
  if (bvp->xi==(CtxFun)xi_default) PetscPrintf(comm,"xi_default\n");
  else PetscPrintf(comm,"(wrong value!)\n");
  PetscPrintf(comm,"/  Boundary conditions on outer faces: (%d,%d,%d,%d,%d,%d)\n",bvp->bctype_xl,bvp->bctype_xr,bvp->bctype_yl,bvp->bctype_yr,bvp->bctype_zl,bvp->bctype_zr);
  PetscPrintf(comm,"/  Order of Robin-like conditions: %d\n",bvp->robin_order);
  PetscPrintf(comm,"/  Robin parameters on faces: ");
  if (bvp->robin_pf==(ArrayCtxFun)robin_pf_T0) PetscPrintf(comm,"robin_pf_T0\n");
  else if (bvp->robin_pf==(ArrayCtxFun)robin_pf_T2) PetscPrintf(comm,"robin_pf_T2\n");
  else if (bvp->robin_pf==PETSC_NULL) PetscPrintf(comm,"(null)\n");
  else PetscPrintf(comm,"(wrong value!)\n");
  PetscPrintf(comm,"/  Robin parameters on edges: ");
  if (bvp->robin_pe==(ArrayCtxFun)robin_pe_T2) PetscPrintf(comm,"robin_pe_T2\n");
  else if (bvp->robin_pe==PETSC_NULL) PetscPrintf(comm,"(null)\n");
  else PetscPrintf(comm,"(wrong value!)\n");
  PetscPrintf(comm,"/  Robin parameters on vertices: ");
  if (bvp->robin_pv==(ArrayCtxFun)robin_pv_T2) PetscPrintf(comm,"robin_pv_T2\n");
  else if (bvp->robin_pv==PETSC_NULL) PetscPrintf(comm,"(null)\n");
  else PetscPrintf(comm,"(wrong value!)\n");
  PetscPrintf(comm,"/  Number of point sources: %d\n",bvp->numsrcpts);
  if (bvp->numsrcpts) {
    PetscPrintf(comm,"/  Source points: (%.2f,%.2f,%.2f), ...\n",PetscRealPart(bvp->srcpts_x[0]),PetscRealPart(bvp->srcpts_y[0]),PetscRealPart(bvp->srcpts_z[0]));
    PetscPrintf(comm,"/  Amplitude of point sources: %.0f, ...\n",PetscRealPart(bvp->srcpts_amplitude[0]));
  }
  PetscPrintf(comm,"/  Quadrature order for assembly: %d\n",bvp->quad_order);
  PetscPrintf(comm,"/  Dirichlet applying strategy and values on diagonal: %s\n",Dstrat[(PetscInt)(bvp->diri_strat)]);
  PetscPrintf(comm,"/  Dirichlet values scaling on diagonal: % .2f\n",bvp->tgv);
  PetscPrintf(comm,"/ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");

  PetscFunctionReturn(0);
}

/*
   Write curret options to a file.
*/
#undef __FUNCT__
#define __FUNCT__ "HelmBVPViewOptions"
MyErrCode HelmBVPViewOptions(HelmBVP bvp, const char *optsfile, MPI_Comm comm)
{

  PetscFunctionBegin;

  PetscFunctionReturn(0);
}


/* ---------------------------------------------------------------------------
    Assembly all: from reading cell data to creating matrix and rhs.
   --------------------------------------------------------------------------- */

/* To facilitate editing, separate files.  But the following functions are
 * highly coupled to HelmBVP, some even need to know selectable function members
 * of HelmBVP which are static in this file.  So we include sources instead of
 * headers. */
#include "helmdata.c"
#include "helmasmvol.c"
#include "helmldsrc.c"
#include "helmasmbc.c"

#define _HELM_DEBUG 0

#undef __FUNCT__
#define __FUNCT__ "HelmBVPAssembly"
MyErrCode HelmBVPAssembly(HelmBVP bvp, MyDOF dof, Mat *A, Vec *b)
{
  MyErrCode myerr;
  const PetscInt stencil_width= 27; /* assume Q1 element */

  PetscFunctionBegin;

  /*
     input check
  */
  if(!(dof->initialized)) return MyDOFNotInitialized;
  if(!(dof->dm->initialized)) return MyDMNotInitialized;

  /*
     get cell data: bvp->w, bvp->rho
  */
  if (!bvp->w) {myerr= HelmBVPInitCellData(bvp,dof->dm); CHKMyErrQ(myerr);}
  #if _HELM_DEBUG
  {
    PetscScalarView(dof->dm->nloc,bvp->w,PETSC_VIEWER_STDOUT_SELF);
    printf("w[0]=%f %+f i\n",PetscRealPart(bvp->w[0]),PetscImaginaryPart(bvp->w[0]));
  }
  #endif

  /*
     initialize matrix
  */
  MatCreate(dof->dm->comm,A); MatSetType(*A,MATAIJ);
  MatSetSizes(*A, dof->ndof_in, dof->ndof_in, dof->ndof, dof->ndof);
  if (dof->dm->p==1) MatSeqAIJSetPreallocation(*A,stencil_width,NULL);
#if 0
  else MatMPIAIJSetPreallocation(*A,stencil_width,NULL,stencil_width,NULL);
#else
  else {
    PetscInt ix, iy, iz, i, *dnnz, *onnz;
    PetscErrorCode ierr;
    const PetscInt sw1d=3;	/* Q1 1-D stencil width */
    ierr= PetscMalloc(dof->ndof_in*sizeof(PetscInt),&dnnz); CHKERRQ(ierr);
    ierr= PetscMalloc(dof->ndof_in*sizeof(PetscInt),&onnz); CHKERRQ(ierr);
    for (i=0,ix=0;ix<dof->ndof_xin;++ix)
      for (iy=0;iy<dof->ndof_yin;++iy)
	for (iz=0;iz<dof->ndof_zin;++iz, ++i) {
	  dnnz[i]= (sw1d-(ix==0 || ix==dof->ndof_xin-1))*(sw1d-(iy==0 || iy==dof->ndof_yin-1))
	    *(sw1d-(iz==0 || iz==dof->ndof_zin-1));
	  onnz[i]= stencil_width-dnnz[i];
	}
    MatMPIAIJSetPreallocation(*A,0,dnnz,0,onnz);
    PetscFree(dnnz); PetscFree(onnz);
  }
#endif
  MatSetLocalToGlobalMapping(*A,dof->ltog,dof->ltog);

  /*
     assembly of matrix
  */
  myerr= HelmBVPInteriorAssembly(bvp,dof,*A); CHKMyErrQ(myerr);
  myerr= HelmBVPRobinFacesAssembly(bvp,dof,*A); CHKMyErrQ(myerr);
  myerr= HelmBVPRobinEdgesAssembly(bvp,dof,*A); CHKMyErrQ(myerr);
  myerr= HelmBVPRobinVerticesAssembly(bvp,dof,*A); CHKMyErrQ(myerr);
  MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY); MatAssemblyEnd(*A,MAT_FINAL_ASSEMBLY);

  /*
     assembly of rhs

     TODO: inhomogeneous boundary conditions, RobinLoad
  */
  VecDuplicate(dof->v,b);
  HelmBVPInteriorLoad(bvp,dof,b);
  VecAssemblyBegin(*b); VecAssemblyEnd(*b);

  /*
     Dirichlet BC for matrix and rhs

     TODO: inhomogeneous BC
  */
  VecSet(dof->v,0);
  myerr= HelmBVPDirichletApply(bvp,dof,*A,*b,dof->v); CHKMyErrQ(myerr);

  #if _HELM_DEBUG
  {
    MatView(*A,PETSC_VIEWER_STDOUT_(dof->dm->comm));
    Mat B;
    MatTranspose(*A,MAT_INITIAL_MATRIX,&B);
    MatAXPY(B,-1.0,*A,DIFFERENT_NONZERO_PATTERN);
    MatView(B,PETSC_VIEWER_STDOUT_(dof->dm->comm));
    MatDestroy(&B);
    VecView(*b,PETSC_VIEWER_STDOUT_(dof->dm->comm));
  }
  #endif


  PetscFunctionReturn(0);
}

#undef _HELM_DEBUG
