#ifndef _FETISYS
#define _FETISYS

#include "fetidof.h"
#include <petscmat.h>

/*
   feti algebraic system
   
   {subdomain matrices, rhs, primal dof} -> {fetiop (including coarse op), fetirhs, fetipc}

   fetisol -> original solution
*/

typedef struct {
  
  
  
  /*
     coarse basis map: block-diagonal, each block corresponds to an
     intersection, each intersection is mastered by one subdomain
  */
  Mat Q;

} structFETISys, FETISys;

#endif
