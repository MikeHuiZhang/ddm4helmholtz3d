#ifndef _HELMBVP
#define _HELMBVP

#include "mydof.h"
#include "myio.h"
#include <petscmat.h>

#define HELMBVP_MAX_NAMELEN 100

#define HELMBVP_AS_ZERO 1e-10

typedef enum
{
  Neumann,
  Robin,
  Dirichlet
} HelmBCType;

typedef enum
{
  ZeroRowsColumns,
  ZeroRows
}DirichletStrategy;

typedef struct{
  MyDM dm;
  PetscScalar freq;
}AttenCtx;

typedef struct{
  MyDM dm;
  PetscScalar freq;
}c2wCtx;

typedef struct{
  PetscScalar wi;
  PetscScalar rho;
  PetscInt sign;
} TaylorCtx;

typedef struct{			/* for one side of the domain */
  PetscScalar *w;
  PetscScalar *rho;
  PetscInt sign;
  void *appendix;
} RobinCtx;

/*
   parameters for xi_default

   wi - wavenumber
   t  - distance to the physical domain
   L  - geometric thickness of the PML at the physical face

   The user is responsible to assure 0 < t < L.
*/
typedef struct{
  PetscInt sign;
  PetscScalar wi;
  PetscScalar t;
  PetscScalar L;
}xiDefaultCtx;

typedef PetscScalar (*CtxFun) (void *ctx);
typedef MyErrCode (*ArrayCtxFun) (PetscScalar *array, const void *ctx);

/*
   Cell context for assembly.
*/
typedef struct{
  PetscInt sign;
  PetscScalar wi;
  PetscScalar rho;
  CtxFun xi;
  PetscScalar t0[3];		/* absolute distance to physical domain of node 0 */
  PetscScalar t1[3];            /* and of node 7 */
  PetscScalar L[3];		/* geometric thickness of PML */
  PetscInt quad_order;		/* quadrature order */
  PetscScalar h[3];
} CellHelmCtxQ1;

/* Note.  Becareful to conversion from PetscReal to PetscScalar or vice versa.
   They have different sizes in memory. */
struct HelmBVP_
{
  /*
     appendix: if one wants to put more information here
  */
  void *appendix;
  MyErrCode (*createappendix)(void**);
  MyErrCode (*destroyappendix)(void**);

  /*
     name and options prefix
  */
  char *myname;
  char *prefix;

  /*
     physical domain: a cube
  */
  PetscScalar xmin, xmax, ymin, ymax, zmin, zmax;

  /*
     frequency, wavenumber, wavespeed, density
  */
  PetscScalar freq, *w, *rho;
  /* wavespeed */
  char *wavespeedfilename; 
  PetscInt siz_wavespeed[3]; 	/* num cells in x,y,z */
  PetscScalar wdomain[6];	/* xl,xr,yl,yr,zl,zr */
  AverageMethod avm_w; 
  AverageCtx *avmctx_w;		/* can not be setted from options */
  ArrayCtxFun AttenuateWavespeed;
  ArrayCtxFun ComputeWavenumber;
  /* density */
  char *densityfilename; 
  PetscInt siz_density[3];
  PetscScalar rhodomain[6];
  AverageMethod avm_rho; 
  AverageCtx *avmctx_rho;	/* can not be setted from options */
  

  /*
     sign convention: + for u(x,y,z)exp(iwt), - for u(x,y,z)exp(-iwt); + for
     exp(iw*<(i*sqrt(w^2-ky^2-kz^2),ky,kz),(x,y,z)> + iwt) outgoing to
     x -> -infty; + for radiation cond. Dn u + iw u = 0.
  */
  PetscInt sign;

  /*
     pml
  */
  CtxFun xi;

  /*
     boundary conditions outside pml (pml can be empty)
  */
  HelmBCType bctype_xl,bctype_xr,bctype_yl,bctype_yr,bctype_zl,bctype_zr;

  /*
     Robin parameters: (strong form for reference, actually different in weak
     form)

       Dn u + pf[0]*u - pf[1]*Lap_f u,        on faces, 

       (Dn1 + Dn2) u + pe[0]*u - pe[1]*Dtt u, on edges, 

       (Dn1 + Dn2 + Dn3) u + pv[0]*u, on vertices.

     The weak bilinear form is 

       (pf[0]*u,v)_F + (pf[1] D_F u,D_F v)_F + (pf[1]*pe[0]*u,v)_E +
       (pf[1]*pe[1]*D_E u, D_E v)_E + (pf[1]*pe[1]*pv[0]u,v)_V.
       
     So it is more convenient to use the coefficients of the above integrals as
     the parameters.
  */
  PetscInt robin_order;
  ArrayCtxFun robin_pf;
  ArrayCtxFun robin_pe;
  ArrayCtxFun robin_pv;
  MyErrCode (*robincreateapp)(void**); /* domain face context */
  MyErrCode (*robindestroyapp)(void**);
  MyErrCode (*robingetapp)(struct HelmBVP_*,RobinCtx*,PetscInt,PetscInt,PetscInt);
  MyErrCode (*robin_createcellctx)(void**); /* face cell context */
  MyErrCode (*robin_destroycellctx)(void **);
  MyErrCode (*robin_getcellctx)(void*,RobinCtx*,PetscInt);

  /*
     source terms
  */
  PetscInt numsrcpts;
  PetscScalar *srcpts_x, *srcpts_y, *srcpts_z;
  PetscScalar *srcpts_amplitude;

  /*
     quadrature order
  */
  PetscInt quad_order;

  /*
     strategy for dirichlet
  */
  DirichletStrategy diri_strat;
  PetscScalar tgv;

};

typedef struct HelmBVP_  structHelmBVP, *HelmBVP;

extern MyErrCode HelmBVPAssembly(HelmBVP bvp, MyDOF dof, Mat *A, Vec *b);

extern MyErrCode HelmBVPCreate(HelmBVP *p_bvp);

extern MyErrCode HelmBVPDestroy(HelmBVP *p_bvp);

extern MyErrCode HelmBVPSetDefaults(HelmBVP bvp);

extern MyErrCode HelmBVPSetPrefix(HelmBVP bvp, const char prefix[]);

extern MyErrCode HelmBVPSetFromOptions(HelmBVP bvp, const char filename[], const char *prefix);

extern MyErrCode HelmBVPSetDomain(HelmBVP bvp, PetscScalar xmin, PetscScalar xmax, PetscScalar ymin, PetscScalar ymax, PetscScalar zmin, PetscScalar zmax);

extern MyErrCode HelmBVPSetFreq(HelmBVP bvp, PetscScalar freq);

extern MyErrCode HelmBVPSetWavNumFun(HelmBVP bvp, ArrayCtxFun wavnum);

extern MyErrCode HelmBVPSetPMLxi(HelmBVP bvp,PetscScalar (*xi)(void *xi));

extern MyErrCode HelmBVPAppend(HelmBVP bvp, void *append);

extern MyErrCode HelmBVPSetCellDataFiles(HelmBVP bvp,const char *wavespeedfilename,const PetscInt siz_w[], const char *densityfilename, const PetscInt siz_rho[],AverageMethod method_w,const AverageCtx *ctx_w,AverageMethod method_rho,const AverageCtx *ctx_rho);

extern MyErrCode HelmBVPSetDataDomain(HelmBVP bvp,const PetscScalar wdomain[6],const PetscScalar rhodomain[6]);

extern MyErrCode HelmBVPInitCellData(HelmBVP bvp,MyDM dm);

extern MyErrCode HelmBVPSetName(HelmBVP bvp, const char* myname);

extern MyErrCode HelmBVPViewOnScreen(HelmBVP bvp, MPI_Comm comm);
extern MyErrCode HelmBVPViewOptions(HelmBVP bvp, const char *optsfile, MPI_Comm comm);

#endif

