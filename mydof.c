#include "mydof.h"

inline MyErrCode MyDOFCreate(MyDOF *p_dof)
{
  PetscErrorCode ierr;

  ierr= PetscMalloc(sizeof(structMyDOF),p_dof);CHKERRQ(ierr);
  PetscMemzero(*p_dof,sizeof(structMyDOF));
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "MyDOFDestroy"
MyErrCode MyDOFDestroy(MyDOF *p_dof)
{
  PetscFunctionBegin;
  if (!*p_dof) return(0);
#ifdef MyDOF_USE_AO
  AODestroy(&((*p_dof)->ao));
#endif
  ISLocalToGlobalMappingDestroy(&((*p_dof)->ltog));
  VecDestroy(&((*p_dof)->v));
  PetscFree((*p_dof)->NaturalIndices);
  PetscFree(*p_dof);
  PetscFunctionReturn(0);
}

inline MyErrCode MyDOFSetDM(MyDOF dof, MyDM dm)
{
  dof->dm= dm; dof->initialized= PETSC_FALSE; return 0;
}

inline MyErrCode MyDOFSetType(MyDOF dof, MyDOFType type)
{
  dof->type= type; return 0;
}

inline MyErrCode MyDOFGetType(MyDOF dof, MyDOFType *type)
{
  *type= dof->type; return 0;
}

static MyErrCode MyDOFSetDOFQ1(MyDOF dof);
static inline PetscInt MyDOFMapCellDOFQ1(PetscInt idof, PetscInt i0, PetscInt ndof_x, PetscInt ndof_y, PetscInt ndof_z);
static inline PetscInt MyDOFCellDOF0Q1(PetscInt ix, PetscInt iy, PetscInt iz, PetscInt nx, PetscInt ny, PetscInt nz);
static inline PetscInt MyDOFMapFaceDOFQ1(PetscInt idof, PetscInt i0, PetscInt ndof_x, PetscInt ndof_y);
static inline PetscInt MyDOFFaceDOF0Q1(PetscInt ix, PetscInt iy, PetscInt nx, PetscInt ny);
static inline PetscInt MyDOFEdgeDOF0Q1(PetscInt i, PetscInt n);
static inline PetscInt MyDOFMapEdgeDOFQ1(PetscInt idof,PetscInt i0,PetscInt ndof);
static MyErrCode MyDOFsearch1dQ1(PetscInt ix, PetscInt dim, const MyDM dm, PetscInt *rx, PetscInt *xins, PetscInt *xine);
static inline void MyDOFpetscid(const struct MyDOF_* dof,PetscInt ix, PetscInt iy, PetscInt iz, PetscInt *id);
static MyErrCode MyDOFQ1GetCoord1d(PetscScalar **coord,const struct MyDOF_ *dof,PetscInt dir,PetscInt idofs,PetscInt idofe);
static inline MyErrCode MyDOFQ1GetElem1d(PetscInt *ne, PetscInt **ie, const struct MyDOF_ *dof, PetscInt dir, PetscInt idof);

#undef __FUNCT__
#define __FUNCT__  "MyDOFSetup"
MyErrCode MyDOFSetup(MyDOF dof)
{
  MyErrCode myerr;
  dof->petscid= MyDOFpetscid;	/* needed by SetDOF */
  switch (dof->type) {
  case MyDOFQ1: default:
    dof->search1d= MyDOFsearch1dQ1;
    dof->getcoord1d= MyDOFQ1GetCoord1d;
    dof->getelem1d= MyDOFQ1GetElem1d;
    myerr= MyDOFSetDOFQ1(dof); CHKMyErrQ(myerr);
    break;
  }
#if 0
  {
    PetscInt ix= 10, iy= 10, iz= 10, id;
    dof->petscid(dof,ix,iy,iz,&id);
    PetscPrintf(dof->dm->comm,"dof (%d,%d,%d) has number %d in petsc ordering\n",ix,iy,iz,id);
  }
#endif
  return 0;
}

static MyErrCode MyDOFsearch1dQ1(PetscInt id, PetscInt dim, const MyDM dm, PetscInt *rd, PetscInt *dins, PetscInt *dine)
{				/* dm is used only with dm->search1d, dm->px, dm->nx_all */
  switch (dim) {
  case 0:
    if (id==dm->nx_all) {
      dm->search1d(id-1,dm->nx_all,dm->px,rd,dins,dine); (*dine)= dm->nx_all;
    }
    else if (id<dm->nx_all) {
      dm->search1d(id,dm->nx_all,dm->px,rd,dins,dine); (*dine)+= (*dine==dm->nx_all)?1:0;
    }
    else {printf("id=%d, dm->nx_all=%d\n",id,dm->nx_all); return IllegalIndex;}
    break;
  case 1:
    if (id==dm->ny_all) {
      dm->search1d(id-1,dm->ny_all,dm->py,rd,dins,dine); (*dine)= dm->ny_all;
    }
    else if (id<dm->ny_all) {
      dm->search1d(id,dm->ny_all,dm->py,rd,dins,dine); (*dine)+= (*dine==dm->ny_all)?1:0;
    }
    else return IllegalIndex;
    break;
  case 2:
    if (id==dm->nz_all) {
      dm->search1d(id-1,dm->nz_all,dm->pz,rd,dins,dine); (*dine)= dm->nz_all;
    }
    else if (id<dm->nz_all) {
      dm->search1d(id,dm->nz_all,dm->pz,rd,dins,dine); (*dine)+= (*dine==dm->nz_all)?1:0;
    }
    else return IllegalIndex;
    break;
  }
  return 0;
}

/* This function should not access dof->petscid. */
static inline void MyDOFpetscid(const struct MyDOF_* dof, PetscInt ix, PetscInt iy, PetscInt iz, PetscInt *id)
{
  PetscInt r, rx, xs, xe, ry, ys, ye, rz, zs, ze;
  MyErrCode myerr;
  /* printf("on %d (%d,%d,%d) -> ",dof->dm->rank,ix,iy,iz); */
  if (ix<dof->dofxine && ix>=dof->dofxins) {
    rx= dof->dm->rx; xs= dof->dofxins; xe= dof->dofxine;
  }
  else { myerr= dof->search1d(ix,0,dof->dm,&rx,&xs,&xe); CHKMyErrQ(myerr);}
  if (iy<dof->dofyine && iy>=dof->dofyins) {
    ry= dof->dm->ry; ys= dof->dofyins; ye= dof->dofyine;
  }
  else { myerr= dof->search1d(iy,1,dof->dm,&ry,&ys,&ye); CHKMyErrQ(myerr);}
  if (iz<dof->dofzine && iz>=dof->dofzins) {
    rz= dof->dm->rz; zs= dof->dofzins; ze= dof->dofzine;
  }
  else { myerr= dof->search1d(iz,2,dof->dm,&rz,&zs,&ze); CHKMyErrQ(myerr);}
  MyArraysub2ind(dof->dm->px,dof->dm->py,dof->dm->pz,rx,ry,rz,&r);
  (*id)= dof->ranges[r] + (ix-xs)*(ze-zs)*(ye-ys) + (iy-ys)*(ze-zs) + iz - zs;
  /* printf("%d\n",*id); */
}

/*
   get coordinates:

   [idof, idofe) is the range relative to on proc dof, i.e. a subset of [0,ndof_?in)
*/
static MyErrCode MyDOFQ1GetCoord1d(PetscScalar **coord,const struct MyDOF_ *dof,PetscInt dir,PetscInt idofs,PetscInt idofe)
{
  PetscInt i, start;
  PetscErrorCode ierr;
  ierr=PetscMalloc((idofe-idofs)*sizeof(PetscScalar),coord);CHKERRQ(ierr);
  switch (dir) {
  case 0:
    start= dof->dofxins;
    for (i=0;i<idofe-idofs;++i) {
      coord[0][i]= dof->dm->x[start+idofs+i];
    }
    break;
  case 1:
    start= dof->dofyins;
    for (i=0;i<idofe-idofs;++i) {
      coord[0][i]= dof->dm->y[start+idofs+i];
    }
    break;
  case 2:
    start= dof->dofzins;
    for (i=0;i<idofe-idofs;++i) {
      coord[0][i]= dof->dm->z[start+idofs+i];
    }
    break;
  default:
    return BadDirection;
  }
  return 0;
}

/*
   get the mesh elements where the given dof lies in

   idof is relative to on proc dof, ranging in [0,ndo_?in)

   ne is 1 or 2

   ie is element number on the global mesh
*/
static inline MyErrCode MyDOFQ1GetElem1d(PetscInt *ne, PetscInt **ie, const struct MyDOF_ *dof, PetscInt dir, PetscInt idof)
{
  PetscErrorCode ierr, gdof;
  if (dir==0) {
    gdof= dof->dofxins+idof;
    (*ne)= (gdof==0 || gdof==dof->ndof_x-1) ? 1:2;
    ierr=PetscMalloc((*ne)*sizeof(PetscInt),ie);CHKERRQ(ierr);
    if (gdof==0)
      (*ie)[0]= 0;
    else if (gdof==dof->ndof_x-1)
      (*ie)[0]= dof->dm->nx_all-1;
    else {
      (*ie)[0]= gdof-1; (*ie)[1]= gdof;
    }
  }
  else if (dir==1) {
    gdof= dof->dofyins+idof;
    (*ne)= (gdof==0 || gdof==dof->ndof_y-1) ? 1:2;
    ierr=PetscMalloc((*ne)*sizeof(PetscInt),ie);CHKERRQ(ierr);
    if (gdof==0)
      (*ie)[0]= 0;
    else if (gdof==dof->ndof_y-1)
      (*ie)[0]= dof->dm->ny_all-1;
    else {
      (*ie)[0]= gdof-1; (*ie)[1]= gdof;
    }
  }
  else if (dir==2) {
    gdof= dof->dofzins+idof;
    (*ne)= (gdof==0 || gdof==dof->ndof_z-1) ? 1:2;
    ierr=PetscMalloc((*ne)*sizeof(PetscInt),ie);CHKERRQ(ierr);
    if (gdof==0)
      (*ie)[0]= 0;
    else if (gdof==dof->ndof_z-1)
      (*ie)[0]= dof->dm->nz_all-1;
    else {
      (*ie)[0]= gdof-1; (*ie)[1]= gdof;
    }
  }
  else return BadDirection;
  return 0;
}

/*
   If use AO or Comm, collective in dof->dm.
*/
#undef __FUNCT__
#define __FUNCT__  "MyDOFSetDOFQ1"
MyErrCode MyDOFSetDOFQ1(MyDOF dof)
{
  PetscInt *PetscIndices=0;
#ifdef MyDOF_USE_AO
  PetscLogStage stage1, stage2;
#elif defined MyDOF_USE_Comm
  PetscInt idx, disp_send[3], disp_recv[3];
  MPI_Status status;
#else
  PetscInt il;			/* local index */
#endif

  PetscInt i,j,k, count=0;

  PetscFunctionBegin;
  if(!(dof->dm)) return MyDMUndefined;
  if(!(dof->dm->initialized)) return MyDMNotInitialized;

  /* cell and global dof */
  dof->ndof_cell= 8; dof->ndofx_cell= 2; dof->ndofy_cell= 2; dof->ndofz_cell= 2;
  dof->ndof_x= dof->dm->nx_all+1;
  dof->ndof_y= dof->dm->ny_all+1;
  dof->ndof_z= dof->dm->nz_all+1;
  dof->ndof= dof->ndof_x*(dof->ndof_y)*(dof->ndof_z);
  dof->ndofpmlxl= dof->dm->eta_xl; dof->ndofpmlxr= dof->dm->eta_xr;
  dof->ndofpmlyl= dof->dm->eta_yl; dof->ndofpmlyr= dof->dm->eta_yr;
  dof->ndofpmlzl= dof->dm->eta_zl; dof->ndofpmlzr= dof->dm->eta_zr;

  /* local pml ndof including ghosted pml dof, but physical boundary is not counted */
  dof->npmlxl_loc= dof->dm->etaloc_xl+(dof->dm->ixend<dof->dm->eta_xl?1:0);
  dof->npmlxr_loc= dof->dm->etaloc_xr+(dof->dm->ixbegin>dof->dm->nx_phy?1:0);
  dof->npmlyl_loc= dof->dm->etaloc_yl+(dof->dm->iyend<dof->dm->eta_yl?1:0);
  dof->npmlyr_loc= dof->dm->etaloc_yr+(dof->dm->iybegin>dof->dm->ny_phy?1:0);
  dof->npmlzl_loc= dof->dm->etaloc_zl+(dof->dm->izend<dof->dm->eta_zl?1:0);
  dof->npmlzr_loc= dof->dm->etaloc_zr+(dof->dm->izbegin>dof->dm->nz_phy?1:0);

  /* local pml ndof excluding ghosted */
  dof->npmlxl_in= dof->dm->etaloc_xl;
  dof->npmlyl_in= dof->dm->etaloc_yl;
  dof->npmlzl_in= dof->dm->etaloc_zl;
  dof->npmlxr_in= PetscMax(0,dof->npmlxr_loc-(dof->dm->ixend!=dof->dm->nx_all?1:0));
  dof->npmlyr_in= PetscMax(0,dof->npmlyr_loc-(dof->dm->iyend!=dof->dm->ny_all?1:0));
  dof->npmlzr_in= PetscMax(0,dof->npmlzr_loc-(dof->dm->izend!=dof->dm->nz_all?1:0));

  /* local dof including ghosted, relative to the pml-augmented domain, [s,e) */
  dof->dofxlocs= dof->dm->ixbegin; dof->dofxloce= dof->dm->ixend+1;
  dof->dofylocs= dof->dm->iybegin; dof->dofyloce= dof->dm->iyend+1;
  dof->dofzlocs= dof->dm->izbegin; dof->dofzloce= dof->dm->izend+1;
  dof->ndof_xloc= dof->dofxloce - dof->dofxlocs;
  dof->ndof_yloc= dof->dofyloce - dof->dofylocs;
  dof->ndof_zloc= dof->dofzloce - dof->dofzlocs;
  dof->ndof_loc= dof->ndof_xloc*(dof->ndof_yloc)*(dof->ndof_zloc);

  /* local dof excluding ghosted, relative to the pml-augmented domain, [s e) */
  dof->dofxins= dof->dofxlocs; dof->dofxine= dof->dofxloce-((dof->dofxloce!=dof->ndof_x)?1:0);
  dof->dofyins= dof->dofylocs; dof->dofyine= dof->dofyloce-((dof->dofyloce!=dof->ndof_y)?1:0);
  dof->dofzins= dof->dofzlocs; dof->dofzine= dof->dofzloce-((dof->dofzloce!=dof->ndof_z)?1:0);
  dof->ndof_xin= dof->dofxine - dof->dofxins;
  dof->ndof_yin= dof->dofyine - dof->dofyins;
  dof->ndof_zin= dof->dofzine - dof->dofzins;
  dof->ndof_in= dof->ndof_xin*(dof->ndof_yin)*(dof->ndof_zin);

  /* parallel Vec corresponding to global dof */
  VecCreateMPI(dof->dm->comm,dof->ndof_in,dof->ndof,&(dof->v)); /* collective */
  /* printf("vec local size %d, global size%d\n",dof->ndof_in,dof->ndof); */
  VecGetOwnershipRange(dof->v,&(dof->dofins),&(dof->dofine));	/* not collective */
  VecGetOwnershipRanges(dof->v,&(dof->ranges));			/* not collective */

  /* NaturalIndices: non-overlapping between processors */
  PetscMalloc(sizeof(PetscInt)*(dof->ndof_in),&(dof->NaturalIndices));
  for(count=0,i=dof->dofxins;i<dof->dofxine;++i)
    for(j=dof->dofyins;j<dof->dofyine;++j)
      for(k=dof->dofzins;k<dof->dofzine;++k, ++count) {
  	MyArraysub2ind(dof->ndof_x,dof->ndof_y,dof->ndof_z,i,j,k,dof->NaturalIndices+count);
      }

  /* AO: optional */
#ifdef MyDOF_USE_AO
  PetscLogStageRegister("AOCreate",&stage1);
  PetscLogStagePush(stage1);
  PetscMalloc(sizeof(PetscInt)*(dof->ndof_in),&PetscIndices);
  for(count=0,i=dof->dofins;i<dof->dofine;++i) PetscIndices[count++]= i;
  AOCreateBasic(dof->dm->comm,dof->ndof_in,dof->NaturalIndices,PetscIndices,&(dof->ao)); /* collective */
  PetscFree(PetscIndices);
  PetscLogStagePop();
#endif

  /* ISLocalToGlobalMapping */
  PetscMalloc(sizeof(PetscInt)*(dof->ndof_loc),&PetscIndices);
  /*   method 1: use AO */
#ifdef MyDOF_USE_AO
  PetscLogStageRegister("AO Mapping",&stage2);
  PetscLogStagePush(stage2);
  for (count= 0, i=dof->dofxlocs; i<dof->dofxloce; ++i)
    for (j=dof->dofylocs; j<dof->dofyloce; ++j)
      for (k=dof->dofzlocs; k<dof->dofzloce; ++k) {
  	MyArraysub2ind(dof->ndof_x,dof->ndof_y,dof->ndof_z,i,j,k,PetscIndices+count);
  	count++;
      }
  AOApplicationToPetsc(dof->ao,dof->ndof_loc,PetscIndices);
  PetscLogStagePop();
  /*   method 2: local communication without AO */
#elif defined MyDOF_USE_Comm
  /*      no AO so use IS */
  /*      dof on this processor  */
  for (count=dof->dofins,i=dof->dofxins; i<dof->dofxine; ++i) {
    for (j=dof->dofyins; j<dof->dofyine; ++j) {
      idx= (i-dof->dofxlocs)*(dof->ndof_zloc)*(dof->ndof_yloc)+(j-dof->dofylocs)*(dof->ndof_zloc);
      for (k=dof->dofzins; k<dof->dofzine; ++k, ++idx, ++count) {
	PetscIndices[idx]= count;
      }
    }
  }
  /*      ghosted face 1, receive from the right and send to the left; note that
   *      the non-overlapping partition of dof to processors is assumed close at
   *      left open at right (or close if right side is on external boundary)*/
  MPI_Sendrecv(&(dof->dofins), 1, MPI_INT,
	       (dof->dm->rx)?(dof->dm->rank-dof->dm->py*(dof->dm->pz)):MPI_PROC_NULL, 100,
	       &count, 1, MPI_INT,
	       (dof->dm->rx+1-dof->dm->px)?(dof->dm->rank+dof->dm->py*(dof->dm->pz)):MPI_PROC_NULL, 100,
	       dof->dm->comm, &status); /* collective */
  if (dof->dofxloce!=dof->dofxine) /* if non-equal, must be different by 1 */
  for (j=dof->dofyins; j<dof->dofyine; ++j) {
    idx= dof->ndof_xin*(dof->ndof_zloc)*(dof->ndof_yloc)+(j-dof->dofylocs)*(dof->ndof_zloc);
    for (k=dof->dofzins; k<dof->dofzine; ++k, ++idx, ++count) {
      PetscIndices[idx]= count;
    }
  }
  /*      ghosted face 2, recv from the back and send to the front */
  disp_send[0]= dof->dofins; disp_send[1]= dof->ndof_yin;
  MPI_Sendrecv(disp_send, 2, MPI_INT,
	       (dof->dm->ry)?(dof->dm->rank-dof->dm->pz):MPI_PROC_NULL, 10,
	       disp_recv, 2, MPI_INT,
	       (dof->dm->ry+1-dof->dm->py)?(dof->dm->rank+dof->dm->pz):MPI_PROC_NULL, 10,
	       dof->dm->comm, &status); /* collective */
  if (dof->dofyloce!=dof->dofyine) /* if non-equal, must be different by 1 */
  for (i=dof->dofxins; i<dof->dofxine; ++i) {
    idx= (i-dof->dofxlocs)*(dof->ndof_zloc)*(dof->ndof_yloc)+dof->ndof_yin*(dof->ndof_zloc);
    count= disp_recv[0] + (i-dof->dofxins)*(dof->ndof_zin)*(disp_recv[1]);
      for (k=dof->dofzins; k<dof->dofzine; ++k, ++idx, ++count) {
      PetscIndices[idx]= count;
    }
  }
  /*      ghosted face 3, recv from the top and send to the bottom */
  disp_send[0]= dof->dofins; disp_send[1]= dof->ndof_zin;
  MPI_Sendrecv(disp_send, 2, MPI_INT,
	       (dof->dm->rz)?(dof->dm->rank-1):MPI_PROC_NULL, 1,
	       disp_recv, 2, MPI_INT,
	       (dof->dm->rz+1-dof->dm->pz)?(dof->dm->rank+1):MPI_PROC_NULL, 1,
	       dof->dm->comm, &status); /* collective */
  if (dof->dofzloce!=dof->dofzine) /* if non-equal, must be different by 1 */
  for (i=dof->dofxins; i<dof->dofxine; ++i) {
    for (j=dof->dofyins; j<dof->dofyine; ++j) {
      idx= (i-dof->dofxlocs)*(dof->ndof_zloc)*(dof->ndof_yloc) + (j-dof->dofylocs)*(dof->ndof_zloc) + dof->ndof_zin;
      count= disp_recv[0] + (i-dof->dofxins)*(dof->ndof_yin)*(disp_recv[1]) + (j-dof->dofyins)*(disp_recv[1]);
      PetscIndices[idx]= count;
    }
  }
  /*      ghosted edge 1, recv from top-right and send to bottom-left */
  disp_send[0]= dof->dofins; disp_send[1]= dof->ndof_zin;
  MPI_Sendrecv(disp_send, 2, MPI_INT,
	       (dof->dm->rz && dof->dm->rx)?(dof->dm->rank-1-dof->dm->py*(dof->dm->pz)):MPI_PROC_NULL, 101,
	       disp_recv, 2, MPI_INT,
	       (dof->dm->rz+1-dof->dm->pz && dof->dm->rx+1-dof->dm->px)?(dof->dm->rank+1+dof->dm->py*(dof->dm->pz)):MPI_PROC_NULL, 101,
	       dof->dm->comm, &status); /* collective */
  if (dof->dofzloce!=dof->dofzine && dof->dofxloce!=dof->dofxine) /* if non-equal, must be different by 1 */
    for (j=dof->dofyins; j<dof->dofyine; ++j) {
      idx= dof->ndof_xin*(dof->ndof_zloc)*(dof->ndof_yloc) + (j-dof->dofylocs)*(dof->ndof_zloc) + dof->ndof_zin;
      count= disp_recv[0] + (j-dof->dofyins)*(disp_recv[1]);
      PetscIndices[idx]= count;
    }
  /*      ghosted edge 2, recv from back-right and send to front-left */
  disp_send[0]= dof->dofins;
  MPI_Sendrecv(disp_send, 1, MPI_INT,
	       (dof->dm->ry && dof->dm->rx)?(dof->dm->rank-dof->dm->pz-dof->dm->py*(dof->dm->pz)):MPI_PROC_NULL, 110,
	       disp_recv, 1, MPI_INT,
	       (dof->dm->ry+1-dof->dm->py && dof->dm->rx+1-dof->dm->px)?(dof->dm->rank+dof->dm->pz+dof->dm->py*(dof->dm->pz)):MPI_PROC_NULL, 110,
	       dof->dm->comm, &status); /* collective */
  if (dof->dofyloce!=dof->dofyine && dof->dofxloce!=dof->dofxine) /* if non-equal, must be different by 1 */
    for (count= disp_recv[0], idx= dof->ndof_xin*(dof->ndof_zloc)*(dof->ndof_yloc)
	 + dof->ndof_yin*(dof->ndof_zloc), k=dof->dofzins; k<dof->dofzine;
	 PetscIndices[idx]= count, ++k, ++count, ++idx){}
  /*      ghosted edge 3, recv from top-back and send to bottom-front */
  disp_send[0]= dof->dofins; disp_send[1]= dof->ndof_yin; disp_send[2]= dof->ndof_zin;
  MPI_Sendrecv(disp_send, 3, MPI_INT,
	       (dof->dm->rz && dof->dm->ry)?(dof->dm->rank-1-dof->dm->pz):MPI_PROC_NULL, 11,
	       disp_recv, 3, MPI_INT,
	       (dof->dm->rz+1-dof->dm->pz && dof->dm->ry+1-dof->dm->py)?(dof->dm->rank+1+dof->dm->pz):MPI_PROC_NULL, 11,
	       dof->dm->comm, &status); /* collective */
  if (dof->dofzloce!=dof->dofzine && dof->dofyloce!=dof->dofyine) /* if non-equal, must be different by 1 */
    for (count= disp_recv[0], idx=dof->ndof_yin*(dof->ndof_zloc) + dof->ndof_zin, i=dof->dofxins;
	 i<dof->dofxine; ++i, idx+=(dof->ndof_zloc)*(dof->ndof_yloc), count+=(disp_recv[1])*(disp_recv[2])) {
      PetscIndices[idx]= count;
    }
  /*      ghosted vertex: only one */
  disp_send[0]= dof->dofins;
  MPI_Sendrecv(disp_send, 1, MPI_INT,
	       (dof->dm->ry && dof->dm->rx && dof->dm->rz)?(dof->dm->rank-1-dof->dm->pz-dof->dm->py*(dof->dm->pz)):MPI_PROC_NULL, 111,
	       disp_recv, 1, MPI_INT,
	       (dof->dm->ry+1-dof->dm->py && dof->dm->rz+1-dof->dm->pz && dof->dm->rx+1-dof->dm->px)?(dof->dm->rank+1+dof->dm->pz+dof->dm->py*(dof->dm->pz)):MPI_PROC_NULL, 111,dof->dm->comm, &status); /* collective */
  if (dof->dofyloce!=dof->dofyine && dof->dofxloce!=dof->dofxine && dof->dofzloce!=dof->dofzine) /* if non-equal, must be different by 1 */
    PetscIndices[dof->ndof_loc-1]= disp_recv[0];
#else
  /* method 3: compute IS locally without any communication */
   /*  dof with ghosted for this processor  */
  for (il=0, i=dof->dofxlocs; i<dof->dofxloce; ++i) {
    for (j=dof->dofylocs; j<dof->dofyloce; ++j) {
      /* il= (i-dof->dofxlocs)*(dof->ndof_zloc)*(dof->ndof_yloc)+(j-dof->dofylocs)*(dof->ndof_zloc); */
      for (k=dof->dofzlocs; k<dof->dofzloce; ++k, ++il) {
	/* printf("(i,j,k)=(%d,%d,%d)\n",i,j,k); */
	dof->petscid(dof,i,j,k,PetscIndices+il);
      }
    }
  }
#endif

  ISLocalToGlobalMappingCreate(dof->dm->comm,1,dof->ndof_loc,PetscIndices,PETSC_OWN_POINTER,&(dof->ltog));
  VecSetLocalToGlobalMapping(dof->v,dof->ltog);

  /* dof maps */
  dof->celldof0= MyDOFCellDOF0Q1; dof->celldofi= MyDOFMapCellDOFQ1;
  dof->facedof0= MyDOFFaceDOF0Q1; dof->facedofi= MyDOFMapFaceDOFQ1;
  dof->edgedof0= MyDOFEdgeDOF0Q1; dof->edgedofi= MyDOFMapEdgeDOFQ1;

  /* finishing */
  dof->initialized= PETSC_TRUE;
  PetscFunctionReturn(0);
}

/*
   View an instance of MyDOF
*/
#undef __FUNCT__
#define __FUNCT__ "MyDOFViewOnScreen"
MyErrCode MyDOFViewOnScreen(MyDOF dof)
{
  PetscFunctionBegin;
  PetscPrintf(dof->dm->comm,"/ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n");
  PetscPrintf(dof->dm->comm,"/       MyDOF on MyDM %s (bounds are describing the interval [ )) \n",dof->dm->myname);
  PetscPrintf(dof->dm->comm,"/\n");
  /* MyDMViewOnScreen(dof->dm); */
  PetscPrintf(dof->dm->comm,"/  N.D.O.F. per cell: %d\n",dof->ndof_cell);
  PetscPrintf(dof->dm->comm,"/  N.D.O.F. on mesh: %d = %d*%d*%d\n",dof->ndof,dof->ndof_x,dof->ndof_y,dof->ndof_z);
  PetscSynchronizedPrintf(dof->dm->comm,"/  Processor %d: \n",dof->dm->rank);
  PetscSynchronizedPrintf(dof->dm->comm,"/     N.D.O.F. (    ghosted): %d = %d*%d*%d\n",dof->ndof_loc,dof->ndof_xloc,dof->ndof_yloc,dof->ndof_zloc);
  PetscSynchronizedPrintf(dof->dm->comm,"/     N.D.O.F. (non-ghosted): %d = %d*%d*%d\n",dof->ndof_in,dof->ndof_xin,dof->ndof_yin,dof->ndof_zin);
  PetscSynchronizedPrintf(dof->dm->comm,"/     Bounds of D.O.F. (including ghosted): 3D (%d, %d, %d, %d, %d, %d)\n",dof->dofxlocs,dof->dofxloce,dof->dofylocs,dof->dofyloce,dof->dofzlocs,dof->dofzloce);
  PetscSynchronizedPrintf(dof->dm->comm,"/     Bounds of D.O.F. (excluding ghosted): 3D (%d, %d, %d, %d, %d, %d), 1D (%d,%d)\n",dof->dofxins,dof->dofxine,dof->dofyins,dof->dofyine,dof->dofzins,dof->dofzine,dof->dofins,dof->dofine);
  PetscSynchronizedPrintf(dof->dm->comm,"/     N.D.O.F. in local non-ghosted pml: (%d, %d, %d, %d, %d, %d)\n",dof->npmlxl_in,dof->npmlxr_in,dof->npmlyl_in,dof->npmlyr_in,dof->npmlzl_in,dof->npmlzr_in);
  PetscSynchronizedFlush(dof->dm->comm,PETSC_STDOUT);
#ifdef MyDOF_USE_AO
  PetscPrintf(dof->dm->comm,"/   AO: \n");
  AOView(dof->ao,PETSC_VIEWER_STDOUT_(dof->dm->comm));
#endif
  /* ISLocalToGlobalMappingView(dof->ltog,PETSC_VIEWER_STDOUT_(dof->dm->comm)); */
  PetscPrintf(dof->dm->comm,"/ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
  PetscFunctionReturn(0);
}

/*
   map: (local) cell numbering + cell-wise dof numbering
     -> (local) mesh-wise dof numbering

   cell numbering of Q1 nodal dof: by first counting z, then y, x
   assuming that 0->2 is y-increasing, 0->1 is z-incresing, 0->4 is x-increasing

       3-----7
      /|    /|
     / |   / |
    1-----5  |
    |  2--|--6
    | /   | /
    |/    |/
    0-----4

    mesh numbering of dof: the same as cell, first counting z, then y, x

    i0: mesh number of the 0 node in the cell, in natural order

    ndof_x, ndof_y, ndof_z: number of dof
*/
inline PetscInt MyDOFMapCellDOFQ1(PetscInt idof, PetscInt i0, PetscInt ndof_x, PetscInt ndof_y, PetscInt ndof_z)
{
  switch (idof)
  {
  case 0: case 1:
    return i0+idof; break;
  case 2: case 3:
    return i0+ndof_z+idof-2; break;
  case 4: case 5:
    return i0+ndof_z*ndof_y+idof-4; break;
  case 6: case 7:
    return i0+ndof_z*ndof_y+ndof_z+idof-6; break;
  default:
    return -1;			/* error index */
  }
}
/*
   cell location -> number on mesh of the 0 node on this cell, in natural order

   ix, iy, iz: location of the cell
   nx, ny, nz: number of cells
*/
inline PetscInt MyDOFCellDOF0Q1(PetscInt ix, PetscInt iy, PetscInt iz, PetscInt nx,PetscInt ny,PetscInt nz)
{
  return ix*(ny+1)*(nz+1)+iy*(nz+1)+iz;
}

inline PetscInt MyDOFFaceDOF0Q1(PetscInt ix, PetscInt iy, PetscInt nx,PetscInt ny)
{
  return ix*(ny+1)+iy;
}

inline PetscInt MyDOFMapFaceDOFQ1(PetscInt idof, PetscInt i0, PetscInt ndof_x, PetscInt ndof_y)
{
  switch (idof)
  {
  case 0: case 1:
    return i0+idof; break;
  case 2: case 3:
    return i0+ndof_y+idof-2; break;
  default:
    return -1;			/* error index */
  }
}

inline PetscInt MyDOFEdgeDOF0Q1(PetscInt i, PetscInt n)
{
  return i;
}

inline PetscInt MyDOFMapEdgeDOFQ1(PetscInt idof,PetscInt i0,PetscInt ndof)
{
  switch (idof){
  case 0: case 1:
    return idof+i0; break;
  default:
    return -1;
  }
}
