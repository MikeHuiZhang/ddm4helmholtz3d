#include "mydm.h"

static inline void mypart1d(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *nxp, PetscInt *startx,PetscInt *endx);

static inline void mysearch1d(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *rx,PetscInt *startx,PetscInt *endx);

#undef __FUNCT__
#define __FUNCT__ "MyDMCreate"
inline MyErrCode MyDMCreate(MyDM *p_dm)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr=PetscMalloc(sizeof(structMyDM),p_dm);CHKERRQ(ierr);
  PetscMemzero(*p_dm,sizeof(structMyDM));
  (*p_dm)->part1d= mypart1d; (*p_dm)->search1d= mysearch1d;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyDMDestroy"
inline MyErrCode MyDMDestroy(MyDM *p_dm)
{
  PetscFunctionBegin;
  if (!*p_dm) return(0);
  PetscFree((*p_dm)->x); PetscFree((*p_dm)->y); PetscFree((*p_dm)->z);
  PetscFree((*p_dm)->myname); PetscFree((*p_dm)->prefix);
  PetscFree(*p_dm);
  PetscFunctionReturn(0);
}

/* ---------------------------------------------------------------------------
   Set a single group of members.
   --------------------------------------------------------------------------- */

#undef __FUNCT__
#define __FUNCT__ "MyDMSetComm"
MyErrCode MyDMSetComm(MyDM dm,MPI_Comm comm,PetscInt px,PetscInt py,PetscInt pz)
{
  PetscMPIInt p;
  MyErrCode myerr=0;

  PetscFunctionBegin;
  if(!dm){myerr=MyDMUndefined;return myerr;}
  if(!comm){myerr=CommUndefined;return myerr;}
  MPI_Comm_size(comm,&p);
  if(px*py*pz-p || px<=0 || py<=0 || pz<=0){myerr=NumProcWrongFactors;return myerr;}
  dm->comm= comm; dm->px= px; dm->py= py; dm->pz= pz; dm->p= p;
  MPI_Comm_rank(comm,&(dm->rank));
  myerr= MyArrayind2sub(px,py,pz,&(dm->rx),&(dm->ry),&(dm->rz),dm->rank);
  CHKMyErrQ(myerr);
  dm->initialized= PETSC_FALSE;
  PetscFunctionReturn(0);
}

/*
   Set comm from options.  If no options found on the command line, set default
   values.
*/
#undef __FUNCT__
#define __FUNCT__ "MyDMSetCommFromOptions"
MyErrCode MyDMSetCommFromOptions(MyDM dm,MPI_Comm comm)
{
  PetscMPIInt p;
  MyErrCode myerr=0;
  PetscErrorCode ierr;
  PetscInt px=0,py=0,pz=0;
  PetscBool set;

  PetscFunctionBegin;
  if(!dm){myerr=MyDMUndefined;return myerr;}
  if(!comm){myerr=CommUndefined;return myerr;}
  MPI_Comm_size(comm,&p);
  ierr= PetscOptionsGetInt(dm->prefix,"-px",&px,&set);CHKERRQ(ierr);if(!set){px=1;}
  ierr= PetscOptionsGetInt(dm->prefix,"-py",&py,&set);CHKERRQ(ierr);if(!set){py=1;}
  ierr=PetscOptionsGetInt(dm->prefix,"-pz",&pz,&set);CHKERRQ(ierr);if(!set){pz=p/px/py;}
  if(px*py*pz-p || px<=0 || py<=0 || pz<=0){myerr=NumProcWrongFactors;return myerr;}
  dm->comm= comm; dm->px= px; dm->py= py; dm->pz= pz; dm->p= p;
  MPI_Comm_rank(comm,&(dm->rank));
  myerr= MyArrayind2sub(px,py,pz,&(dm->rx),&(dm->ry),&(dm->rz),dm->rank);
  CHKMyErrQ(myerr);
  dm->initialized= PETSC_FALSE;
  PetscFunctionReturn(0);
}

/*
   Set number of physical cells from the command line.  If no options found on
   the command line, set default values.
*/
#undef __FUNCT__
#define __FUNCT__  "MyDMSetNumCellsFromOptions"
inline MyErrCode MyDMSetNumCellsFromOptions(MyDM dm)
{
  PetscBool set;

  PetscFunctionBegin;
  PetscOptionsGetInt(dm->prefix,"-nx",&(dm->nx_phy),&set);
  if(!set){dm->nx_phy= 4;}
  PetscOptionsGetInt(dm->prefix,"-ny",&(dm->ny_phy),&set);
  if(!set){dm->ny_phy= 4;}
  PetscOptionsGetInt(dm->prefix,"-nz",&(dm->nz_phy),&set);
  if(!set){dm->nz_phy= 4;}
  dm->initialized= PETSC_FALSE;
  PetscFunctionReturn(0);
}

inline MyErrCode MyDMSetNumCells(MyDM dm, PetscInt nx_phy, PetscInt ny_phy, PetscInt nz_phy)
{
  if(nx_phy<=0 || ny_phy<=0 || nz_phy<=0){return WrongNumCells;}
  dm->nx_phy= nx_phy; dm->ny_phy= ny_phy; dm->nz_phy= nz_phy;
  dm->initialized= PETSC_FALSE;
  return 0;
}

/*
   Set number of cells in PML from the command line.  If no options found, set
   default values.
*/
#undef __FUNCT__
#define __FUNCT__ "MyDMSetNumPMLFromOptions"
inline MyErrCode MyDMSetNumPMLFromOptions(MyDM dm)
{
  PetscInt *vals, nmax=6, i;
  PetscBool set;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr=PetscMalloc(sizeof(PetscInt)*nmax,&vals);CHKERRQ(ierr);
  PetscOptionsGetIntArray(dm->prefix,"-eta",vals,&nmax,&set);
  for(i=nmax;i<6;++i){vals[i]=0;}
  dm->eta_xl= vals[0]; dm->eta_xr= vals[1]; dm->eta_yl= vals[2];
  dm->eta_yr= vals[3]; dm->eta_zl= vals[4]; dm->eta_zr= vals[5];
  PetscFree(vals);
  dm->initialized= PETSC_FALSE;
  PetscFunctionReturn(0);
}

inline MyErrCode MyDMSetNumPML(MyDM dm, PetscInt eta_xl, PetscInt eta_xr, PetscInt eta_yl,PetscInt eta_yr, PetscInt eta_zl, PetscInt eta_zr)
{
  if(eta_xl<0 || eta_xr<0 || eta_yl<0 || eta_yr<0 || eta_zl<0 || eta_zr<0){return WrongNumPML;}
  dm->eta_xl= eta_xl; dm->eta_xr= eta_xr; dm->eta_yl= eta_yl; dm->eta_yr= eta_yr; dm->eta_zl= eta_zl; dm->eta_zr= eta_zr;
  dm->initialized= PETSC_FALSE;
  return 0;
}


/*
   Set uniform coordinates including PML.

   xmin, xmax, ..  physical domain
*/
static inline PetscErrorCode SetUniformCoord1D(PetscScalar **x, PetscScalar xmin, PetscScalar xmax, PetscInt nx_all, PetscInt eta_xl, PetscInt eta_xr)
{
  PetscErrorCode ierr;
  PetscInt i;
  PetscScalar h;

  ierr=PetscMalloc(sizeof(PetscScalar)*(nx_all+1),x);CHKERRQ(ierr);
  h= (xmax-xmin)/(nx_all-eta_xl-eta_xr);
  for(i=0;i<nx_all+1;++i){(*x)[i]= xmin + (i-eta_xl)*h;}
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "MyDMSetUniformCoord"
inline MyErrCode MyDMSetUniformCoord(MyDM dm,PetscScalar xmin, PetscScalar xmax, PetscScalar ymin, PetscScalar ymax, PetscScalar zmin, PetscScalar zmax)
{
  PetscFunctionBegin;
  if(!(dm->initialized)){return MyDMNotInitialized;}
  SetUniformCoord1D(&(dm->x),xmin,xmax,dm->nx_all,dm->eta_xl,dm->eta_xr);
  SetUniformCoord1D(&(dm->y),ymin,ymax,dm->ny_all,dm->eta_yl,dm->eta_yr);
  SetUniformCoord1D(&(dm->z),zmin,zmax,dm->nz_all,dm->eta_zl,dm->eta_zr);
  dm->has_coord= PETSC_TRUE;
  PetscFunctionReturn(0);
}

inline MyErrCode MyDMSetName(MyDM dm,const char *name)
{
  PetscErrorCode ierr;
  if(!(dm->myname)){ierr=PetscMalloc(MYDM_MAX_NAMELEN*sizeof(char),&(dm->myname));CHKERRQ(ierr);}
  PetscStrcpy(dm->myname,name);
  return 0;
}

inline MyErrCode MyDMSetNameFromOptions(MyDM dm)
{
  PetscErrorCode ierr;
  PetscBool set;
  if(!(dm->myname)){ierr=PetscMalloc(MYDM_MAX_NAMELEN*sizeof(char),&(dm->myname));CHKERRQ(ierr);}
  PetscOptionsGetString(dm->prefix,"-name",dm->myname,MYDM_MAX_NAMELEN*sizeof(char),&set);
  if(!set) dm->myname[0]= 0;
  return 0;
}

MyErrCode MyDMSetPrefix(MyDM dm,const char *prefix)
{
  PetscErrorCode ierr;

  if(prefix){
    if(prefix[0]) {
      if (!(dm->prefix)) {ierr=PetscMalloc(MYDM_MAX_NAMELEN*sizeof(char),&(dm->prefix));CHKERRQ(ierr);}
      PetscStrcpy(dm->prefix,prefix);
    }
    else PetscFree(dm->prefix); /* set to PETSC_NULL */
  }
  else PetscFree(dm->prefix);

  return 0;
}

/* ---------------------------------------------------------------------------
   Set members from options.  If no options found, set default values.
   --------------------------------------------------------------------------- */
inline MyErrCode MyDMSetFromOptions(MyDM dm,MPI_Comm comm,const char *optsfile,const char *prefix)
{
  MyErrCode myerr;
  PetscErrorCode ierr;

  if(optsfile){
    if(optsfile[0]){ierr=PetscOptionsInsertFile(comm,optsfile,PETSC_TRUE);CHKERRQ(ierr);}
  }
  MyDMSetPrefix(dm,prefix);
  myerr= MyDMSetCommFromOptions(dm,comm);CHKMyErrQ(myerr);
  myerr= MyDMSetNumCellsFromOptions(dm);CHKMyErrQ(myerr);
  myerr= MyDMSetNumPMLFromOptions(dm);CHKMyErrQ(myerr);
  MyDMSetNameFromOptions(dm);

  return 0;
}

/* ---------------------------------------------------------------------------
   Compute the partition.
   --------------------------------------------------------------------------- */
static inline void mypart1d(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *nxp, PetscInt *startx,PetscInt *endx)
{				/* ix is the index of the part */
  if (ix<(nx % px)) {
    (*nxp)= nx/px+1; (*startx)= ix*(*nxp);  (*endx)= (*startx) + (*nxp);
  }
  else {
    (*nxp)= nx/px; (*startx)= (nx%px)+ix*(*nxp); (*endx)= (*startx)+(*nxp);
  }
}

static inline void mysearch1d(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *rx,PetscInt *startx,PetscInt *endx)
{
  /* ix is the index of the element, rx is the index of the part (processor),
   * computed according to mypart1d */
  if (ix<(nx % px)*(nx/px+1)) {
    (*rx)= ix/(nx/px+1); (*startx)= (*rx)*(nx/px+1); (*endx)= (*startx) + (nx/px+1);
  }
  else {
    (*rx)= nx%px + (ix-(nx%px)*(nx/px+1))/(nx/px); 
    (*startx)= (*rx)*(nx/px) + nx%px; (*endx)= (*startx) + nx/px;
  }
}

#undef __FUNCT__ 
#define __FUNCT__  "MyDMSetup"
MyErrCode MyDMSetup(MyDM dm)
{
  if(!(dm->comm)){return CommUndefined;}
  if(dm->nx_phy<=0 || dm->ny_phy<=0 || dm->nz_phy<=0){return WrongNumCells;}
  if(dm->eta_xl<0 || dm->eta_xr<0 || dm->eta_yl<0 || dm->eta_yr<0 || dm->eta_zl<0 || dm->eta_zr<0){return WrongNumPML;}
  if((dm->px)*(dm->py)*(dm->pz)-(dm->p) || dm->px<=0 || dm->py<=0 || dm->pz<=0){return NumProcWrongFactors;}
  if(!dm->part1d || !dm->search1d) return MyDMNeedsFunc;
  dm->nx_all= dm->nx_phy + dm->eta_xl + dm->eta_xr;
  dm->ny_all= dm->ny_phy + dm->eta_yl + dm->eta_yr;
  dm->nz_all= dm->nz_phy + dm->eta_zl + dm->eta_zr;
  if(dm->nx_all<dm->px || dm->ny_all<dm->py || dm->nz_all<dm->pz){return CellLessThanProc;}
  dm->n_phy= dm->nx_phy*(dm->ny_phy)*(dm->nz_phy);
#if 0
  {
    PetscInt iz=14, rz, izs, ize;
    dm->search1d(iz,dm->nz_all,dm->pz,&rz,&izs,&ize);
    printf("element %d is in proc %d who owns %d~%d elements\n",iz,rz,izs,ize);
  }
#endif
  dm->part1d(dm->rx,dm->nx_all,dm->px,&(dm->nxloc),&(dm->ixbegin),&(dm->ixend));
  dm->part1d(dm->ry,dm->ny_all,dm->py,&(dm->nyloc),&(dm->iybegin),&(dm->iyend));
  dm->part1d(dm->rz,dm->nz_all,dm->pz,&(dm->nzloc),&(dm->izbegin),&(dm->izend));
  dm->nloc= dm->nxloc*(dm->nyloc)*(dm->nzloc);
  dm->etaloc_xl= PetscMax(PetscMin(dm->eta_xl-dm->ixbegin,dm->nxloc),0);
  dm->etaloc_yl= PetscMax(PetscMin(dm->eta_yl-dm->iybegin,dm->nyloc),0);
  dm->etaloc_zl= PetscMax(PetscMin(dm->eta_zl-dm->izbegin,dm->nzloc),0);
  dm->etaloc_xr= PetscMax(PetscMin(dm->eta_xr-(dm->nx_all-dm->ixend),dm->nxloc),0);
  dm->etaloc_yr= PetscMax(PetscMin(dm->eta_yr-(dm->ny_all-dm->iyend),dm->nyloc),0);
  dm->etaloc_zr= PetscMax(PetscMin(dm->eta_zr-(dm->nz_all-dm->izend),dm->nzloc),0);  
  dm->initialized= PETSC_TRUE;
  return 0;
}

/* ---------------------------------------------------------------------------
   Viewer of a MyDM.
   --------------------------------------------------------------------------- */
/*
   Comm view: only 0 proc gets the complete info.
*/
#undef __FUNCT__
#define __FUNCT__ "MyCommView"
PetscErrorCode MyCommView(MPI_Comm comm, char **myinfo)
{
  PetscMPIInt rank, local_rank, temp, size, local_infolen=0, infolen=0, *recvcnts=0, *disp=0, i;
  char *local_info=0;
  int mpierr;

  PetscFunctionBegin;

  /* local info: rank */
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank); temp= rank;
  MPI_Comm_rank(comm,&local_rank);
  if(!rank){local_infolen=1;}else{while(rank){rank/=10;local_infolen++;}}
  local_infolen++; rank= temp;
  PetscMalloc(sizeof(char)*(local_infolen+1),&local_info);
  sprintf(local_info,"%d,",rank);

  /* global info: ranks */
  MPI_Reduce(&local_infolen, &infolen, 1, MPI_INT, MPI_SUM, 0, comm);  
  if(!local_rank) PetscMalloc(sizeof(char)*(infolen+1),myinfo);
  MPI_Comm_size(comm,&size);
  if(!local_rank) PetscMalloc(sizeof(PetscMPIInt)*size,&recvcnts);
  if(!local_rank) PetscMalloc(sizeof(PetscMPIInt)*size,&disp);
  mpierr= MPI_Gather(&local_infolen,1,MPI_INT,recvcnts,1,MPI_INT,0,comm);
  if(!local_rank) {disp[0]= 0; for(i=1;i<size;++i) disp[i]= disp[i-1]+recvcnts[i-1];}
  mpierr= MPI_Gatherv(local_info,local_infolen,MPI_CHAR,*myinfo,recvcnts,disp,MPI_CHAR,0,comm);
  if(!local_rank) {(*myinfo)[infolen-1]= '\0';}
  PetscFree(local_info); PetscFree(disp); PetscFree(recvcnts);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyDMViewOnScreen"
MyErrCode MyDMViewOnScreen(MyDM dm)
{
  char *myinfo=0;

  PetscFunctionBegin;
  MyCommView(dm->comm,&myinfo);
  PetscPrintf(dm->comm,"/ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n");
  PetscPrintf(dm->comm,"/       MyDM %s (bounds are describing the interval [ ))    \n",dm->myname);
  PetscPrintf(dm->comm,"/\n");
  PetscPrintf(dm->comm,"/  Processors in MPI_Comm: %s\n",myinfo);
  PetscPrintf(dm->comm,"/  Num processors: %d = %d*%d*%d\n",dm->p,dm->px,dm->py,dm->pz);
  PetscPrintf(dm->comm,"/  Num physical cells: (%d,%d,%d)\n",dm->nx_phy,dm->ny_phy,dm->nz_phy);
  PetscPrintf(dm->comm,"/  Num cells in PML in 6 directions: (%d,%d,%d,%d,%d,%d)\n",dm->eta_xl,dm->eta_xr,dm->eta_yl,dm->eta_yr,dm->eta_zl,dm->eta_zr);
  PetscPrintf(dm->comm,"/  Num all cells: (%d,%d,%d)\n",dm->nx_all,dm->ny_all,dm->nz_all);
  if(dm->has_coord){
    if(dm->nx_all>1)PetscPrintf(dm->comm,"/  Mesh coordinates:  x = %.2f, %.2f,.., %.2f\n",PetscRealPart(dm->x[0]),PetscRealPart(dm->x[1]),PetscRealPart(dm->x[dm->nx_all]));
    else PetscPrintf(dm->comm,"/  Mesh coordinates:  x = %.2f,.., %.2f\n",PetscRealPart(dm->x[0]),PetscRealPart(dm->x[1]));
    if(dm->ny_all>1)PetscPrintf(dm->comm,"/                     y = %.2f, %.2f,.., %.2f\n",PetscRealPart(dm->y[0]),PetscRealPart(dm->y[1]),PetscRealPart(dm->y[dm->ny_all]));
    else PetscPrintf(dm->comm,"/                     y = %.2f, %.2f,.., %.2f\n",PetscRealPart(dm->y[0]),PetscRealPart(dm->y[1]));
    if(dm->nz_all>1)PetscPrintf(dm->comm,"/                     z = %.2f, %.2f,.., %.2f\n",PetscRealPart(dm->z[0]),PetscRealPart(dm->z[1]),PetscRealPart(dm->z[dm->nz_all]));
    else PetscPrintf(dm->comm,"/                     z = %.2f, %.2f,.., %.2f\n",PetscRealPart(dm->z[0]),PetscRealPart(dm->z[1]));
  }
  PetscSynchronizedPrintf(dm->comm,"/  Processor %d: \n",dm->rank);
  PetscSynchronizedPrintf(dm->comm,"/      3D subscripts (%d,%d,%d)\n",dm->rx,dm->ry,dm->rz);
  PetscSynchronizedPrintf(dm->comm,"/      Num local cells: (%d,%d,%d)\n",dm->nxloc,dm->nyloc,dm->nzloc);
  PetscSynchronizedPrintf(dm->comm,"/      Bounds of local cells: (%d,%d,%d,%d,%d,%d)\n",dm->ixbegin,dm->ixend,dm->iybegin,dm->iyend,dm->izbegin,dm->izend);
  PetscSynchronizedPrintf(dm->comm,"/      Num locl pml on six directions: (%d,%d,%d,%d,%d,%d)\n",dm->etaloc_xl,dm->etaloc_xr,dm->etaloc_yl,dm->etaloc_yr,dm->etaloc_zl,dm->etaloc_zr);
  PetscSynchronizedFlush(dm->comm,PETSC_STDOUT);
  PetscPrintf(dm->comm,"/ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
  PetscFree(myinfo);
  PetscFunctionReturn(0);
}
