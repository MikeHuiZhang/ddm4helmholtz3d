# Domain Decomposition Methods for Helmholtz Problems in 3-D #

We solve the Helmholtz problem on a 3-D rectangle filled with heterogeneous media by domain decomposition methods. We implement FETI-DPH and optimised Schwarz methods in substructured form. For each space dimension, we can assign either multiple workers per subdomain or one worker for multiple subdomains. When one worker participates (there might be co-workers) multiple subdomain solves, we solve the multiple subdomains sequentially e.g. in symmetric Gauss-Seidel order. In particular, if we have 1 X py X pz processors in x,y,z and we do symmetric Gauss-Seidel in x, then we get the double sweep alternating OSM (see [Nataf93](http://www.cmap.polytechnique.fr/RI/R.I.284.html)). OSM has been accelerated by using PML transmission (see e.g. [Schadle, et al](http://www.zib.de/Publications/Reports/ZR-06-04.pdf) and more recently [Enguist-Ying](http://math.stanford.edu/~lexing/publication/sweeppml.pdf)).  Here, we implement also PML transmission.  Note that the smallest things take the most efforts for implementation! For examples, the edges, the vertices and the coarse problem.  The alternating OSM can avoids all these complications, but parallelism *between* subdomains is lost and must be found *inside* each subdomain.  This issue was addressed with a tailored direct solver by [Poulson et al](http://epubs.siam.org/doi/abs/10.1137/120871985). 

### This is a research program ###

* for comparing various DDMs for Helmholtz problems
* in particular, comparing alternating and parallel OSM


### Current Status ###

* Only direct solvers through PETSC interfaces
* No DDM done

### Dependencies ###

* MPI
* PETSC configured with downloading parallel direct solvers (MUMPS, SuperLU_Dist, Pastix)