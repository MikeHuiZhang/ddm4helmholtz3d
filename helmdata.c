#ifndef _HELMDATA
#define _HELMDATA

/*
   Copy data into PML.
*/
#undef __FUNCT__
#define __FUNCT__ "CopyCellDataInPML"
static MyErrCode CopyCellDataInPML(PetscScalar *w,MyDM dm)
{
  PetscInt ix,iy,iz,ixphy,iyphy,izphy,count=0;

  PetscFunctionBegin;
  if(!(dm->initialized)) return MyDMNotInitialized;
  for(ix=dm->ixbegin;ix<dm->ixend;ix++){
    ixphy= PetscMin(PetscMax(ix,dm->eta_xl),dm->nx_all-1-dm->eta_xr);
    for(iy=dm->iybegin;iy<dm->iyend;iy++){
      iyphy= PetscMin(PetscMax(iy,dm->eta_yl),dm->ny_all-1-dm->eta_yr);
      for(iz=dm->izbegin;iz<dm->izend;iz++){
	izphy= PetscMin(PetscMax(iz,dm->eta_zl),dm->nz_all-1-dm->eta_zr);
	if(izphy==iz && iyphy==iy && ixphy==ix) {count++; continue;}
	else {
	  w[count]= w[(ixphy-dm->ixbegin)*(dm->nyloc)*(dm->nzloc)+(iyphy-dm->iybegin)*(dm->nzloc)+izphy-dm->izbegin];
	  count++;
	}
      }
    }
  }
  PetscFunctionReturn(0);
}


/*
   Initialize cell data for HelmBVP on a MyDM
*/
#undef __FUNCT__
#define __FUNCT__ "HelmBVPInitCellData"
MyErrCode HelmBVPInitCellData(HelmBVP bvp,MyDM dm)
{
  PetscErrorCode myerr;
  PetscFunctionBegin;

  /* Read in wavespeed and compute wavenumber including PML (values without
     complex streching 'xi').  */
  if(bvp->wavespeedfilename) {
    if(bvp->wavespeedfilename[0]){
      bvp->w= PETSC_NULL;
      myerr= AverageCellRealToMeshScalar(&(bvp->w),dm,bvp->avm_w,bvp->avmctx_w,
	     bvp->wavespeedfilename,bvp->wdomain[0],bvp->wdomain[1],
             bvp->wdomain[2],bvp->wdomain[3],bvp->wdomain[4],bvp->wdomain[5],
             bvp->siz_wavespeed[0],bvp->siz_wavespeed[1],bvp->siz_wavespeed[2]);
      CHKMyErrQ(myerr);
      /* printf("w[0]=%f %+f i\n",PetscRealPart(bvp->w[0]),PetscImaginaryPart(bvp->w[0])); */
      myerr= CopyCellDataInPML(bvp->w,dm); CHKMyErrQ(myerr);
    }
    else{		     /* filename is of length 0 then use constant one */
      SetCellDataConstant(&(bvp->w),dm->nloc,1.0);
    }
  }
  else{				/* no file then use constant one */
    SetCellDataConstant(&(bvp->w),dm->nloc,1.0);
  }
  if(bvp->AttenuateWavespeed){
    AttenCtx atten_ctx= {dm, bvp->freq};
    bvp->AttenuateWavespeed(bvp->w,&atten_ctx);
  }
  if(bvp->ComputeWavenumber){
    c2wCtx c2w_ctx= {dm, bvp->freq};
    myerr= bvp->ComputeWavenumber(bvp->w,&c2w_ctx); CHKMyErrQ(myerr);
  }
  else return(HelmBVPNullFun);

  /* Read in density */
  if(bvp->densityfilename) {
    if(bvp->densityfilename[0]) {
      myerr= AverageCellRealToMeshScalar(&(bvp->rho),dm,bvp->avm_rho,
             bvp->avmctx_rho,bvp->densityfilename,bvp->rhodomain[0],
             bvp->rhodomain[1],bvp->rhodomain[2],bvp->rhodomain[3],
             bvp->rhodomain[4],bvp->rhodomain[5],bvp->siz_density[0],
             bvp->siz_density[1],bvp->siz_density[2]); CHKMyErrQ(myerr);
      myerr= CopyCellDataInPML(bvp->rho,dm); CHKMyErrQ(myerr);
    }
    else {
      SetCellDataConstant(&(bvp->rho),dm->nloc,1.0);
    }
  }
  else {
    SetCellDataConstant(&(bvp->rho),dm->nloc,1.0);
  }

  PetscFunctionReturn(0);
}

#endif
