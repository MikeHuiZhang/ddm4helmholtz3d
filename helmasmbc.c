#ifndef _HELMASMBC
#define _HELMASMBC


/*
   Macro for one dimensional information: d = x,y,z
*/
#define GetDimMode(dim,dmode,ondb,bctype,dofds,dofde,ds,de,hasdpml)	\
  switch (dmode) {							\
  case 0:								\
    switch (dim) {							\
    case 0:								\
      hasdpml= (dm->eta_xl>0);						\
      ondb= (bvp->bctype_xl==bctype && dm->ixbegin==0); break;		\
    case 1:								\
      hasdpml= (dm->eta_yl>0);						\
      ondb= (bvp->bctype_yl==bctype && dm->iybegin==0); break;		\
    case 2: default:							\
      hasdpml= (dm->eta_zl>0);						\
      ondb= (bvp->bctype_zl==bctype && dm->izbegin==0); break;		\
    }									\
    dofds= 0; dofde= 1; ds= 0; de= 1; break;				\
  case 1:								\
    switch (dim) {							\
    case 0:								\
      hasdpml= (dm->eta_xr>0);						\
      ondb= (bvp->bctype_xr==bctype && dm->ixend==dm->nx_all);		\
      dofds= dof->ndof_xloc-1; dofde= dof->ndof_xloc;			\
      ds= dm->nxloc-1; de= dm->nxloc; break;				\
    case 1:								\
      hasdpml= (dm->eta_yr>0);						\
      ondb= (bvp->bctype_yr==bctype && dm->iyend==dm->ny_all);		\
      dofds= dof->ndof_yloc-1; dofde= dof->ndof_yloc;			\
      ds= dm->nyloc-1; de= dm->nyloc; break;				\
    case 2: default:							\
      hasdpml= (dm->eta_zr>0);						\
      ondb= (bvp->bctype_zr==bctype && dm->izend==dm->nz_all);		\
      dofds= dof->ndof_zloc-1; dofde= dof->ndof_zloc;			\
      ds= dm->nzloc-1; de= dm->nzloc; break;				\
    }									\
    break;								\
  case 2: default:							\
    switch (dim) {							\
    case 0:								\
      hasdpml= (dm->eta_xl>0 || dm->eta_xr>0);				\
      dofds= 0; dofde= dof->ndof_xloc;					\
      ds= 0; de= dm->nxloc; break;					\
    case 1:								\
      hasdpml= (dm->eta_yl>0 || dm->eta_yr>0);				\
      dofds= 0; dofde= dof->ndof_yloc;					\
      ds= 0; de= dm->nyloc; break;					\
    case 2: default:							\
      hasdpml= (dm->eta_zl>0 || dm->eta_zr>0);				\
      dofds= 0; dofde= dof->ndof_zloc;					\
      ds= 0; de= dm->nzloc; break;					\
    }									\
    ondb= 0; break;							\
  }


/* ---------------------------------------------------------------------------
   Assembly of Robin face conditions for HelmBVP with MyDOF.
   --------------------------------------------------------------------------- */

/*
   Face stiffness and mass matrices on a 2D rectilinear Q1-element.

   1---3
   |   |
   0---2
*/
#undef __FUNCT__
#define __FUNCT__ "FaceGetMatQ1"
MyErrCode FaceGetMatQ1(PetscScalar **M,PetscScalar **K,PetscInt **rows,PetscInt *ndof,PetscScalar h[])
{
  PetscErrorCode ierr;
  PetscInt i,j,ix,iy,iz,jx,jy,jz;
  PetscScalar h01,h10;
  const PetscInt ndofxQ1=2, ndofyQ1= 2, ndofzQ1= 1;
  const PetscInt ndofQ1= 4;

  PetscFunctionBegin;
  if(ndof) ndof[0]= ndofQ1;
  if(!h) return EmptyArray;

  /* cell dofs */
  if(rows) {
    if(!(*rows)) {ierr=PetscMalloc(ndofQ1*sizeof(PetscInt),rows);CHKERRQ(ierr);}
    for(i=0;i<ndofQ1;i++) (*rows)[i]= i;
  }

  /* cell mass: in 1D cell it is (h/3, h/6; h/6, h/3) */
  if(M) {
    if(!(*M)) {ierr=PetscMalloc(ndofQ1*(ndofQ1)*sizeof(PetscScalar),M);CHKERRQ(ierr);}

    (*M)[0]= 1.0/9.0*h[0]*h[1]; /* M(0,0) */
    (*M)[1]= (*M)[0]*0.5;	/* M(1,0) */
    (*M)[2]= (*M)[1];		/* M(2,0) */
    (*M)[3]= (*M)[1]*0.5;
    for(j=1;j<ndofQ1;j++){	/* M(:,j) */
      MyArrayind2sub(ndofxQ1, ndofyQ1, ndofzQ1, &jx, &jy, &jz, j);
      for(i=0;i<ndofQ1;i++){
	MyArrayind2sub(ndofxQ1, ndofyQ1, ndofzQ1, &ix, &iy, &iz, i);
	(*M)[j*ndofQ1+i]= (*M)[0]*((ix-jx)?.5:1.)*((iy-jy)?.5:1.);
      }
    }
  }

  /* cell stiffness: in 1D cell it is (1.0/h, -1.0/h; -1.0/h, 1.0/h), in 2D it
     should be a sum of stiffness in one direction tensored with masses in other
     direction */
  if(K) {
    if(!(*K)) {ierr=PetscMalloc(ndofQ1*(ndofQ1)*sizeof(PetscScalar),K);CHKERRQ(ierr);}
    h01= h[0]/h[1]; h10= h[1]/h[0];
    (*K)[0]= (h01+h10)/3.0;		 /* K(0,0) */
    (*K)[1]= (-h01+h10*.5)/3.0;		 /* K(1,0) */
    (*K)[2]= (-h10+h01*.5)/3.0;		 /* K(2,0) */
    (*K)[3]= -(h01+h10)/6.0;		 /* K(7,0) */
    for(j=1;j<ndofQ1;j++){		 /* K(:,j) */
      MyArrayind2sub(ndofxQ1, ndofyQ1, ndofzQ1, &jx, &jy, &jz, j);
      for(i=0;i<ndofQ1;i++){
	MyArrayind2sub(ndofxQ1, ndofyQ1, ndofzQ1, &ix, &iy, &iz, i);
	(*K)[j*ndofQ1+i]=(h01*((iy-jy)?-1:1)*((ix-jx)?.5:1)+
			  h10*((ix-jx)?-1:1)*((iy-jy)?.5:1))/3.0;
      }
    }
  }

  PetscFunctionReturn(0);
}


/*
   Assembly for Robin-like boundary conditions on one side of the domain, using
   Q1 element nodal basis.

   submat -- matrix on this side, supports MatSetValuesLocal in natural
   ordering, first z then y

   ny, nz -- number of mesh elements in 1D

   y, z -- 1D coordinates on this side

   robin_pf -- ArrayCtxFun that computes robin parameters on one face element

   ctx -- context on this side: w, rho arrays in natural ordering on this side,
   sign convention, and other information
*/
#undef __FUNCT__
#define __FUNCT__  "RobinFaceAssembly"
MyErrCode RobinFaceAssembly(MyDOF dof, Mat submat,PetscInt ny,PetscInt nz,PetscScalar y[],PetscScalar z[],ArrayCtxFun robin_pf, PetscInt robin_order, RobinCtx *ctx,MyErrCode (*robin_createcellctx)(void**),MyErrCode (*robin_destroycellctx)(void**),MyErrCode (*robin_getcellctx)(void*,RobinCtx*,PetscInt))
{
  PetscInt iy, iz, i, ndof, *celldofs=0, *idofs=0, idof0;
  PetscScalar h[2], robinp[2], *Mi=0, *Ai=0;
  MyErrCode myerr;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  h[0]= 1.; h[1]= 1.;
  switch (dof->type) {
  case MyDOFQ1: default:
    FaceGetMatQ1(&Mi,PETSC_NULL,&celldofs,&ndof,h);
    break;
  }
  ierr= PetscMalloc(ndof*ndof*sizeof(PetscScalar),&Ai);CHKERRQ(ierr);
  ierr= PetscMalloc(ndof*sizeof(PetscInt),&idofs); CHKERRQ(ierr);

  if (robin_pf==(ArrayCtxFun)robin_pf_T0) {
    TaylorCtx taylorctx;  taylorctx.sign= ctx->sign;
    for (iy = 0; iy < ny; ++iy) {
      h[0]= y[iy+1]-y[iy];
      for (iz = 0; iz < nz; ++iz) {
	h[1]= z[iz+1] - z[iz];
	myerr= MyArraysub2ind(1,ny,nz,0,iy,iz,&i); CHKMyErrQ(myerr);
	taylorctx.wi= ctx->w[i]; taylorctx.rho= ctx->rho[i];
	robin_pf(robinp,&taylorctx);
	for (i = 0; i < ndof*ndof; ++i) Ai[i]= Mi[i]*h[0]*h[1]*robinp[0];
	idof0= dof->facedof0(iy,iz,ny,nz);
	for(i=0;i<ndof;++i) {
	  idofs[i]= dof->facedofi(celldofs[i],idof0,ny+1,nz+1);
	  if (idofs[i]<0) return IllegalIndex;
	}
	MatSetValuesLocal(submat,ndof,idofs,ndof,idofs,Ai,ADD_VALUES);
      }
    }
  }
  else if (robin_pf==(ArrayCtxFun)robin_pf_T2) {
    TaylorCtx taylorctx;  taylorctx.sign= ctx->sign;
    for (iy = 0; iy < ny; ++iy) {
      h[0]= y[iy+1]-y[iy];
      for (iz = 0; iz < nz; ++iz) {
	h[1]= z[iz+1] - z[iz];
	myerr= MyArraysub2ind(1,ny,nz,0,iy,iz,&i); CHKMyErrQ(myerr);
	taylorctx.wi= ctx->w[i]; taylorctx.rho= ctx->rho[i];
	robin_pf(robinp,&taylorctx);
	switch (dof->type) {
	case MyDOFQ1: default:
	  FaceGetMatQ1(PETSC_NULL,&Ai,&idofs,&ndof,h);
	  break;
	}
	for (i = 0; i < ndof*ndof; ++i) Ai[i]= Ai[i]*robinp[1]+Mi[i]*h[0]*h[1]*robinp[0];
	idof0= dof->facedof0(iy,iz,ny,nz);
	for(i=0;i<ndof;++i) {
	  idofs[i]= dof->facedofi(idofs[i],idof0,ny+1,nz+1);
	  if (idofs[i]<0) return IllegalIndex;
	}
	MatSetValuesLocal(submat,ndof,idofs,ndof,idofs,Ai,ADD_VALUES);
      }
    }
  }
  else if (robin_createcellctx && robin_getcellctx && robin_order==2) {
    void *cellctx; robin_createcellctx(&cellctx);
    for (iy = 0; iy < ny; ++iy) {
      h[0]= y[iy+1]-y[iy];
      for (iz = 0; iz < nz; ++iz) {
	h[1]= z[iz+1] - z[iz];
	myerr= MyArraysub2ind(1,ny,nz,0,iy,iz,&i); CHKMyErrQ(myerr);
	robin_getcellctx(cellctx,ctx,i);
	robin_pf(robinp,&cellctx);
	switch (dof->type) {
	case MyDOFQ1: default:
	  FaceGetMatQ1(PETSC_NULL,&Ai,&idofs,&ndof,h);
	  break;
	}
	for (i = 0; i < ndof*ndof; ++i) Ai[i]= Ai[i]*robinp[1]+Mi[i]*h[0]*h[1]*robinp[0];
	idof0= dof->facedof0(iy,iz,ny,nz);
	for(i=0;i<ndof;++i) {
	  idofs[i]= dof->facedofi(idofs[i],idof0,ny+1,nz+1);
	  if (idofs[i]<0) return IllegalIndex;
	}
	MatSetValuesLocal(submat,ndof,idofs,ndof,idofs,Ai,ADD_VALUES);
      }
    }
    if (robin_destroycellctx) robin_destroycellctx(&cellctx);
    else PetscFree(cellctx);
  }
  else if (robin_createcellctx && robin_getcellctx && robin_order==0) {
    void *cellctx; robin_createcellctx(&cellctx);
    for (iy = 0; iy < ny; ++iy) {
      h[0]= y[iy+1]-y[iy];
      for (iz = 0; iz < nz; ++iz) {
	h[1]= z[iz+1] - z[iz];
	myerr= MyArraysub2ind(1,ny,nz,0,iy,iz,&i); CHKMyErrQ(myerr);
	robin_getcellctx(cellctx,ctx,i);
	robin_pf(robinp,&cellctx);
	for (i = 0; i < ndof*ndof; ++i) Ai[i]= Mi[i]*h[0]*h[1]*robinp[0];
	idof0= dof->facedof0(iy,iz,ny,nz);
	for(i=0;i<ndof;++i) {
	  idofs[i]= dof->facedofi(idofs[i],idof0,ny+1,nz+1);
	  if (idofs[i]<0) return IllegalIndex;
	}
	MatSetValuesLocal(submat,ndof,idofs,ndof,idofs,Ai,ADD_VALUES);
      }
    }
    if (robin_destroycellctx) robin_destroycellctx(&cellctx);
    else PetscFree(cellctx);
  }
  else
    return UnknownCtxFun;

  PetscFree(celldofs); PetscFree(idofs); PetscFree(Mi); PetscFree(Ai);
  PetscFunctionReturn(0);
}


/*
   Assembly of Robin b.c. for a HelmBVP.

   TODO: If on exterior boundary of PML, anisotropy is introduced by xi, I am not
   sure what Robin parameters should be used.  Since xi varies in the domain, the
   implementation will involve numerical integration.
*/

#define SetRobinFace(xmode,ymode,zmode) {				\
    PetscInt onxb,dofxs,dofxe,xs,xe,hasxpml;				\
    PetscInt onyb,dofys,dofye,ys,ye,hasypml;				\
    PetscInt onzb,dofzs,dofze,zs,ze,haszpml;				\
    GetDimMode(0,xmode,onxb,Robin,dofxs,dofxe,xs,xe,hasxpml);		\
    GetDimMode(1,ymode,onyb,Robin,dofys,dofye,ys,ye,hasypml);		\
    GetDimMode(2,zmode,onzb,Robin,dofzs,dofze,zs,ze,haszpml);		\
    if (onxb+onyb+onzb==1) {						\
      if (hasxpml || hasypml || haszpml)				\
	return RobinOnPMLNotImplemented;				\
      myerr= MyArraySubIS(&subis,PETSC_COMM_SELF,dof->ndof_xloc,	\
			  dof->ndof_yloc,dof->ndof_zloc,dofxs,dofxe,	\
			  dofys,dofye,dofzs,dofze); CHKMyErrQ(myerr);	\
      ierr= MatGetLocalSubMatrix(A,subis,subis,&submat); CHKERRQ(ierr);	\
      myerr= MyArraySubArray(&(ctx->w),bvp->w,dm->nxloc,dm->nyloc,	\
			     dm->nzloc,xs,xe,ys,ye,zs,ze);		\
      CHKMyErrQ(myerr);							\
      myerr= MyArraySubArray(&(ctx->rho),bvp->rho,dm->nxloc,dm->nyloc,	\
			     dm->nzloc,xs,xe,ys,ye,zs,ze);		\
      CHKMyErrQ(myerr);							\
      if (bvp->robingetapp)						\
	bvp->robingetapp(bvp,ctx,xmode,ymode,zmode);			\
      if (onxb)								\
      {myerr= RobinFaceAssembly(dof,submat,dm->nyloc,dm->nzloc,		\
				dm->y+dm->iybegin,dm->z+dm->izbegin,	\
				bvp->robin_pf,bvp->robin_order,ctx,	\
				bvp->robin_createcellctx,		\
				bvp->robin_destroycellctx,		\
				bvp->robin_getcellctx);			\
	CHKMyErrQ(myerr);}						\
      if (onyb)								\
      {myerr= RobinFaceAssembly(dof,submat,dm->nxloc,dm->nzloc,		\
				dm->x+dm->ixbegin,dm->z+dm->izbegin,	\
				bvp->robin_pf,bvp->robin_order,ctx,	\
				bvp->robin_createcellctx,		\
				bvp->robin_destroycellctx,		\
				bvp->robin_getcellctx);			\
	CHKMyErrQ(myerr);}						\
      if (onzb)								\
      {myerr= RobinFaceAssembly(dof,submat,dm->nxloc,dm->nyloc,		\
				dm->x+dm->ixbegin,dm->y+dm->iybegin,	\
				bvp->robin_pf,bvp->robin_order,ctx,	\
				bvp->robin_createcellctx,		\
				bvp->robin_destroycellctx,		\
				bvp->robin_getcellctx);			\
	CHKMyErrQ(myerr);}						\
      PetscFree(ctx->w); PetscFree(ctx->rho);				\
      ierr= MatRestoreLocalSubMatrix(A,subis,subis,&submat);		\
      CHKERRQ(ierr);							\
      ierr= ISDestroy(&subis); CHKERRQ(ierr);				\
    }									\
}


#undef __FUNCT__
#define __FUNCT__  "HelmBVPRobinFacesAssembly"
MyErrCode HelmBVPRobinFacesAssembly(HelmBVP bvp,MyDOF dof,Mat A)
{
  PetscErrorCode ierr;
  MyErrCode myerr;
  Mat submat;			/* submatrix on one side */
  IS subis;			/* to get the submat */
  RobinCtx *ctx=0;
  MyDM dm=0;
  PetscFunctionBegin;
  ierr= PetscMalloc(sizeof(RobinCtx),&ctx); CHKERRQ(ierr);
  ierr= PetscMemzero(ctx,sizeof(RobinCtx)); CHKERRQ(ierr);
  if (bvp->robincreateapp) bvp->robincreateapp(&(ctx->appendix));
  ctx->sign= bvp->sign;
  dm= dof->dm;
  SetRobinFace(0,2,2);
  SetRobinFace(1,2,2);
  SetRobinFace(2,0,2);
  SetRobinFace(2,1,2);
  SetRobinFace(2,2,0);
  SetRobinFace(2,2,1);
  if (ctx->appendix) {
    if (bvp->robindestroyapp) bvp->robindestroyapp(&(ctx->appendix));
    else PetscFree(ctx->appendix);
  }
  PetscFree(ctx);
  PetscFunctionReturn(0);
}

/* ---------------------------------------------------------------------------
   Assembly of Robin edge conditions.
   --------------------------------------------------------------------------- */

/*
   Mass and stiffness matrices on a 1D element
*/
#undef __FUNCT__
#define __FUNCT__ "EdgeGetMatQ1"
MyErrCode EdgeGetMatQ1(PetscScalar **Ms,PetscScalar **Ks,PetscInt **idofs,PetscInt *ndof,PetscScalar h[])
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if(ndof) *ndof= 2;
  if(idofs) {
    if(*idofs==PETSC_NULL){ierr= PetscMalloc((*ndof)*sizeof(PetscInt),idofs); CHKERRQ(ierr);}
    (*idofs)[0]= 0; (*idofs)[1]= 1;
  }
  if(Ms) {
    if(*Ms==PETSC_NULL){ierr= PetscMalloc((*ndof)*(*ndof)*sizeof(PetscScalar),Ms); CHKERRQ(ierr);}
    (*Ms)[0]= h[0]/3.0; (*Ms)[1]= h[0]/6.0; (*Ms)[2]= h[0]/6.0; (*Ms)[3]= h[0]/3.0;
  }
  if(Ks) {
    if(*Ks==PETSC_NULL){ierr= PetscMalloc((*ndof)*(*ndof)*sizeof(PetscScalar),Ks); CHKERRQ(ierr);}
    (*Ks)[0]= 1.0/h[0]; (*Ks)[1]= -1.0/h[0]; (*Ks)[2]= -1.0/h[0]; (*Ks)[3]= 1.0/h[0];
  }
  PetscFunctionReturn(0);
}

/*
   Set values of the matrix corresponding to one edge of the domain.
*/
#undef __FUNCT__
#define __FUNCT__  "RobinEdgeAssembly"
MyErrCode RobinEdgeAssembly(MyDOF dof,Mat submat,PetscInt nx,PetscScalar x[],ArrayCtxFun robin_pe,RobinCtx *ctx)
{
  PetscInt i,j,*celldofs=0,*idofs=0, ndof, idof0;
  PetscScalar *Ai=0, *Mi=0, h, robinp[2];
  PetscErrorCode ierr;

  PetscFunctionBegin;
  h= 1.0; 
  switch (dof->type) {
  case MyDOFQ1: default:
    EdgeGetMatQ1(&Mi,PETSC_NULL,&celldofs,&ndof,&h); break;
  }
  ierr= PetscMalloc(ndof*ndof*sizeof(PetscScalar),&Ai); CHKERRQ(ierr);
  ierr= PetscMalloc(ndof*sizeof(PetscInt),&idofs); CHKERRQ(ierr);
  if (robin_pe==(ArrayCtxFun)robin_pe_T2) {
    TaylorCtx taylorctx; taylorctx.sign= ctx->sign;
    for (i = 0; i < nx; ++i) {
      taylorctx.wi= ctx->w[i]; taylorctx.rho= ctx->rho[i];
      robin_pe(robinp,&taylorctx);
      h= x[i+1]-x[i];
      switch (dof->type) {
      case MyDOFQ1: default:
	EdgeGetMatQ1(PETSC_NULL,&Ai,PETSC_NULL,PETSC_NULL,&h); break;
      }
      for (j = 0; j < ndof*ndof; ++j) Ai[j]= Ai[j]*robinp[1] + Mi[j]*h*robinp[0];
      idof0= dof->edgedof0(i,nx);
      for (j = 0; j < ndof; ++j)
	idofs[j]= dof->edgedofi(celldofs[j],idof0,nx+1); /* assume Q1 */
      MatSetValuesLocal(submat,ndof,idofs,ndof,idofs,Ai,ADD_VALUES);
    }
  }
  else return UnknownCtxFun;

  PetscFree(Mi); PetscFree(Ai); PetscFree(idofs); PetscFree(celldofs);
  PetscFunctionReturn(0);
}

/*
   Assembly of Robin conditions on common edges of Robin faces.

*/

#define SetRobinEdge(xmode,ymode,zmode)	{				\
    PetscInt onxb,dofxs,dofxe,xs,xe,hasxpml;				\
    PetscInt onyb,dofys,dofye,ys,ye,hasypml;				\
    PetscInt onzb,dofzs,dofze,zs,ze,haszpml;				\
    GetDimMode(0,xmode,onxb,Robin,dofxs,dofxe,xs,xe,hasxpml);		\
    GetDimMode(1,ymode,onyb,Robin,dofys,dofye,ys,ye,hasypml);		\
    GetDimMode(2,zmode,onzb,Robin,dofzs,dofze,zs,ze,haszpml);		\
    if (onxb+onyb+onzb==2) {						\
      if (hasxpml || hasypml || haszpml)				\
	return RobinOnPMLNotImplemented;				\
      myerr= MyArraySubIS(&subis,PETSC_COMM_SELF,dof->ndof_xloc,	\
			  dof->ndof_yloc,dof->ndof_zloc,dofxs,dofxe,	\
			  dofys,dofye,dofzs,dofze);			\
      CHKMyErrQ(myerr);							\
      ierr= MatGetLocalSubMatrix(A,subis,subis,&submat); CHKERRQ(ierr);	\
      myerr= MyArraySubArray(&(ctx->w),bvp->w,dm->nxloc,dm->nyloc,	\
			     dm->nzloc,xs,xe,ys,ye,zs,ze);		\
      CHKMyErrQ(myerr);							\
      myerr= MyArraySubArray(&(ctx->rho),bvp->rho,dm->nxloc,dm->nyloc,	\
			     dm->nzloc,xs,xe,ys,ye,zs,ze);		\
      CHKMyErrQ(myerr);							\
      if (zmode>1)							\
      {myerr= RobinEdgeAssembly(dof,submat,dm->nzloc,dm->z+dm->izbegin,	\
				bvp->robin_pe,ctx); CHKMyErrQ(myerr);}	\
      if (ymode>1)							\
      {myerr= RobinEdgeAssembly(dof,submat,dm->nyloc,dm->y+dm->iybegin,	\
				bvp->robin_pe,ctx); CHKMyErrQ(myerr);}	\
      if (xmode>1)							\
      {myerr= RobinEdgeAssembly(dof,submat,dm->nxloc,dm->x+dm->ixbegin,	\
				bvp->robin_pe,ctx); CHKMyErrQ(myerr);}	\
      PetscFree(ctx->w); PetscFree(ctx->rho);				\
      ierr= MatRestoreLocalSubMatrix(A,subis,subis,&submat);		\
      CHKERRQ(ierr);							\
      ierr= ISDestroy(&subis); CHKERRQ(ierr);				\
    }									\
  }

/*
   Assembly of edges for Robin conditions.

   TODO: supports also optimized Robin.
*/
#undef __FUNCT__
#define __FUNCT__  "HelmBVPRobinEdgesAssembly"
MyErrCode HelmBVPRobinEdgesAssembly(HelmBVP bvp,MyDOF dof,Mat A)
{
  MyDM dm=0;
  MyErrCode myerr;
  PetscErrorCode ierr;
  IS subis;
  Mat submat;
  RobinCtx *ctx=0;

  PetscFunctionBegin;
  if (bvp->robin_order==0 || bvp->robin_pe!=(ArrayCtxFun)robin_pe_T2)
    PetscFunctionReturn(0);
  dm= dof->dm;
  ierr= PetscMalloc(sizeof(RobinCtx),&ctx); CHKERRQ(ierr);
  ctx->sign= bvp->sign;

  SetRobinEdge(0,0,2);
  SetRobinEdge(0,1,2);
  SetRobinEdge(1,0,2);
  SetRobinEdge(1,1,2);

  SetRobinEdge(0,2,0);
  SetRobinEdge(0,2,1);
  SetRobinEdge(1,2,0);
  SetRobinEdge(1,2,1);

  SetRobinEdge(2,0,0);
  SetRobinEdge(2,0,1);
  SetRobinEdge(2,1,0);
  SetRobinEdge(2,1,1);

  PetscFree(ctx);
  PetscFunctionReturn(0);
}

/* ---------------------------------------------------------------------------
   Assembly of Robin vertex conditions.
   --------------------------------------------------------------------------- */

#define SetRobinVertex(xmode,ymode,zmode) {				\
  PetscInt onxb,dofxs,dofxe,xs,xe,hasxpml;				\
  PetscInt onyb,dofys,dofye,ys,ye,hasypml;				\
  PetscInt onzb,dofzs,dofze,zs,ze,haszpml;				\
  GetDimMode(0,xmode,onxb,Robin,dofxs,dofxe,xs,xe,hasxpml);		\
  GetDimMode(1,ymode,onyb,Robin,dofys,dofye,ys,ye,hasypml);		\
  GetDimMode(2,zmode,onzb,Robin,dofzs,dofze,zs,ze,haszpml);		\
  if (onxb && onyb && onzb) {						\
    if (hasxpml || hasypml || haszpml) return RobinOnPMLNotImplemented;	\
    if (bvp->robin_pv==(ArrayCtxFun)robin_pv_T2) {			\
      TaylorCtx ctx; ctx.sign= bvp->sign;				\
      myerr= MyArraysub2ind(dm->nxloc,dm->nyloc,dm->nzloc,xs,ys,zs,&i);	\
      CHKMyErrQ(myerr);							\
      ctx.wi= bvp->w[i]; ctx.rho= bvp->rho[i];                          \
      bvp->robin_pv(&robinp,&ctx);					\
      myerr= MyArraysub2ind(dof->ndof_xloc,dof->ndof_yloc,		\
			    dof->ndof_zloc,dofxs,dofys,dofzs,&i);	\
      CHKMyErrQ(myerr);	              					\
      MatSetValuesLocal(A,1,&i,1,&i,&robinp,ADD_VALUES);		\
    }									\
    else return UnknownCtxFun;						\
  }									\
}


/*
   Assembly of common vertices of Robin faces.

   TODO: supports also optimized Robin parameters.
*/
#undef __FUNCT__
#define __FUNCT__  "HelmBVPRobinVerticesAssembly"
MyErrCode HelmBVPRobinVerticesAssembly(HelmBVP bvp, MyDOF dof, Mat A)
{
  MyDM dm= 0;
  MyErrCode myerr;
  PetscScalar robinp;
  PetscInt i;

  PetscFunctionBegin;
  if (bvp->robin_order==0 || bvp->robin_pv!=(ArrayCtxFun)robin_pv_T2)
    PetscFunctionReturn(0);
  dm= dof->dm;

  SetRobinVertex(0,0,0);
  SetRobinVertex(0,0,1);
  SetRobinVertex(0,1,0);
  SetRobinVertex(0,1,1);
  SetRobinVertex(1,0,0);
  SetRobinVertex(1,0,1);
  SetRobinVertex(1,1,0);
  SetRobinVertex(1,1,1);

  PetscFunctionReturn(0);
}


/* ---------------------------------------------------------------------------
   Set Dirichlet boundary conditions.  The optional vec x contains the exact BC.

   Assume mat has been assembled.
   --------------------------------------------------------------------------- */

#define SetDirichletFace(xmode,ymode,zmode) {				\
  PetscInt onxb,dofxs,dofxe,xs,xe,hasxpml;				\
  PetscInt onyb,dofys,dofye,ys,ye,hasypml;				\
  PetscInt onzb,dofzs,dofze,zs,ze,haszpml;				\
  GetDimMode(0,xmode,onxb,Dirichlet,dofxs,dofxe,xs,xe,hasxpml);		\
  GetDimMode(1,ymode,onyb,Dirichlet,dofys,dofye,ys,ye,hasypml);		\
  GetDimMode(2,zmode,onzb,Dirichlet,dofzs,dofze,zs,ze,haszpml);		\
  if (onxb || onyb || onzb) {						\
    myerr= MyArraySubIS(&subis,PETSC_COMM_SELF,dof->ndof_xloc,		\
			dof->ndof_yloc,dof->ndof_zloc,dofxs,dofxe,	\
			dofys,dofye,dofzs,dofze);			\
    CHKMyErrQ(myerr);							\
  }									\
  else {								\
    ierr= ISCreateGeneral(PETSC_COMM_SELF,0,PETSC_NULL,PETSC_OWN_POINTER,&subis); \
    CHKERRQ(ierr);							\
  }									\
  if (bvp->diri_strat==ZeroRowsColumns) {				\
    ierr= MatZeroRowsColumnsLocalIS(A,subis,bvp->tgv,x,b);		\
    CHKERRQ(ierr);							\
  }									\
  else if (bvp->diri_strat==ZeroRows) {					\
    ierr= MatZeroRowsLocalIS(A,subis,bvp->tgv,x,b);			\
    CHKERRQ(ierr);							\
  }									\
  else printf("loss of Dirichlet BC, check -diri_strat !\n");		\
  ierr= ISDestroy(&subis); CHKERRQ(ierr);				\
}									


#undef __FUNCT__
#define __FUNCT__  "HelmBVPDirichletApply"
MyErrCode HelmBVPDirichletApply(HelmBVP bvp,MyDOF dof,Mat A,Vec b,Vec x)
{
  IS subis=0;
  PetscErrorCode ierr;
  MyErrCode myerr;
  MyDM dm=0;

  PetscFunctionBegin;
  dm= dof->dm;
  if (x) VecScale(x,bvp->tgv);
  SetDirichletFace(0,2,2);
  SetDirichletFace(1,2,2);

  SetDirichletFace(2,0,2);
  SetDirichletFace(2,1,2);

  SetDirichletFace(2,2,0);
  SetDirichletFace(2,2,1);
  PetscFunctionReturn(0);
}

#undef SetDirichletFace
#undef SetRobinFace
#undef SetRobinEdge
#undef SetRobinVertex
#undef GetDimMode

#endif
