#ifndef _MYIO
#define _MYIO

#include "mydm.h"
#include <petscao.h>
#include <petscvec.h>

#define MYIO_MAX_FILENAME 100

typedef enum
{
  FirstXYZ,
  FirstYZX,
  FirstZXY,
  FirstYXZ,
  FirstXZY,
  FirstZYX
} DimOrder;

typedef enum
{
  ArithemeticMean,
  HarmonicMean,
  pArithemeticMean,  
  GeometricMean
} AverageMethod;

typedef struct
{
  PetscReal p;
} AverageCtx;

extern PetscErrorCode MyVecViewAO(Vec v, AO ao, PetscViewer viewer);

extern MyErrCode MyMatView(PetscScalar *A,PetscInt m,PetscInt n,const char *format);

extern MyErrCode SetCellDataConstant(PetscScalar **data,PetscInt siz,PetscScalar val);

extern MyErrCode AverageCellRealToMeshScalar(PetscScalar **array, MyDM dm, AverageMethod method, AverageCtx *ctx, char *filename, PetscScalar xl, PetscScalar xr, PetscScalar yl, PetscScalar yr, PetscScalar zl, PetscScalar zr, PetscInt nx, PetscInt ny, PetscInt nz);

extern MyErrCode ReadArrayDataReal(PetscReal data[],char *filename,PetscInt nx,PetscInt ny,PetscInt nz,PetscInt ixbegin, PetscInt ixend, PetscInt iybegin, PetscInt iyend,PetscInt izbegin,PetscInt izend);

extern MyErrCode AverageCellDataReal(const MPI_Comm comm,const AverageMethod method,const AverageCtx *ctx,const char *filename_old,const PetscInt nx_old,const PetscInt ny_old,const PetscInt nz_old,const char *filename_new,const DimOrder order_new,PetscInt nx_new,PetscInt ny_new,PetscInt nz_new);

#endif
