function chkvscat(s,p,n,opml,npml,olp)

if nargin<1
  s= [4 2 3];                           % num. subdomains, z,y,x
  p= [2 4 3];                           % num. processors, either s|p or p|s
  n= [10 10 12];                          % num. elements
  opml= [1 1 1; 1 1 1];                 % original pml
  npml= 2; olp= 1;                      % dd pml and overlap
elseif nargin<6
  error('incomplete arguments');  
end

%   generate domain vector, petsc indices
nall= n+sum(opml)
v= getpetscmap(nall,p);

%   generate subdomain vectors, values are petsc indices in original domain
%   including original pml, and -1 in dd pml
vs= cell(s); errall= []; sp= max(p./s,[1 1 1]);
for ix=1:s(3)
    for iy= 1:s(2)
        for iz=1:s(1)
            [np, ibegin, iend]= part1d([iz,iy,ix],n,s); % first phys. domain
            ibegin= max(ibegin-floor((olp+1)/2),[1 1 1]); % increase overlap
            iend= min(iend+floor(olp/2),n+1);
            ibegin= ibegin + opml(1,:); % shift to be relative to the
            iend= iend + opml(1,:);     % pml-augmented domain
            
            % add original pml to subdomains, the following works by assuming pml on each
            % side lies entirely in one subdomain
            ibegin= ibegin - opml(1,:).*[iz==1,iy==1,ix==1]; 
            iend= iend + opml(2,:).*[iz==s(1),iy==s(2),ix==s(3)];
            np= iend - ibegin;
            
            % subdomain vector including dd pml
            displ= [iz~=1,iy~=1,ix~=1]*npml;
            dispr= [iz~=s(1),iy~=s(2),ix~=s(3)]*npml;
            vs{iz,iy,ix}= zeros(np+1+displ+dispr);
            iis= 1+displ; iie= np+1+displ;
            for m=1:3
                ids{m}= iis(m):iie(m);
                id{m}= ibegin(m):iend(m);
            end            
            maps= getpetscmap(size(vs{iz,iy,ix})-1,sp);
            vs{iz,iy,ix}(maps(ids{1},ids{2},ids{3}))=v(id{1},id{2},id{3});
            fn= [num2str(ix-1),'_',num2str(iy-1),'_',num2str(iz-1),'.bin'];
            upetsc= PetscBinaryRead(fn,'complex',true);
% $$$             disp(['error for subdomain',fn]);
            errall= [errall max(abs(vs{iz,iy,ix}(:)-1-upetsc))];
            clear maps;
        end
    end
end
max(errall)

end

function [np,ibegin,iend]= part1d(k,n,p)
    r= mod(n,p);
    np= floor(n./p)+(k<=r); ibegin= r.*(k>r)+(k-1).*np+1; iend= ibegin+np;
end

function map=getpetscmap(nall,p)
    count= 0; map= zeros(nall+1);
    for ix= 1:p(3)
        for iy= 1:p(2)  
            for iz= 1:p(1)
                [np,ibegin,iend]= part1d([iz,iy,ix],nall,p);
                idz= ibegin(1):iend(1)-(iz<p(1));
                idy= ibegin(2):iend(2)-(iy<p(2));
                idx= ibegin(3):iend(3)-(ix<p(3));
                lid= [length(idz),length(idy),length(idx)];
                map(idz,idy,idx)= reshape(count+1:count+prod(lid),lid);
                count= count + prod(lid);
            end
        end
    end
end