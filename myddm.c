#include "myddm.h"

static inline void mypart1d(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *nxp, PetscInt *startx,PetscInt *endx);

static inline void mysearch1d(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *rx,PetscInt *startx,PetscInt *endx);

#undef __FUNCT__
#define __FUNCT__  "MyDDMCreate"
MyErrCode MyDDMCreate(MyDDM *p_ddm)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr= PetscMalloc(sizeof(structMyDDM),p_ddm); CHKERRQ(ierr);
  ierr= PetscMemzero(*p_ddm,sizeof(structMyDDM)); CHKERRQ(ierr);
  (*p_ddm)->part1d= mypart1d; (*p_ddm)->search1d= mysearch1d;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyDDMDestroy"
MyErrCode MyDDMDestroy(MyDDM *p_ddm)
{
  PetscErrorCode ierr;
  MyErrCode myerr;
  PetscInt i;

  PetscFunctionBegin;
  if (!*p_ddm) return(0);
  ierr= PetscFree((*p_ddm)->myname); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->prefix); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->xs_in); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->xe_in); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->ys_in); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->ye_in); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->zs_in); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->ze_in); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->xs); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->xe); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->ys); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->ye); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->zs); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->ze); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->opmlxl); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->opmlxr); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->opmlyl); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->opmlyr); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->opmlzl); CHKERRQ(ierr);
  ierr= PetscFree((*p_ddm)->opmlzr); CHKERRQ(ierr);
  if ((*p_ddm)->subdm) 
    for (i = 0; i < (*p_ddm)->sloc; ++i) {
      MPI_Comm_free(&((*p_ddm)->subdm[i]->comm)); /* collective */
      myerr= MyDMDestroy((*p_ddm)->subdm+i); CHKMyErrQ(myerr);
    }  
  ierr= PetscFree((*p_ddm)->subdm); CHKERRQ(ierr);
  ierr= PetscFree(*p_ddm); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyDDMSetPrefix"
MyErrCode MyDDMSetPrefix(MyDDM ddm,const char prefix[])
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if(prefix){
    if(prefix[0]) {
      if (!(ddm->prefix)) {
	ierr= PetscMalloc(MYDDM_MAX_NAMELEN*sizeof(char),&(ddm->prefix));
	CHKERRQ(ierr);
      }
      PetscStrcpy(ddm->prefix,prefix);
    }
    else PetscFree(ddm->prefix); /* set to PETSC_NULL */
  }
  else PetscFree(ddm->prefix);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyDDMSetNumSub"
inline MyErrCode MyDDMSetNumSub(MyDDM ddm,PetscInt sx,PetscInt sy,PetscInt sz)
{
  if (sx<=0 || sy<=0 || sz<=0) {sx= 0; sy=0; sz= 0; ddm->has_numsub= PETSC_FALSE;}
  else {ddm->sx= sx; ddm->sy= sy; ddm->sz= sz; ddm->has_numsub= PETSC_TRUE;}
  ddm->has_partition= PETSC_FALSE; ddm->has_interface= PETSC_FALSE;
  ddm->initialized= PETSC_FALSE;
  return 0;
}

#undef __FUNCT__
#define __FUNCT__  "MyDDMSetNumSubFromOptions"
MyErrCode MyDDMSetNumSubFromOptions(MyDDM ddm)
{
  PetscErrorCode ierr;
  PetscBool set;

  PetscFunctionBegin;
  ierr= PetscOptionsGetInt(ddm->prefix,"-sx",&(ddm->sx),&set); CHKERRQ(ierr);
  if (!set) ddm->sx= ddm->gdm->px;
  ierr= PetscOptionsGetInt(ddm->prefix,"-sy",&(ddm->sy),&set); CHKERRQ(ierr);
  if (!set) ddm->sy= ddm->gdm->py;
  ierr= PetscOptionsGetInt(ddm->prefix,"-sz",&(ddm->sz),&set); CHKERRQ(ierr);
  if (!set) ddm->sz= ddm->gdm->pz;
  ddm->has_numsub= PETSC_TRUE; ddm->has_partition=PETSC_FALSE;
  ddm->has_interface=PETSC_FALSE; ddm->initialized= PETSC_FALSE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyDDMSetExtension"
inline MyErrCode MyDDMSetExtension(MyDDM ddm,PetscInt nolp,PetscInt npml)
{
  if (nolp<0 || npml<0) {nolp= 0; npml= 0; ddm->has_extension= PETSC_FALSE;}
  else {ddm->nolp= nolp; ddm->npml= npml; ddm->has_extension= PETSC_TRUE;}
  ddm->has_partition= PETSC_FALSE; ddm->has_interface= PETSC_FALSE;
  ddm->initialized= PETSC_FALSE;
  return 0;
}

#undef __FUNCT__
#define __FUNCT__  "MyDDMSetExtensionFromOptions"
MyErrCode MyDDMSetExtensionFromOptions(MyDDM ddm)
{
  PetscErrorCode ierr;
  PetscBool set;

  PetscFunctionBegin;
  ierr= PetscOptionsGetInt(ddm->prefix,"-nolp",&(ddm->nolp),&set); CHKERRQ(ierr);
  if (!set) ddm->nolp= 0;
  ierr= PetscOptionsGetInt(ddm->prefix,"-npml",&(ddm->npml),&set); CHKERRQ(ierr);
  if (!set) ddm->npml= 0;
  ddm->has_extension= PETSC_TRUE; ddm->has_partition=PETSC_FALSE;
  ddm->has_interface= PETSC_FALSE; ddm->initialized= PETSC_FALSE;
  PetscFunctionReturn(0);
}

/*
   Copied from mydm.c.
*/
static inline void mypart1d(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *nxp, PetscInt *startx,PetscInt *endx)
{
  if (ix<(nx % px)) {
    (*nxp)= nx/px+1; (*startx)= ix*(*nxp);  (*endx)= (*startx) + (*nxp);
  }
  else {
    (*nxp)= nx/px; (*startx)= (nx%px)+ix*(*nxp); (*endx)= (*startx)+(*nxp);
  }
}

static inline void mysearch1d(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *rx,PetscInt *startx,PetscInt *endx)
{
  /* ix is the index of the element, rx is the index of the subdomain,
   * computed according to mypart1d */
  if (ix<(nx % px)*(nx/px+1)) {
    (*rx)= ix/(nx/px+1); (*startx)= (*rx)*(nx/px+1); (*endx)= (*startx) + (nx/px+1);
  }
  else {
    (*rx)= nx%px + (ix-(nx%px)*(nx/px+1))/(nx/px); 
    (*startx)= (*rx)*(nx/px) + nx%px; (*endx)= (*startx) + nx/px;
  }
}

/*
   Given number of subdomains, compute the partition.  First, compute
   non-overlapping partition of the original __physical__ domain to xs_in,
   xe_in, etc.  Then, extend each subdomain according to overlap, DD pml and the
   original pml.
*/
#undef __FUNCT__
#define __FUNCT__  "MyDDMComputePartition"
MyErrCode MyDDMComputePartition(MyDDM ddm)
{
  PetscErrorCode ierr;
  MyDM gdm;
  PetscInt ix,iy,iz,i=0,s,nxs_in,nys_in,nzs_in;
  MPI_Comm subcomm;
  MyErrCode myerr;

  PetscFunctionBegin;
  if (!ddm->gdm) return NullMyDM;
  if (!ddm->gdm->initialized) return MyDMNotInitialized;
  if (!ddm->has_numsub || !ddm->has_extension) return DDMNeedsInputs;
  gdm= ddm->gdm;
  if (gdm->px % ddm->sx && ddm->sx % gdm->px) return SubToProcNonInteger;
  if (gdm->py % ddm->sy && ddm->sy % gdm->py) return SubToProcNonInteger;
  if (gdm->pz % ddm->sz && ddm->sz % gdm->pz) return SubToProcNonInteger;
  ddm->sxloc= PetscMax(ddm->sx/gdm->px,1);
  ddm->syloc= PetscMax(ddm->sy/gdm->py,1);
  ddm->szloc= PetscMax(ddm->sz/gdm->pz,1);
  ddm->sloc= ddm->sxloc*(ddm->syloc)*(ddm->szloc);
  ierr= PetscMalloc(ddm->sxloc*sizeof(PetscInt),&(ddm->xs_in)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->syloc*sizeof(PetscInt),&(ddm->ys_in)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->szloc*sizeof(PetscInt),&(ddm->zs_in)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->sxloc*sizeof(PetscInt),&(ddm->xe_in)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->syloc*sizeof(PetscInt),&(ddm->ye_in)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->szloc*sizeof(PetscInt),&(ddm->ze_in)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->sxloc*sizeof(PetscInt),&(ddm->xs)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->syloc*sizeof(PetscInt),&(ddm->ys)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->szloc*sizeof(PetscInt),&(ddm->zs)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->sxloc*sizeof(PetscInt),&(ddm->xe)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->syloc*sizeof(PetscInt),&(ddm->ye)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->szloc*sizeof(PetscInt),&(ddm->ze)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->sxloc*sizeof(PetscInt),&(ddm->opmlxl)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->sxloc*sizeof(PetscInt),&(ddm->opmlxr)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->syloc*sizeof(PetscInt),&(ddm->opmlyl)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->syloc*sizeof(PetscInt),&(ddm->opmlyr)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->szloc*sizeof(PetscInt),&(ddm->opmlzl)); CHKERRQ(ierr);
  ierr= PetscMalloc(ddm->szloc*sizeof(PetscInt),&(ddm->opmlzr)); CHKERRQ(ierr);
  /* x */
  if (ddm->sx<=gdm->px) {
    ddm->spx= gdm->px/ddm->sx; ddm->isxs= gdm->rx/ddm->spx; ddm->isxe= ddm->isxs+1;
    ddm->part1d(ddm->isxs,gdm->nx_phy,ddm->sx,&nxs_in,ddm->xs_in,ddm->xe_in);
    ddm->xs[0]= PetscMax(ddm->xs_in[0]-(ddm->nolp+1)/2,0);
    ddm->xe[0]= PetscMin(ddm->xe_in[0]+ddm->nolp/2,gdm->nx_phy);
    /* printf("proc %d, (xs, xe)=(%d,%d)\n",ddm->gdm->rank,ddm->xs[0],ddm->xe[0]); */
    ddm->opmlxl[0]= (ddm->isxs)?0:gdm->eta_xl;
    ddm->opmlxr[0]= (ddm->isxs-ddm->sx+1)?0:gdm->eta_xr;
  }
  else {
    ddm->spx= 1; ddm->isxs= gdm->rx*(ddm->sxloc); ddm->isxe= ddm->isxs+ddm->sxloc;
    for (ix=0;ix<ddm->sxloc;ix++) {
      ddm->part1d(ddm->isxs+ix,gdm->nx_phy,ddm->sx,&nxs_in,ddm->xs_in+ix,ddm->xe_in+ix);
      ddm->xs[ix]= PetscMax(ddm->xs_in[ix]-(ddm->nolp+1)/2,0);
      ddm->xe[ix]= PetscMin(ddm->xe_in[ix]+ddm->nolp/2,gdm->nx_phy);
      ddm->opmlxl[ix]= (ddm->isxs+ix)?0:gdm->eta_xl;
      ddm->opmlxr[ix]= (ddm->isxs+ix-ddm->sx+1)?0:gdm->eta_xr;      
    }
  }
  /* y */
  if (ddm->sy<gdm->py) {
    ddm->spy= gdm->py/ddm->sy; ddm->isys= gdm->ry/ddm->spy; ddm->isye= ddm->isys+1;
    ddm->part1d(ddm->isys,gdm->ny_phy,ddm->sy,&nys_in,ddm->ys_in,ddm->ye_in);
    ddm->ys[0]= PetscMax(ddm->ys_in[0]-(ddm->nolp+1)/2,0);
    ddm->ye[0]= PetscMin(ddm->ye_in[0]+ddm->nolp/2,gdm->ny_phy);
    ddm->opmlyl[0]= (ddm->isys)?0:gdm->eta_yl;
    ddm->opmlyr[0]= (ddm->isys-ddm->sy+1)?0:gdm->eta_yr;
  }
  else {
    ddm->spy= 1; ddm->isys= gdm->ry*(ddm->syloc); ddm->isye= ddm->isys+ddm->syloc;
    for (iy=0;iy<ddm->syloc;iy++) {
      ddm->part1d(ddm->isys+iy,gdm->ny_phy,ddm->sy,&nys_in,ddm->ys_in+iy,ddm->ye_in+iy);
      ddm->ys[iy]= PetscMax(ddm->ys_in[iy]-(ddm->nolp+1)/2,0);
      ddm->ye[iy]= PetscMin(ddm->ye_in[iy]+ddm->nolp/2,gdm->ny_phy);
      ddm->opmlyl[iy]= (ddm->isys+iy)?0:gdm->eta_yl;
      ddm->opmlyr[iy]= (ddm->isys+iy-ddm->sy+1)?0:gdm->eta_yr;
    }
  }
  /* z */
  if (ddm->sz<gdm->pz) {
    ddm->spz= gdm->pz/ddm->sz; ddm->iszs= gdm->rz/ddm->spz; ddm->isze= ddm->iszs+1;
    ddm->part1d(ddm->iszs,gdm->nz_phy,ddm->sz,&nzs_in,ddm->zs_in,ddm->ze_in);
    ddm->zs[0]= PetscMax(ddm->zs_in[0]-(ddm->nolp+1)/2,0);
    ddm->ze[0]= PetscMin(ddm->ze_in[0]+ddm->nolp/2,gdm->nz_phy);
    ddm->opmlzl[0]= (ddm->iszs)?0:gdm->eta_zl;
    ddm->opmlzr[0]= (ddm->iszs-ddm->sz+1)?0:gdm->eta_zr;
  }
  else {
    ddm->spz= 1; ddm->iszs= gdm->rz*(ddm->szloc); ddm->isze= ddm->iszs+ddm->szloc;
    for (iz=0;iz<ddm->szloc;iz++) {
      ddm->part1d(ddm->iszs+iz,gdm->nz_phy,ddm->sz,&nzs_in,ddm->zs_in+iz,ddm->ze_in+iz);
      ddm->zs[iz]= PetscMax(ddm->zs_in[iz]-(ddm->nolp+1)/2,0);
      ddm->ze[iz]= PetscMin(ddm->ze_in[iz]+ddm->nolp/2,gdm->nz_phy);
      ddm->opmlzl[iz]= (ddm->iszs+iz)?0:gdm->eta_zl;
      ddm->opmlzr[iz]= (ddm->iszs+iz-ddm->sz+1)?0:gdm->eta_zr;
    }
  }
  ddm->sp= ddm->spx*(ddm->spy)*(ddm->spz);
  /* subdomain dm on this proc */
  ierr= PetscMalloc(ddm->sxloc*(ddm->syloc)*(ddm->szloc)*sizeof(MyDM),&(ddm->subdm));CHKERRQ(ierr);
  i= 0;
  for (ix = 0; ix < ddm->sxloc; ++ix) {
    for (iy = 0; iy < ddm->syloc; ++iy) {
      for (iz = 0; iz < ddm->szloc; ++iz) {
	MyDMCreate(ddm->subdm+i);
	s= (ddm->isxs+ix)*(ddm->sy)*(ddm->sz) + (ddm->isys+iy)*(ddm->sz) + ddm->iszs+iz;
	MPI_Comm_split(gdm->comm,s,gdm->rank,&subcomm); /* collective */
	myerr= MyDMSetComm(ddm->subdm[i],subcomm,ddm->spx,ddm->spy,ddm->spz); CHKMyErrQ(myerr);
	myerr= MyDMSetNumCells(ddm->subdm[i],ddm->xe[ix]-ddm->xs[ix],ddm->ye[iy]-ddm->ys[iy],ddm->ze[iz]-ddm->zs[iz]); CHKMyErrQ(myerr);
	myerr= MyDMSetNumPML(ddm->subdm[i],(ddm->isxs+ix)?ddm->npml:gdm->eta_xl,
			     (ddm->isxs+ix-ddm->sx+1)?ddm->npml:gdm->eta_xr,
			     (ddm->isys+iy)?ddm->npml:gdm->eta_yl,
			     (ddm->isys+iy-ddm->sy+1)?ddm->npml:gdm->eta_yr,
			     (ddm->iszs+iz)?ddm->npml:gdm->eta_zl,
			     (ddm->iszs+iz-ddm->sz+1)?ddm->npml:gdm->eta_zr);
	CHKMyErrQ(myerr);	
	if (!ddm->subdm[i]->myname) {
	  ierr=PetscMalloc(sizeof(char)*MYDM_MAX_NAMELEN,&(ddm->subdm[i]->myname));CHKERRQ(ierr);
	  PetscMemzero(ddm->subdm[i]->myname,sizeof(char)*MYDM_MAX_NAMELEN);
	}
	if (!ddm->subdm[i]->prefix) {
	  ierr=PetscMalloc(sizeof(char)*MYDM_MAX_NAMELEN,&(ddm->subdm[i]->prefix));CHKERRQ(ierr);
	  PetscMemzero(ddm->subdm[i]->prefix,sizeof(char)*MYDM_MAX_NAMELEN);
	}
	if (gdm->myname && gdm->myname[0]) {
	  sprintf(ddm->subdm[i]->myname,"%s_%d",gdm->myname,s);
	}
        else {
	  sprintf(ddm->subdm[i]->myname,"s%d",s);
	}
	if (gdm->prefix && gdm->prefix[0]) {
	  sprintf(ddm->subdm[i]->prefix,"%s_%d",gdm->prefix,s);
	}
	else {
	  sprintf(ddm->subdm[i]->prefix,"s%d",s);
	}
	MyDMSetup(ddm->subdm[i]);
	/* assume the original gdm has uniform coordinates, so is subdm here */
	MyDMSetUniformCoord(ddm->subdm[i],
			    gdm->x[ddm->xs[ix]+gdm->eta_xl], gdm->x[ddm->xe[ix]+gdm->eta_xl],
			    gdm->y[ddm->ys[iy]+gdm->eta_yl], gdm->y[ddm->ye[iy]+gdm->eta_yl],
			    gdm->z[ddm->zs[iz]+gdm->eta_zl], gdm->z[ddm->ze[iz]+gdm->eta_zl]);
	MyDMViewOnScreen(ddm->subdm[i]);
	i++;
      }
    }
  }
  ddm->has_interface= PETSC_FALSE; ddm->initialized= PETSC_FALSE;
  if (ddm->has_partition) return DDMResetPartition;
  else ddm->has_partition= PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyDDMComputeInterface"
MyErrCode MyDDMComputeInterface(MyDDM ddm)
{
  PetscFunctionBegin;
  if (!ddm->gdm) return NullMyDM;
  if (!ddm->gdm->initialized) return MyDMNotInitialized;
  if (!ddm->has_partition) return DDMNeedsInputs;

  ddm->initialized= PETSC_FALSE;
  if (ddm->has_interface) return DDMResetInterface;
  else ddm->has_interface= PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyDDMSetup"
MyErrCode MyDDMSetup(MyDDM ddm)
{
  PetscErrorCode myerr;
  PetscFunctionBegin;
  if (ddm->initialized) return DDMAlreadyInitialized;
  if (!ddm->has_numsub || !ddm->has_extension) return DDMNeedsInputs;
  myerr=MyDDMComputePartition(ddm);CHKMyErrQ(myerr);
  ddm->initialized= PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyDDMSetFromOptions"
MyErrCode MyDDMSetFromOptions(MyDDM ddm,MyDM gdm,char filename[],char prefix[])
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (gdm==PETSC_NULL) return NullMyDM;
  if (!gdm->initialized) return MyDMNotInitialized;
  ddm->gdm= gdm;
  if (filename) {
    if (filename[0]) {
      ierr= PetscOptionsInsertFile(gdm->comm,filename,PETSC_TRUE); CHKERRQ(ierr);
    }
  }
  MyDDMSetPrefix(ddm,prefix);
  MyDDMSetNumSubFromOptions(ddm);
  MyDDMSetExtensionFromOptions(ddm);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__  "MyDDMViewOnScreen"
MyErrCode MyDDMViewOnScreen(MyDDM ddm)
{
  PetscInt i, ix, iy, iz;
  PetscFunctionBegin;
  if (!ddm->initialized) return DDMNotInitialized;  
  PetscPrintf(ddm->gdm->comm,"/ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n");
  PetscPrintf(ddm->gdm->comm,"/       MyDDM on MyDM %s (bounds are describing the interval [ ))     \n",ddm->gdm->myname);
  PetscPrintf(ddm->gdm->comm,"/\n");
  PetscPrintf(ddm->gdm->comm,"/ Number of subdomains: %dx%dx%d\n",ddm->sx,ddm->sy,ddm->sz);
  PetscPrintf(ddm->gdm->comm,"/ Number of elements in overlap: %d, in pml:  %d\n",ddm->nolp,ddm->npml);
  PetscPrintf(ddm->gdm->comm,"/ Number of processors per subdomain: %d = %dx%dx%d\n",ddm->sp,ddm->spx,ddm->spy,ddm->spz);
  PetscSynchronizedPrintf(ddm->gdm->comm,"/ Processor %d:\n",ddm->gdm->rank);
  PetscSynchronizedPrintf(ddm->gdm->comm,"/    Number of subdomains participated in:  %d = %dx%dx%d\n",ddm->sloc,ddm->sxloc,ddm->syloc,ddm->szloc);
  PetscSynchronizedPrintf(ddm->gdm->comm,  "/    Subdomains participated in:  (%d~%d,%d~%d,%d~%d), i.e.\n/       ",ddm->isxs,ddm->isxe,ddm->isys,ddm->isye,ddm->iszs,ddm->isze);
  for (i=0; i<ddm->sloc; ++i) {
    /* collective: call together, ok, because each processor has the same number of subdomains */
    PetscSynchronizedPrintf(ddm->gdm->comm,"%s, ",ddm->subdm[i]->myname);
    if ((i+1)%8==0) PetscSynchronizedPrintf(ddm->gdm->comm,"\n/        ");
  }
  PetscSynchronizedFlush(ddm->gdm->comm,PETSC_STDOUT);
  for (i=0; i<ddm->sloc; ++i) {
    PetscPrintf(ddm->subdm[i]->comm,"/ %s:\n",ddm->subdm[i]->myname);
    MyArrayind2sub(ddm->sxloc,ddm->syloc,ddm->szloc,&ix,&iy,&iz,i);
    PetscPrintf(ddm->subdm[i]->comm,"/    Bounds of no-overlap  phys. subdomain in orig. phys. domain: (%d~%d,%d~%d,%d~%d)\n",ddm->xs_in[ix],ddm->xe_in[ix],ddm->ys_in[iy],ddm->ye_in[iy],ddm->zs_in[iz],ddm->ze_in[iz]);
    PetscPrintf(ddm->subdm[i]->comm,"/    Bounds of overlapping phys. subdomain in orig. phys. domain: (%d~%d,%d~%d,%d~%d)\n",ddm->xs[ix],ddm->xe[ix],ddm->ys[iy],ddm->ye[iy],ddm->zs[iz],ddm->ze[iz]);
    PetscPrintf(ddm->subdm[i]->comm,"/    Num. cells of subdomain with overlap and pml: (%d,%d,%d)\n",ddm->subdm[i]->nx_all,ddm->subdm[i]->ny_all,ddm->subdm[i]->nz_all);
    PetscPrintf(ddm->subdm[i]->comm,"/    Num. cells of original pml: (%d,%d,%d,%d,%d,%d)\n",ddm->opmlxl[ix],ddm->opmlxr[ix],ddm->opmlyl[iy],ddm->opmlyr[iy],ddm->opmlzl[iz],ddm->opmlzr[iz]);
  }
  /* for (i=0; i<ddm->sloc; ++i) { */
  /*   MyDMViewOnScreen(ddm->subdm[i]); */
  /* } */
  PetscPrintf(ddm->gdm->comm,"/ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n");
  PetscFunctionReturn(0);
}
