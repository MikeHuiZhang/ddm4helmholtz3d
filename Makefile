OBJS = helm3d.o helmbvp.o myddof.o mydof.o myddm.o mydm.o myarray.o myio.o myerr.o kspdirect.o fetidof.o fetihelm.o
#CFLAGS = -g -O0
default: clean helm3d
helm3d: $(OBJS)
#-@${MKDIR} bin
	${CLINKER} -o $@ $^ ${PETSC_LIB}
	${DSYMUTIL} $@
	${RM} -f *.o
	etags *.[h,c]
clean::
	${RM} helm3d
#	${RM} TAGS
#	${RM} -r helm3d.dSYM

PETSC_DIR= /Users/huizhang/Software/petsc
PETSC_ARCH= z-clang-db
include ${PETSC_DIR}/conf/variables
include ${PETSC_DIR}/conf/rules
