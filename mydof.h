#ifndef _MYDOF
#define _MYDOF

#include "mydm.h"
#include <petscao.h>
#include <petscvec.h>

//#define MyDOF_USE_AO
//#define MyDOF_USE_Comm

typedef enum
{
  MyDOFQ1,
} MyDOFType;


/*
   Define dof associated with mesh nodes, distributed on processors.  Both mesh
   and dof are cartesian.
*/
struct MyDOF_
{
  /*
     mesh on which we build dof
  */
  MyDM dm;

  /*
     status
  */
  PetscBool initialized;

  /*
     dof type
  */
  MyDOFType type;

  /*
     number of dof on each cell
  */
  PetscInt ndof_cell, ndofx_cell, ndofy_cell, ndofz_cell;

  /*
     number of global dof
  */
  PetscInt ndof, ndof_x, ndof_y, ndof_z;

  /*
     number of pml dof on the boundary, dof on physical boundary is not counted
  */
  PetscInt ndofpmlxl, ndofpmlxr, ndofpmlyl, ndofpmlyr, ndofpmlzl, ndofpmlzr;

  /*
     search function
  */
  MyErrCode (*search1d)(PetscInt ix, PetscInt dim, MyDM dm, PetscInt *rx, PetscInt *xins, PetscInt *xine);
  void (*petscid)(const struct MyDOF_ *dof, PetscInt ix, PetscInt iy, PetscInt iz, PetscInt *id);
  
  /*
     function for query coordinate in 1-D
  */
  MyErrCode (*getcoord1d)(PetscScalar**,const struct MyDOF_*,PetscInt,PetscInt,PetscInt);  

  /*
     function for query elements in 1-D
  */
  MyErrCode (*getelem1d)(PetscInt*,PetscInt**,const struct MyDOF_*,PetscInt,PetscInt);

  /*
     local dof on this processor including ghosted, in 3D subscripts on the dm
     with pml
  */
  PetscInt ndof_loc, ndof_xloc, ndof_yloc, ndof_zloc;
  PetscInt dofxlocs, dofxloce, dofylocs, dofyloce, dofzlocs, dofzloce;
  
  /*
     local dof excluding ghosted, in 3D subscripts on the dm with pml
  */
  PetscInt ndof_in, ndof_xin, ndof_yin, ndof_zin;
  PetscInt dofxins, dofxine, dofyins, dofyine, dofzins, dofzine;

  /*
     number of local (on proc. including ghosted) pml dof, it is zero for
     interior proc.
  */
  PetscInt npmlxl_loc, npmlxr_loc, npmlyl_loc, npmlyr_loc, npmlzl_loc, npmlzr_loc;

  /*
     number of local (on proc. excluding ghosted) pml dof, it is zero for
     interior proc.
  */
  PetscInt npmlxl_in, npmlxr_in, npmlyl_in, npmlyr_in, npmlzl_in, npmlzr_in;


  /*
     local to global mapping in 1D numbering: assume Petsc orders Vec and Mat in
     such a way that the first n1 entries go to the first processor, the next n2
     entries go to the second processor, etc.
  */
  PetscInt dofins, dofine;     /* in petsc ordering, dof contiguous on each proc. */
#ifdef MyDOF_USE_AO  
  AO ao;		       /* optional: global natural order to global petsc
				* order, not scalable either in time or memory */
#endif
  ISLocalToGlobalMapping ltog; /* local (incl. ghosted) natural to global petsc */
  PetscInt *NaturalIndices;
  Vec v;		       /* a sample global vector */
  const PetscInt *ranges;      /* ownership ranges of v */

  /*
     cell dof map in natural odering
  */
  PetscInt (*celldof0) (PetscInt ix, PetscInt iy, PetscInt iz, PetscInt nx, PetscInt ny, PetscInt nz);
  PetscInt (*celldofi) (PetscInt idof, PetscInt i0, PetscInt ndof_x, PetscInt ndof_y, PetscInt ndof_z);

  /*
     2D cell dof map in natural ordering within a rectangle mesh
  */
  PetscInt (*facedof0) (PetscInt ix, PetscInt iy, PetscInt nx, PetscInt ny); 
  PetscInt (*facedofi) (PetscInt idof, PetscInt i0, PetscInt ndofx, PetscInt ndofy);
  
  /*
     1D cell dof map within a line segment
  */
  PetscInt (*edgedof0) (PetscInt i, PetscInt n);
  PetscInt (*edgedofi) (PetscInt idof,PetscInt i0,PetscInt ndof);
};

typedef struct MyDOF_  structMyDOF, *MyDOF;
extern MyErrCode MyDOFCreate(MyDOF *p_dof);
extern MyErrCode MyDOFDestroy(MyDOF *p_dof);
extern MyErrCode MyDOFSetDM(MyDOF dof, MyDM dm);
extern MyErrCode MyDOFSetType(MyDOF dof, MyDOFType type);
extern MyErrCode MyDOFGetType(MyDOF dof, MyDOFType *type);
extern MyErrCode MyDOFSetup(MyDOF dof);
extern MyErrCode MyDOFViewOnScreen(MyDOF dof);

#endif
