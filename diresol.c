  {
    Vec udirect=0;
    VecDuplicate(b,&udirect);
    DirectSetupKSP(&kspdirect,A);
    ierr= KSPSolve(kspdirect,b,udirect); CHKERRQ(ierr);
#if 0
    VecView(udirect,PETSC_VIEWER_STDOUT_(dm->comm));
    MatView(A,PETSC_VIEWER_STDOUT_(dm->comm));
    VecView(b,PETSC_VIEWER_STDOUT_(dm->comm));
#endif
#if 1
    PetscViewerBinaryOpen(PETSC_COMM_WORLD,"u.bin",FILE_MODE_WRITE,&viewer);
#ifdef MyDOF_USE_AO
    MyVecViewAO(udirect,dof->ao,viewer); PetscViewerDestroy(&viewer);
#else
    VecView(udirect,viewer); PetscViewerDestroy(&viewer);
    PetscViewerBinaryOpen(PETSC_COMM_WORLD,"idx.bin",FILE_MODE_WRITE,&viewer);
    PetscIntView(dof->ndof_in,dof->NaturalIndices,viewer);
    PetscViewerDestroy(&viewer);
#endif
#endif
#if 0
    PetscViewerBinaryOpen(PETSC_COMM_WORLD,"A.bin",FILE_MODE_WRITE,&viewer);
    MatView(A,viewer); PetscViewerDestroy(&viewer);
    PetscViewerBinaryOpen(PETSC_COMM_WORLD,"b.bin",FILE_MODE_WRITE,&viewer);
    VecView(b,viewer); PetscViewerDestroy(&viewer);
#endif
    VecDestroy(&udirect);
  }
