#ifndef _MYDM
#define _MYDM

#include "myerr.h"
#include "myarray.h"
#define MYDM_MAX_NAMELEN 100
/*
   Define the global mesh distributed on processors.
*/
typedef struct
{
  /*
     communicator
  */
  MPI_Comm comm;

  /*
     number of processors in each direction and total
  */
  PetscInt px, py, pz, p;

  /*
     number of elements on global mesh in each direction
  */
  PetscInt nx_phy, ny_phy, nz_phy, n_phy; /* physical mesh */
  PetscInt eta_xl, eta_xr, eta_yl, eta_yr, eta_zl, eta_zr; /* number of elements to extend from the bounds of x,y,z */
  PetscInt nx_all, ny_all, nz_all; /* extended mesh */

  /*
     1D partition and search functions
  */
  void (*part1d)(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *nxp, PetscInt *startx,PetscInt *endx);
  void (*search1d)(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *rx, PetscInt *startx,PetscInt *endx);

  /*
     element-wise non-overlapping partition of extended mesh onto current processor
  */
  PetscMPIInt rank, rx, ry, rz; /* location of current proc. */
  PetscInt nxloc, nyloc, nzloc, nloc;	/* number of elements on this proc. */
  PetscInt ixbegin, ixend, iybegin, iyend, izbegin, izend; /* bounds relative to extended mesh */
  PetscInt etaloc_xl, etaloc_xr, etaloc_yl, etaloc_yr, etaloc_zl, etaloc_zr; /* number of pml elements on proc. */

  /*
     coordinates of global extended mesh nodes in each direction
  */
  PetscScalar *x,*y,*z;
    
  /*
     status
  */
  PetscBool initialized, has_coord;

  /*
     name of this MyDM
  */
  char *myname;
  
  /*
     prefix for options
  */
  char *prefix;

} structMyDM, *MyDM;

extern MyErrCode MyDMCreate(MyDM *p_dm);

extern MyErrCode MyDMDestroy(MyDM *p_dm);

extern MyErrCode MyDMSetFromOptions(MyDM dm,MPI_Comm comm,const char *optsfile,const char *prefix);

extern MyErrCode MyDMViewOnScreen(MyDM dm);

extern MyErrCode MyDMSetComm(MyDM dm,MPI_Comm comm,PetscInt px,PetscInt py,PetscInt pz);

extern MyErrCode MyDMSetCommFromOptions(MyDM dm,MPI_Comm comm);

extern MyErrCode MyDMSetNumCellsFromOptions(MyDM dm);

extern MyErrCode MyDMSetNumCells(MyDM dm, PetscInt nx_phy, PetscInt ny_phy, PetscInt nz_phy);

extern MyErrCode MyDMSetNumPML(MyDM dm, PetscInt eta_xl, PetscInt eta_xr, PetscInt eta_yl,PetscInt eta_yr, PetscInt eta_zl, PetscInt eta_zr);

extern MyErrCode MyDMSetNumPMLFromOptions(MyDM dm);

extern MyErrCode MyDMSetup(MyDM dm);

extern MyErrCode MyDMSetUniformCoord(MyDM dm, PetscScalar xmin, PetscScalar xmax, PetscScalar ymin, PetscScalar ymax, PetscScalar zmin, PetscScalar zmax);

extern MyErrCode MyDMSetName(MyDM dm,const char *name);
extern MyErrCode MyDMSetNameFromOptions(MyDM dm);

#endif
