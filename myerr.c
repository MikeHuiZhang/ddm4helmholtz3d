#include "myerr.h"

void CHKMyErrQ(MyErrCode myerr)
{
  if(!myerr) return;
  /* else PetscErrorPrintf(PETSC_COMM_WORLD,"%s: ",__FUNCT__); */
  switch (myerr){
  case WrongDomain:
    PetscErrorPrintf("Wrong domain for bvp! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case CommUndefined:
    PetscErrorPrintf("Comm is null! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case MyDMUndefined:
    PetscErrorPrintf("DM is null! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case NumProcWrongFactors:
    PetscErrorPrintf("px*py*pz!=p || px<=0, ..! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case IllegalIndex:
    PetscErrorPrintf("Illegal index! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case WrongNumCells:
    PetscErrorPrintf("Number of cells must be positive! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case WrongNumPML:
    PetscErrorPrintf("Number of cells in PML must be positive! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case CellLessThanProc:
    PetscErrorPrintf("num cells must be >= num proc! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case MyDMNotInitialized:
    PetscErrorPrintf("MyDM is not initialized! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case HelmBVPNullFun:
    PetscErrorPrintf("HelmBVP necessary function member is missing! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case CannotOpenFile:
    PetscErrorPrintf("Cannot open file! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case FileLengthNonMatching:
    PetscErrorPrintf("File length is not matching with the number of cells! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case CtxIsNull:
    PetscErrorPrintf("Necessary context is null! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case FileNameIsNull:
    PetscErrorPrintf("File name is null! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case HasCoordFalse:
    PetscErrorPrintf("DM Does not have coordinates! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case CoordOutofDomain:
    PetscErrorPrintf("Coorindates are out of domain! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case OptArrayIgnored:		/* this should not break the processes */
    PetscErrorPrintf("Extra values of option array are ignored! ");
    break;
  case OptArrayInComplete:
    PetscErrorPrintf("Not enough values for option array! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case ZeroPhyCells:
    PetscErrorPrintf("Zero number of physical cells! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case SrcPtsUnsetted:
    PetscErrorPrintf("Coordinates of point sources are not setted! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case WavespeedDomainUnsetted:
    PetscErrorPrintf("Domain of wavespeed is unsetted! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case GaussQuadOrderOut:
    PetscErrorPrintf("Gauss quadrature order is not implemented! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case UnknownCtxFun:
    PetscErrorPrintf("Unknown CtxFun! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case MyDOFNotInitialized:
    PetscErrorPrintf("MyDOF is not initialized! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case RobinOnPMLNotImplemented:
    PetscErrorPrintf("Robin on PML boundary is not implemented!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  /* case NotAnEdge: */
  /*   PetscErrorPrintf("Not an edge to set!"); */
  /*   MPI_Abort(MPI_COMM_WORLD,myerr); */
  /*   break;     */
  /* case NotAFace: */
  /*   PetscErrorPrintf("Not a face to set!"); */
  /*   MPI_Abort(MPI_COMM_WORLD,myerr); */
  /*   break;     */
  /* case NotAVertex: */
  /*   PetscErrorPrintf("Not a vertex to set!"); */
  /*   MPI_Abort(MPI_COMM_WORLD,myerr); */
  /*   break;     */
  case NullMyDM:
    PetscErrorPrintf("MyDM is null!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case MyDMNeedsFunc:
    PetscErrorPrintf("MyDM's function member is null!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case DDMNeedsInputs:
    PetscErrorPrintf("MyDDM needs to SetNumSub or SetSubdomains before ComputePartition");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case DDMResetPartition:
    PetscErrorPrintf("Partition of MyDDM is resetted!");
    break;
  case DDMResetInterface:
    PetscErrorPrintf("Interface of MyDDM is resetted!");
    break;
  case DDMAlreadyInitialized:
    PetscErrorPrintf("MyDDM is already setup!");
    break;
  case DDMNotInitialized:
    PetscErrorPrintf("MyDDM is not setup!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case ZeroSubdomains:
    PetscErrorPrintf("Zero number of subdomains! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case TooManySubdomains:
    PetscErrorPrintf("Too many number of subdomains! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case SubToProcNonInteger:
    PetscErrorPrintf("Number of subdomains to num proc and vice versa are not interger! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case NotTheSameDM:
    PetscErrorPrintf("ddof->gdof->dm must be the same as ddof->ddm->gdm!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case DDOFNeedsInputs:
    PetscErrorPrintf("MyDDOF needs GDOF and DDM be setted and initialized before setup");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case DDOFNotInitialized:
    PetscErrorPrintf("MyDDOF is not initialized. Please call MyDDOFSetup first.");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case SubDOFNull:
    PetscErrorPrintf("subdof or subdof[0] of ddof is null!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case SubDOFBoundsNull:
    PetscErrorPrintf("3D bounds of subdof's of ddof are null!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case SubISNull:
    PetscErrorPrintf("lis or gis of subdof's of ddof is null!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case FETIDOFNull:
    PetscErrorPrintf("FETIDOF is null!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case FETIDOFNeedsInput:
    PetscErrorPrintf("FETIDOF function needs input!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case FETIDOFCannotOverlap:
    PetscErrorPrintf("FETIDOF can NOT use overlapping DDM!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case FETIDOFPMLNotImpl:
    PetscErrorPrintf("FETIDOF with pml-augmented subdomains is not implemented!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case FETIDOFNotInit:
    PetscErrorPrintf("FETIDOF is not initialized!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case FETIDOFWrong:
    PetscErrorPrintf("FETIDOF is in wrong state!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case MySectionWrong:
    PetscErrorPrintf("MySection is in wrong state!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  case BadDirection:
    PetscErrorPrintf("Direction number must be one of 0,1,2!");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  default:
    PetscErrorPrintf("Unknown myerr! ");
    MPI_Abort(MPI_COMM_WORLD,myerr);
    break;
  }
  
  return;  
}
