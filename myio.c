#include "myio.h"

#undef __FUNCT__
#define __FUNCT__  "MyVecViewAO"
PetscErrorCode MyVecViewAO(Vec v,AO ao,PetscViewer viewer)
{
  PetscInt ivstart,ivend;
  IS iv, iw;
  VecScatter scatter;
  Vec w;

  PetscFunctionBegin;
  if (ao){
    VecGetOwnershipRange(v,&ivstart,&ivend);
    ISCreateStride(PetscObjectComm((PetscObject)v),ivend-ivstart,ivstart,1,&iv);
    ISDuplicate(iv,&iw); ISCopy(iv,iw);
    AOPetscToApplicationIS(ao,iw);
    VecScatterCreate(v,iv,v,iw,&scatter);
    VecDuplicate(v,&w);
    VecScatterBegin(scatter,v,w,INSERT_VALUES,SCATTER_FORWARD);
    VecScatterEnd(scatter,v,w,INSERT_VALUES,SCATTER_FORWARD);
    VecView(w,viewer);
    ISDestroy(&iv); ISDestroy(&iw); 
    VecScatterDestroy(&scatter); VecDestroy(&w);
  }
  else {
    VecView(v,viewer);
  }
  PetscFunctionReturn(0);
}




/* Assume all data are of PetscReal or PetscScalar. If the data file is of
   single precision, then it will not work with double precision Petsc. */

/*
   Functions for resampling methods.
*/
static inline PetscReal nofun(PetscReal x)
{
  return x;
}

static inline PetscReal hmfun(PetscReal x)
{
  return 1.0/x;
}

static PetscReal p4pow= 2.0;

static inline PetscReal powpfun(PetscReal x)
{
  return pow(x,p4pow);
}

static inline PetscReal invpowpfun(PetscReal x)
{
  return pow(x,1.0/p4pow);
}

/*
   Set constant.
*/
inline MyErrCode SetCellDataConstant(PetscScalar **data,PetscInt siz,PetscScalar val)
{
  PetscErrorCode ierr;
  PetscInt i;
  ierr= PetscMalloc(siz*sizeof(PetscScalar),data);CHKERRQ(ierr);
  for(i = 0; i < siz; ++i) (*data)[i]=val;
  return 0;
}

/*
   Matrix view.
*/
inline MyErrCode MyMatView(PetscScalar *A,PetscInt m,PetscInt n,const char *format)
{
  PetscInt i,j;
  for(i=0;i<m;++i){
    printf("\n");
    for(j=0;j<n;++j){
      if (format)
	printf(format,PetscRealPart(A[j*m+i]),PetscImaginaryPart(A[j*m+i]));
      else
	printf("% .3f%+.3fi ",PetscRealPart(A[j*m+i]),PetscImaginaryPart(A[j*m+i]));
    }
  }
  printf("\n");
  return 0;
}


/*
   Resample cell-wise data from a file to a new mesh on an extended sub-region.
   Each processor only creates and reads data for its portion of the new mesh.

   The new mesh itself is not necessarily a sub-mesh of the original mesh of the
   data file, e.g. it can be non-uniform.  Alignment of the new mesh to the
   original one is based on the geometry (coordinates).

   Assume the original mesh of the data is uniform and the new mesh of type
   rectilinear.  The new mesh can have non-physical extension where we put
   values zeros.

   Assume the original data is real and the array on the new mesh is of type
   PetscScalar.
*/
#undef __FUNCT__
#define __FUNCT__ "AverageCellRealToMeshScalar"
MyErrCode AverageCellRealToMeshScalar(PetscScalar **array, MyDM dm, AverageMethod method, AverageCtx *ctx, char *filename, PetscScalar xl, PetscScalar xr, PetscScalar yl, PetscScalar yr, PetscScalar zl, PetscScalar zr, PetscInt nx, PetscInt ny, PetscInt nz)
{
  PetscScalar hx, hy, hz;	/* uniform mesh size of data file */
  PetscInt ixs,ixe,iys,iye,izs,ize; /* subscript bounds for the physical mesh on
					    this proc. */
  PetscInt jxs,jxe,jys,jye,jzs,jze; /* subscript bounds of the data file for
				       this proc. */
  PetscReal (*fun)(PetscReal x); /* functions for averaging */
  PetscReal (*invfun)(PetscReal x);
  PetscReal *data=0;
  PetscInt ix,iy,iz,jx,jy,jz;
  PetscScalar hx_new,hy_new,hz_new; /* (non-uniform) local mesh size */
  PetscScalar rx, ry, rz;	    /* local weights */
  PetscInt idx;			    /* index in *array */
  MyErrCode myerr;

  PetscFunctionBegin;
  /*
     Input check.
  */
  if(!(dm->has_coord)) return HasCoordFalse;
  if(!(dm->initialized)) return MyDMNotInitialized;
  if(dm->ixend-1<dm->eta_xl || dm->iyend-1<dm->eta_yl || dm->izend-1<dm->eta_zl) return ZeroPhyCells;
  if(dm->ixbegin>dm->nx_all-1-dm->eta_xr || dm->iybegin>dm->ny_all-1-dm->eta_yr || dm->izbegin>dm->nz_all-1-dm->eta_zr) return ZeroPhyCells;
  if(PetscRealPart(dm->x[dm->eta_xl])<PetscRealPart(xl) || PetscRealPart(dm->y[dm->eta_yl])<PetscRealPart(yl) || PetscRealPart(dm->z[dm->eta_zl])<PetscRealPart(zl)) return CoordOutofDomain;
  if(PetscRealPart(dm->x[dm->nx_all-dm->eta_xr])>PetscRealPart(xr) || PetscRealPart(dm->y[dm->ny_all-dm->eta_yr])>PetscRealPart(yr) || PetscRealPart(dm->z[dm->nz_all-dm->eta_zr])>PetscRealPart(zr)) return CoordOutofDomain;
  if(filename){if(filename[0]==0) return FileNameIsNull;}else return FileNameIsNull;
  if(PetscRealPart(xl)>=PetscRealPart(xr) || PetscRealPart(yl)>=PetscRealPart(yr) || PetscRealPart(zl)>=PetscRealPart(zr)) return WrongDomain;

  /*
     Alignment, we read from the file only those necessary for this processor.
  */
  /* physical cells of this processor is in [ixs,ixe)X[iys,iye)X[izs,ize) on global extended mesh */
  ixs= PetscMax(dm->ixbegin,dm->eta_xl); ixe= PetscMin(dm->ixend,dm->nx_all-dm->eta_xr);
  iys= PetscMax(dm->iybegin,dm->eta_yl); iye= PetscMin(dm->iyend,dm->ny_all-dm->eta_yr);
  izs= PetscMax(dm->izbegin,dm->eta_zl); ize= PetscMin(dm->izend,dm->nz_all-dm->eta_zr);
  /* printf("ix: %d~%d, iy: %d~%d, iz: %d~%d\n",ixs,ixe,iys,iye,izs,ize); */
  /* cell data necessary for this processor is in [jxs,jxe)X[jys,jye)X[jzs,jze) on data file */
  hx= (xr-xl)/nx; hy= (yr-yl)/ny; hz= (zr-zl)/nz; /* data mesh size */
  /* printf("h= %f, %f, %f\n",PetscRealPart(hx),PetscRealPart(hy),PetscRealPart(hz)); */
  jxs= (PetscInt)floor(PetscRealPart((dm->x[ixs]-xl)/hx)); jxe= (PetscInt)floor(PetscRealPart((dm->x[ixe]-xl)/hx)); jxe= PetscMin(jxe+1,nx);
  jys= (PetscInt)floor(PetscRealPart((dm->y[iys]-yl)/hy)); jye= (PetscInt)floor(PetscRealPart((dm->y[iye]-yl)/hy)); jye= PetscMin(jye+1,ny);
  jzs= (PetscInt)floor(PetscRealPart((dm->z[izs]-zl)/hz)); jze= (PetscInt)floor(PetscRealPart((dm->z[ize]-zl)/hz)); jze= PetscMin(jze+1,nz);

  /*
     Read necessary data from file.
  */
  PetscMalloc(sizeof(PetscReal)*(jxe-jxs)*(jye-jys)*(jze-jzs),&data);
  myerr= ReadArrayDataReal(data,filename,nx,ny,nz,jxs,jxe,jys,jye,jzs,jze);
  CHKMyErrQ(myerr);
  /* printf("jx: %d~%d, jy: %d~%d, jz: %d~%d\n",jxs,jxe,jys,jye,jzs,jze); */
  /* printf("w in file, the first 3 values intersects this proc: %f, %f, %f\n",data[0],data[1],data[2]); */
  /* idx= 0; */
  /* for(jx=jxs;jx<jxe;jx++) */
  /*   for(jy=jys;jy<jye;jy++) */
  /*     for(jz=jzs;jz<jze;jz++){ */
  /* 	printf("%.4f  ",data[idx++]); */
  /*     } */

  /*
     Redefine the domain
  */
  xl= xl+jxs*hx; yl= yl+jys*hy; zl= zl+jzs*hz;
  nx= jxe-jxs; ny= jye-jys; nz= jze-jzs;

  /*
     Choose a method of averaging.
  */
  switch (method) {
  case ArithemeticMean:
    fun= nofun; invfun= nofun; break;
  case GeometricMean:
    fun= &log; invfun= &exp; break;
  case pArithemeticMean:
    if(!ctx) return CtxIsNull;
    p4pow= ctx->p; fun= powpfun; invfun= invpowpfun; break;
  default: case HarmonicMean:
    fun= hmfun; invfun= hmfun; break;
  }

  /*
     Create array including PML for this processor and set values for physical
     cells.  Values for PML cells are left zeros.
  */
  if(array && !(*array)) PetscMalloc(sizeof(PetscScalar)*(dm->nloc),array);
  PetscMemzero(*array,sizeof(PetscScalar)*(dm->nloc));
  for(ix=ixs;ix<ixe;++ix){
    jxs= (PetscInt)floor(PetscRealPart((dm->x[ix]-xl)/hx));
    jxe= (PetscInt)floor(PetscRealPart((dm->x[ix+1]-xl)/hx));
    jxe= PetscMin(jxe+1,nx);
    hx_new= dm->x[ix+1]-dm->x[ix];
    for(iy=iys;iy<iye;++iy){
      jys= (PetscInt)floor(PetscRealPart((dm->y[iy]-yl)/hy));
      jye= (PetscInt)floor(PetscRealPart((dm->y[iy+1]-yl)/hy));
      jye= PetscMin(jye+1,ny);
      hy_new= dm->y[iy+1]-dm->y[iy];
      for(iz=izs;iz<ize;++iz){
  	jzs= (PetscInt)floor(PetscRealPart((dm->z[iz]-zl)/hz));
  	jze= (PetscInt)floor(PetscRealPart((dm->z[iz+1]-zl)/hz));
  	jze= PetscMin(jze+1,nz);
  	hz_new= dm->z[iz+1]-dm->z[iz];
  	idx= (ix-dm->ixbegin)*(dm->nyloc)*(dm->nzloc)+(iy-dm->iybegin)*(dm->nzloc)+(iz-dm->izbegin);
  	for(jx=jxs;jx<jxe;++jx){
  	  rx= (PetscMin(PetscRealPart(xl+jx*hx+hx),PetscRealPart(dm->x[ix+1]))-PetscMax(PetscRealPart(xl+jx*hx),PetscRealPart(dm->x[ix])))/hx_new;
  	  for(jy=jys;jy<jye;++jy){
  	    ry= (PetscMin(PetscRealPart(yl+jy*hy+hy),PetscRealPart(dm->y[iy+1]))-PetscMax(PetscRealPart(yl+jy*hy),PetscRealPart(dm->y[iy])))/hy_new;
  	    for(jz=jzs;jz<jze;++jz){
  	      rz= (PetscMin(PetscRealPart(zl+jz*hz+hz),PetscRealPart(dm->z[iz+1]))-PetscMax(PetscRealPart(zl+jz*hz),PetscRealPart(dm->z[iz])))/hz_new;
  	      (*array)[idx]+= ((PetscScalar)(fun(data[jx*ny*nz+jy*nz+jz])))*rx*ry*rz;
  	      /* printf("jx=%d, jy=%d, jz= %d, nx=%d, ny=%d, nz=%d, r=(%.2f,%.2f,%.2f)\n",jx,jy,jz,nx,ny,nz,PetscRealPart(rx),PetscRealPart(ry),PetscRealPart(rz)); */
  	    }
  	  }
  	}
  	(*array)[idx]= (PetscScalar)(invfun(PetscRealPart((*array)[idx])));
  	/* printf("idx=%2d, val=%.4f\n",idx,PetscRealPart((*array)[idx])); */
      }
    }
  }

  PetscFree(data);
  PetscFunctionReturn(0);
}


/*
   Read in cell-wise data from portion of a file.  Alignment of the partial data
   to the file is based on 3D subscripts (geometry is not considered).

   This is done purely parallel.  Each processor reads only the portion of data
   that it needs.  Multiple processes reading the same file simultaneously
   should not be a problem.  Assume the data runs first in z then y, x.  We
   assume data is already allocated with memory.
*/
#undef __FUNCT__
#define __FUNCT__ "ReadArrayDataReal"
MyErrCode ReadArrayDataReal(PetscReal data[],char *filename,PetscInt nx,PetscInt ny,PetscInt nz,PetscInt ixs, PetscInt ixe, PetscInt iys, PetscInt iye,PetscInt izs,PetscInt ize)
{
  FILE *file;
  unsigned long flen;
  PetscInt ix,iy,count;

  PetscFunctionBegin;
  /* open files */
  file= fopen(filename,"r"); if(!file){return CannotOpenFile;}
  fseek(file, 0, SEEK_END); flen= (unsigned long) ftell(file);
  if (flen/sizeof(PetscReal)!=(nx*ny*nz)){return FileLengthNonMatching;}

  /* read data */
  count= 0;
  for(ix=ixs;ix<ixe;ix++){
    for(iy=iys;iy<iye;iy++){
      fseek(file, (ix*ny*nz+iy*nz+izs)*sizeof(PetscReal), SEEK_SET);
      fread(data+count,sizeof(PetscReal),ize-izs,file);
      count+= ize-izs;
    }
  }
  fclose(file);

  PetscFunctionReturn(0);
}

/*
   Resmaple data on one mesh to another and save into a new file.

   Always do integral average, that is, values are weighed by the volumes they
   occupy in a new cell.

   Assume both the data mesh and the new mesh are of uniform spacing and they
   cover the same domain.

   Assume in the old file, first count along z then y, x.

   For the moment, we only do it by one processor.  On a network file system
   (NFS), the created file is already exposed to other processors.
*/
#undef __FUNCT__
#define __FUNCT__ "AverageCellDataReal"
MyErrCode AverageCellDataReal(MPI_Comm comm,const AverageMethod method,const AverageCtx *ctx,const char *filename_old,const PetscInt nx_old,const PetscInt ny_old,const PetscInt nz_old, const char *filename_new,const DimOrder order_new,PetscInt nx_new,PetscInt ny_new,PetscInt nz_new)
{
  int flag;
  PetscInt i,j,k,count,io,jo,ko,iolds,iolde,jolds,jolde,kolds,kolde, ni, nj, nk, ni_old, nj_old, nk_old, dispi_old, dispj_old, dispk_old;
  PetscMPIInt rank=0;
  FILE *file_old, *file_new;
  unsigned long flen;
  PetscReal *data_old, *data_new, hi_new, hj_new, hk_new, l1, l2, ri, rj, rk;
  PetscReal (*fun)(PetscReal x);
  PetscReal (*invfun)(PetscReal x);
  PetscErrorCode ierr;

  PetscFunctionBegin;
  /* test communicator */
  MPI_Initialized(&flag);
  if (flag && comm!=MPI_COMM_NULL) {
    MPI_Comm_rank(comm,&rank);
    if (rank) {MPI_Barrier(comm); PetscFunctionReturn(0);}
  }
  else if (flag && comm==MPI_COMM_NULL) {return CommUndefined;}

  /* open files */
  file_old= fopen(filename_old,"r"); if(!file_old){return CannotOpenFile;}
  fseek(file_old, 0, SEEK_END); flen= (unsigned long) ftell(file_old);
  if (flen/sizeof(PetscReal)!=(nx_old*ny_old*nz_old)){return FileLengthNonMatching;}
  file_new= fopen(filename_new,"w"); if(!file_new){return CannotOpenFile;}

  /* read old data */
  ierr=PetscMalloc((PetscInt)flen,&data_old);CHKERRQ(ierr);
  fseek(file_old, 0, SEEK_SET);
  fread(data_old,sizeof(PetscReal),nx_old*ny_old*nz_old,file_old);
  fclose(file_old);

  /* compute and write new data */
  ierr=PetscMalloc(sizeof(PetscReal)*nx_new*ny_new*nz_new,&data_new);CHKERRQ(ierr);
  PetscMemzero(data_new,sizeof(PetscReal)*nx_new*ny_new*nz_new);
  switch (method) {
  case ArithemeticMean:
    fun= nofun; invfun= nofun; break;
  case GeometricMean:
    fun= &log; invfun= &exp; break;
  case pArithemeticMean:
    if(!ctx) return CtxIsNull;
    p4pow= ctx->p; fun= powpfun; invfun= invpowpfun; break;
  default: case HarmonicMean:
    fun= hmfun; invfun= hmfun; break;
  }
  switch (order_new){
  case FirstXYZ:		/* let h_old=1 */
    ni= nz_new; ni_old= nz_old; dispi_old= 1; hi_new= ((PetscReal)nz_old)/((PetscReal)nz_new);
    nj= ny_new; nj_old= ny_old; dispj_old= nz_old; hj_new= ((PetscReal)ny_old)/((PetscReal)ny_new);
    nk= nx_new; nk_old= nx_old; dispk_old= ny_old*nz_old; hk_new= ((PetscReal)nx_old)/((PetscReal)nx_new);
    break;
  case FirstYZX:
    ni= nx_new; ni_old= nx_old; dispi_old= ny_old*nz_old; hi_new= ((PetscReal)nx_old)/((PetscReal)nx_new);
    nj= nz_new; nj_old= nz_old; dispj_old= 1; hj_new= ((PetscReal)nz_old)/((PetscReal)nz_new);
    nk= ny_new; nk_old= ny_old; dispk_old= nz_old; hk_new= ((PetscReal)ny_old)/((PetscReal)ny_new);
    break;
  case FirstZXY:
    ni= ny_new; ni_old= ny_old; dispi_old= nz_old; hi_new= ((PetscReal)ny_old)/((PetscReal)ny_new);
    nj= nx_new; nj_old= nx_old; dispj_old= ny_old*nz_old; hj_new= ((PetscReal)nx_old)/((PetscReal)nx_new);
    nk= nz_new; nk_old= nz_old; dispk_old= 1; hk_new= ((PetscReal)nz_old)/((PetscReal)nz_new);
    break;
  case FirstYXZ:
    ni= nz_new; ni_old= nz_old; dispi_old= 1; hi_new= ((PetscReal)nz_old)/((PetscReal)nz_new);
    nj= nx_new; nj_old= nx_old; dispj_old= ny_old*nz_old; hj_new= ((PetscReal)nx_old)/((PetscReal)nx_new);
    nk= ny_new; nk_old= ny_old; dispk_old= nz_old; hk_new= ((PetscReal)ny_old)/((PetscReal)ny_new);
    break;
  case FirstXZY:
    ni= ny_new; ni_old= ny_old; dispi_old= nz_old; hi_new= ((PetscReal)ny_old)/((PetscReal)ny_new);
    nj= nz_new; nj_old= nz_old; dispj_old= 1; hj_new= ((PetscReal)nz_old)/((PetscReal)nz_new);
    nk= nx_new; nk_old= nx_old; dispk_old= ny_old*nz_old; hk_new= ((PetscReal)nx_old)/((PetscReal)nx_new);
    break;
  default: case FirstZYX:
    ni= nx_new; ni_old= nx_old; dispi_old= ny_old*nz_old; hi_new= ((PetscReal)nx_old)/((PetscReal)nx_new);
    nj= ny_new; nj_old= ny_old; dispj_old= nz_old; hj_new= ((PetscReal)ny_old)/((PetscReal)ny_new);
    nk= nz_new; nk_old= nz_old; dispk_old= 1; hk_new= ((PetscReal)nz_old)/((PetscReal)nz_new);
    break;
  }
  count= 0;
  for(i=0;i<ni;i++){
    iolds= (PetscInt)floor(i*hi_new);		   /* let h_old=1 */
    iolde= (PetscInt)floor((i+1)*hi_new); iolde= (iolde<ni_old-1)?iolde:ni_old-1;
    for(j=0;j<nj;j++){
      jolds= (PetscInt)floor(j*hj_new);
      jolde= (PetscInt)floor((j+1)*hj_new); jolde= (jolde<nj_old-1)?jolde:nj_old-1;
      for(k=0;k<nk;k++){
  	kolds= (PetscInt)floor(k*hk_new);
	kolde= (PetscInt)floor((k+1)*hk_new);
	kolde= (kolde<nk_old-1)?kolde:nk_old-1;
  	for(io=iolds;io<=iolde;io++){
  	  l1= (PetscReal)io; l2= l1+1.0;
  	  l1= (l1>i*hi_new)?l1:i*hi_new; l2= (l2<(i+1)*hi_new)?l2:(i+1)*hi_new;
  	  ri= (l2-l1)/hi_new;
  	  for(jo=jolds;jo<=jolde;jo++){
  	    l1= (PetscReal)jo; l2= l1+1.0;
  	    l1= (l1>j*hj_new)?l1:j*hj_new; l2= (l2<(j+1)*hj_new)?l2:(j+1)*hj_new;
  	    rj= (l2-l1)/hj_new;
  	    for(ko=kolds;ko<=kolde;ko++){
  	      l1= (PetscReal)ko; l2= l1+1.0;
  	      l1= (l1>k*hk_new)?l1:k*hk_new; l2= (l2<(k+1)*hk_new)?l2:(k+1)*hk_new;
  	      rk= (l2-l1)/hk_new;
  	      data_new[count]+= fun(data_old[io*dispi_old+jo*dispj_old+ko*dispk_old])*ri*rj*rk;
  	    }
  	  }
  	}
  	data_new[count]= invfun(data_new[count]);
	/* printf("%.4f ",data_new[count]); */
  	count++;
      }
    }
  }
  fwrite(data_new,sizeof(PetscReal),nx_new*ny_new*nz_new,file_new);
  fclose(file_new);
  PetscFree(data_old); PetscFree(data_new);
  if (flag) MPI_Barrier(comm);
  PetscFunctionReturn(0);
}
