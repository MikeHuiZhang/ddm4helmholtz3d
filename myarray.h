#ifndef _MYARRAY
#define _MYARRAY

#include "myerr.h"
#include <petscis.h>

extern MyErrCode MyArraysub2ind(PetscInt nx, PetscInt ny, PetscInt nz, PetscInt ix, PetscInt iy, PetscInt iz, PetscInt *ind);

extern MyErrCode MyArrayind2sub(PetscInt nx, PetscInt ny, PetscInt nz, PetscInt *ix, PetscInt *iy, PetscInt *iz, PetscInt ind);

extern MyErrCode MyArraySubArray(PetscScalar **subarray,PetscScalar array[],PetscInt nx,PetscInt ny,PetscInt nz,PetscInt xs,PetscInt xe, PetscInt ys,PetscInt ye,PetscInt zs,PetscInt ze);

extern MyErrCode MyArraySubIS(IS *subis, MPI_Comm comm, PetscInt nx, PetscInt ny, PetscInt nz, PetscInt xs, PetscInt xe, PetscInt ys, PetscInt ye, PetscInt zs, PetscInt ze);


#endif
