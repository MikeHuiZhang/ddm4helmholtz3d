#ifndef _FETIHELM
#define _FETIHELM

#include "fetisys.h"
#include "helmbvp.h"

/* 
   feti formulation of Helmholtz BVP

   specify regularization for op and pc

   assembly of subdomain matrices for HelmBVP

   setup plane wave coarse basis Q

 */

struct FETIHelm_
{
  /* prefix */
  char *prefix;

  /* orginal bvp */
  HelmBVP bvp;

  /* fetidof */
  FETIDOF fetidof;

  /* regularization parameters: reg4op is signed and small to approximate
   * Neumann, reg4pc can be signed and must be large to approximate Dirichlet */
  PetscScalar reg4op, reg4pc;

  /* subdomain bvp's for the feti operator */
  HelmBVP *sbvp;

  /* number of coarse basis per face/edge/vertex */
  PetscInt nwf, nwe, nwv;

  /* use coarse basis as dual or primal coarse dof: true for primal */
  PetscBool pwf, pwe, pwv;

  /* coarse basis consists of nodal values of the following functions */
  ArrayCtxFun cff, cfe;
  MyErrCode (*createcfctx)(void **ctx,PetscInt,PetscInt,const struct FETIHelm_*);
  MyErrCode (*createcectx)(void **ctx,PetscInt,PetscInt,const struct FETIHelm_*);
  MyErrCode (*destroycfctx)(void **ctx);
  MyErrCode (*destroycectx)(void **ctx);
  void *capp;			/* appendix for coarsening */
  MyErrCode (*destroycapp)(void**);
};

typedef struct FETIHelm_  structFETIHelm, *FETIHelm;
typedef MyErrCode (*FETIHelmCreateCoarseCtx)(void **ctx,PetscInt,PetscInt,const struct FETIHelm_*);
typedef MyErrCode (*FETIHelmDestroyCoarseCtx)(void **ctx);

extern MyErrCode FETIHelmCreate(FETIHelm*);
extern MyErrCode FETIHelmDestroy(FETIHelm*);
extern MyErrCode FETIHelmSetBVPDOF(FETIHelm,HelmBVP,FETIDOF);
extern MyErrCode FETIHelmSetPrefix(FETIHelm,const char*);
extern MyErrCode FETIHelmSetFromOptions(FETIHelm,const char*,const char*);
extern MyErrCode FETIHelmCreateSubBVP(FETIHelm);
extern MyErrCode FETIHelmSetDirichlet(FETIHelm);
extern MyErrCode FETIHelmCreateCoarseDOF(FETIHelm);
extern MyErrCode FETIHelmSetup(FETIHelm);
#endif
